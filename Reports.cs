using System;
using System.Collections.Generic;
using System.Text;
using UEI.Workflow2010.Report.DataAccess;
using UEI.Workflow2010.Report;
using BusinessObject;
using System.Data;
using System.Collections;

namespace UEI.Workflow2010.Report.Service
{
    public class Reports
    {
        #region Variable
        private DBFactory _DB = null;
        private IDBAccess _DBAccess = null;
        private TimeSpan totalExecTime = TimeSpan.Zero;
        private XMLOperations _XML = null;
        #endregion

        #region Constructor
        public Reports()
        {
            _DB = new DBFactory();            
            SetDBAccesslayerType(DBLayerType.IBatis);
            _DBAccess.DBInit();                        
        }        
        #endregion

        #region Set DB Layer Type
        public void SetDBAccesslayerType(DBLayerType _DBLayerType)
        {            
            switch (_DBLayerType)
            {
                case DBLayerType.IBatis:
                    _DBAccess = _DB.GetDBAccessLayerType(DBLayerType.IBatis);
                    break;
                case DBLayerType.ADONet:
                    _DBAccess = _DB.GetDBAccessLayerType(DBLayerType.ADONet);
                    break;
            }
        }
        #endregion

        #region GetAllIDs
        //Returns the entire ids.
        public IList<string> GetAllIDs()
        {
            try
            {
                ErrorLogging.WriteToLogFile("--Function Name: GetAllIDs--", false);
                IList<string> idList = new List<string>();

                ErrorLogging.WriteToLogFile("UEI2_BO_Sp_GetID execution time", false);
                DateTime spStart = DateTime.Now;
                ErrorLogging.WriteToLogFile("Start Time: " + spStart.Hour.ToString() + ":" + spStart.Minute.ToString() + ":" + spStart.Second.ToString() + ":" + spStart.Millisecond.ToString(), false);

                idList = _DBAccess.GetAllIDList();

                DateTime spEnd = DateTime.Now;

                ErrorLogging.WriteToLogFile("End Time: " + spEnd.Hour.ToString() + ":" + spEnd.Minute.ToString() + ":" + spEnd.Second.ToString() + ":" + spEnd.Millisecond.ToString(), false);

                totalExecTime = spEnd - spStart;
                ErrorLogging.WriteToLogFile("Total sp Time: " + totalExecTime.Hours.ToString() + ":" + totalExecTime.Minutes.ToString() + ":" + totalExecTime.Seconds.ToString() + ":" + totalExecTime.Milliseconds.ToString(), false);

                return idList;
            }
            catch(Exception ex)
            {
                ErrorLogging.WriteToLogFile("---Exception raised---", false);
                ErrorLogging.WriteToErrorLogFile(ex);

                return null;
            }
        }
        public IList<string> GetAllIDs(params string[] IDsOrModes)
        {
            ErrorLogging.WriteToLogFile("--Function Name: GetAllIDs--", false);

            IList<string> idList = new List<string>();
            if (IDsOrModes != null && IDsOrModes.Length > 0)
            {
                IDSearchParameter param = new IDSearchParameter();                
                param.FirstIDOrMode = IDsOrModes[0];
                if (IDsOrModes.Length == 2 && IDsOrModes[1] != null)
                {
                    //check for first node and second node
                    Int32 firstID = 0, secondID = 0;
                    firstID = Int32.Parse(IDsOrModes[0].Substring(1, 4));
                    secondID = Int32.Parse(IDsOrModes[1].Substring(1, 4));
                    if (firstID > secondID)
                    {
                        param.FirstIDOrMode = IDsOrModes[1];
                        param.LastIDOrMode = IDsOrModes[0];
                    }
                    else
                    {
                        param.FirstIDOrMode = IDsOrModes[0];
                        param.LastIDOrMode = IDsOrModes[1];
                    }                    
                }
                ErrorLogging.WriteToLogFile("UEI2_Sp_GetModeIDList execution time", false);
                DateTime spStart = DateTime.Now;
                ErrorLogging.WriteToLogFile("Start Time: " + spStart.Hour.ToString() + ":" + spStart.Minute.ToString() + ":" + spStart.Second.ToString() + ":" + spStart.Millisecond.ToString(), false);

                idList = _DBAccess.GetAllIDList(param);

                DateTime spEnd = DateTime.Now;

                ErrorLogging.WriteToLogFile("End Time: " + spEnd.Hour.ToString() + ":" + spEnd.Minute.ToString() + ":" + spEnd.Second.ToString() + ":" + spEnd.Millisecond.ToString(), false);

                totalExecTime = spEnd - spStart;
                ErrorLogging.WriteToLogFile("Total sp Time: " + totalExecTime.Hours.ToString() + ":" + totalExecTime.Minutes.ToString() + ":" + totalExecTime.Seconds.ToString() + ":" + totalExecTime.Milliseconds.ToString(), false);


            }
            return idList;
        }
        public IList<string> GetAllModes()
        {
            ErrorLogging.WriteToLogFile("--Function Name: GetAllModes()--", false);

            IList<string> modeList = new List<string>();
            
            ErrorLogging.WriteToLogFile("UEI2_Sp_GetModeList execution time", false);
            DateTime spStart = DateTime.Now;
            ErrorLogging.WriteToLogFile("Start Time: " + spStart.Hour.ToString() + ":" + spStart.Minute.ToString() + ":" + spStart.Second.ToString() + ":" + spStart.Millisecond.ToString(), false);

            modeList = _DBAccess.GetModeList();

            DateTime spEnd = DateTime.Now;
            ErrorLogging.WriteToLogFile("End Time: " + spEnd.Hour.ToString() + ":" + spEnd.Minute.ToString() + ":" + spEnd.Second.ToString() + ":" + spEnd.Millisecond.ToString(), false);

            totalExecTime = spEnd - spStart;
            ErrorLogging.WriteToLogFile("Total sp Time: " + totalExecTime.Hours.ToString() + ":" + totalExecTime.Minutes.ToString() + ":" + totalExecTime.Seconds.ToString() + ":" + totalExecTime.Milliseconds.ToString(), false);

            return modeList;
        }
        #endregion

        #region ID Selection
        public Model.IDByRegionReportModel IDByRegionandSubDevice(IList<Model.Id> ids, IList<Model.Region> locations,List<String> selectedModes, IList<Model.DeviceType> devices, IList<Model.DataSources> dataSources, Model.IdSetupCodeInfo idSetupCodeInfoParam)
        {
            IDSearchParameter searchParam = null;
            try
            {
                if (ids != null && locations != null && dataSources != null)
                {
                    searchParam = new IDSearchParameter();
                    ////for procedure
                    StringBuilder paramBuilder = new StringBuilder();
                    String idStr = String.Empty;
                    foreach (Model.Id id in ids)
                        paramBuilder.Append(id.ID + ",");
                    if (paramBuilder.Length > 0)
                        paramBuilder.Remove(paramBuilder.Length - 1, 1);

                    idStr = paramBuilder.ToString();

                    String strLocations = String.Empty;
                    if (locations != null)
                    {
                        _XML = new XMLOperations("UEI2Server",XMLOperations.EncodingType.UTF16);                        
                        _XML.ParentNode             = "Document";
                        _XML.ParentAttributeName    = "Type";
                        _XML.ParentAttributeValue   = "LocationSelections";
                        _XML.ParentAttributeName    = "Version";
                        _XML.ParentAttributeValue   = "1.0.0";
                        _XML.AppendParent();
                                              
                        foreach (Model.Region r in locations)
                        {
                            _XML.ParentNode             = "Region";
                            _XML.ParentAttributeName    = "Name";
                            _XML.ParentAttributeValue   = r.Name;
                            _XML.ParentAttributeName    = "Weight";
                            _XML.ParentAttributeValue   = "0";
                            _XML.ParentAttributeName    = "SystemFlags";
                            if (idSetupCodeInfoParam.IncludeNonCountryLinkedRecords == true)
                            {
                                _XML.ParentAttributeValue = "2";
                            }
                            else
                            {
                                _XML.ParentAttributeValue = "0";
                            }
                            if (r.SubRegions != null)
                            {
                                foreach (Model.Region subregion in r.SubRegions)
                                {
                                    _XML.ChildNode              = "SubRegion";
                                    _XML.ChildAttributeName     = "Name";
                                    _XML.ChildAttributeValue    = subregion.Name;
                                    _XML.ChildAttributeName     = "Weight";
                                    _XML.ChildAttributeValue    = "0";
                                    _XML.ChildAttributeName     = "SystemFlags";
                                    _XML.ChildAttributeValue    = "0";
                                    if (r.Countries != null)
                                    {
                                        foreach (Model.Country country in r.Countries)
                                        {
                                            if (subregion.Name == country.SubRegion)
                                            {
                                                _XML.ChildNode              = "Country";
                                                _XML.ChildAttributeName     = "Name";
                                                _XML.ChildAttributeValue    = country.Name;
                                                _XML.ChildAttributeName     = "Weight";
                                                _XML.ChildAttributeValue    = "0";
                                                _XML.ChildAttributeName     = "SystemFlags";
                                                _XML.ChildAttributeValue    = "0";
                                                _XML.AppendChild();
                                            }
                                        }
                                    }
                                    _XML.AppendChild();
                                }
                            }
                            if (r.Countries != null)
                            {
                                foreach (Model.Country country in r.Countries)
                                {
                                    if (country.SubRegion == String.Empty)
                                    {
                                        _XML.ChildNode              = "Country";
                                        _XML.ChildAttributeName     = "Name";
                                        _XML.ChildAttributeValue    = country.Name;
                                        _XML.ChildAttributeName     = "Weight";
                                        _XML.ChildAttributeValue    = "0";
                                        _XML.ChildAttributeName     = "SystemFlags";
                                        _XML.ChildAttributeValue    = "0";
                                        _XML.AppendChild();
                                    }
                                }
                            }
                            _XML.AppendParent();
                        }                        
                    }
                    strLocations = _XML.XMLGenerator();
                    _XML = null;

                    String strDevices = String.Empty;
                    if (devices != null)
                    {
                        _XML = new XMLOperations("UEI2Server",XMLOperations.EncodingType.UTF16);                        
                        _XML.ParentNode             = "Document";
                        _XML.ParentAttributeName    = "Type";
                        _XML.ParentAttributeValue   = "DeviceSelections";
                        _XML.ParentAttributeName    = "Version";
                        _XML.ParentAttributeValue   = "1.0.0";
                        _XML.AppendParent();
                        
                        foreach (String mode in selectedModes)
                        {
                            _XML.ParentNode             = "DeviceMode";
                            _XML.ParentAttributeName    = "Name";
                            _XML.ParentAttributeValue   = mode;
                            _XML.ParentAttributeName    = "SystemFlags";
                            

                            //if(idSetupCodeInfoParam.IncludeEmptyDeviceType == true)
                            //    _XML.ParentAttributeValue = "3";
                            //else if (idSetupCodeInfoParam.IncludeEmptyDeviceType == false)
                            //    _XML.ParentAttributeValue = "1";

                            if(idSetupCodeInfoParam.IncludeNonComponentLinkedRecords == true && idSetupCodeInfoParam.IncludeEmptyDeviceType == true)
                                _XML.ParentAttributeValue = "7";
                            else if(idSetupCodeInfoParam.IncludeNonComponentLinkedRecords == true && idSetupCodeInfoParam.IncludeEmptyDeviceType == false)
                                _XML.ParentAttributeValue = "4";
                            else if(idSetupCodeInfoParam.IncludeNonComponentLinkedRecords == false && idSetupCodeInfoParam.IncludeEmptyDeviceType == true)
                                _XML.ParentAttributeValue = "3";
                            else if(idSetupCodeInfoParam.IncludeNonComponentLinkedRecords == false && idSetupCodeInfoParam.IncludeEmptyDeviceType == false)
                                _XML.ParentAttributeValue = "1";

                            foreach (Model.DeviceType device in devices)
                            {
                                if (device.Mode == mode)
                                {
                                    _XML.ChildNode              = "MainDevices";
                                    _XML.ChildAttributeName     = "Name";
                                    _XML.ChildAttributeValue    = device.Name;
                                    _XML.ChildAttributeName     = "SystemFlags";
                                    _XML.ChildAttributeValue    = "1";
                                    if (device.SubDevices != null)
                                    {
                                        foreach (Model.DeviceType subdevice in device.SubDevices)
                                        {
                                            _XML.ChildNode              = "SubDevices";
                                            _XML.ChildAttributeName     = "Name";
                                            _XML.ChildAttributeValue    = subdevice.Name;
                                            _XML.ChildAttributeName     = "SystemFlags";
                                            _XML.ChildAttributeValue    = "1";
                                            if (device.Components != null)
                                            {
                                                foreach (Model.Component component in device.Components)
                                                {
                                                    if (subdevice.Name == component.SubDeviceType)
                                                    {
                                                        _XML.ChildNode                  = "Component";
                                                        _XML.ChildAttributeName         = "Name";
                                                        _XML.ChildAttributeValue        = component.Name;
                                                        _XML.ChildAttributeName         = "SystemFlags";
                                                            _XML.ChildAttributeValue    = "0";
                                                        _XML.ChildAttributeName         = "SystemFlagsExt";
                                                            _XML.ChildAttributeValue    = "1";
                                                        _XML.AppendChild();
                                                    }
                                                }
                                            }
                                            _XML.AppendChild();
                                        }
                                    }
                                    if (device.Components != null)
                                    {
                                        foreach (Model.Component component in device.Components)
                                        {
                                            if (component.SubDeviceType == String.Empty)
                                            {
                                                _XML.ChildNode              = "Component";
                                                _XML.ChildAttributeName     = "Name";
                                                _XML.ChildAttributeValue    = component.Name;
                                                _XML.ChildAttributeName     = "SystemFlags";
                                                _XML.ChildAttributeValue    = "0";
                                                _XML.ChildAttributeName     = "SystemFlagsExt";
                                                _XML.ChildAttributeValue    = "1";
                                                _XML.AppendChild();
                                            }
                                        }
                                    }
                                    _XML.AppendChild();
                                }
                            }
                            _XML.AppendParent();
                        }                        
                    }
                    strDevices = _XML.XMLGenerator();

                    paramBuilder = new StringBuilder();
                    String dataSourceStr = String.Empty;
                    foreach (Model.DataSources l in dataSources)
                        paramBuilder.Append(l.Name + ",");
                    if (paramBuilder.Length > 0)
                        paramBuilder.Remove(paramBuilder.Length - 1, 1);

                    dataSourceStr = paramBuilder.ToString();

                    searchParam.Ids             = idStr;
                    searchParam.XMLLocations    = strLocations;
                    searchParam.XMLDevices      = strDevices;
                    searchParam.DataSources     = dataSourceStr;
                    searchParam.StartDate       = idSetupCodeInfoParam.StartDate;
                    searchParam.EndDate         = idSetupCodeInfoParam.EndDate;
                    searchParam.RegionType      = 0;
                    searchParam.IncludeUnknownLocation = (idSetupCodeInfoParam.IncludeUnknownLocation == true) ? 1 : 0;

                    if (idSetupCodeInfoParam.SortType == UEI.Workflow2010.Report.Model.ReportSortBy.ModelCount)
                        searchParam.SearchFlag = "0";
                    else if (idSetupCodeInfoParam.SortType == UEI.Workflow2010.Report.Model.ReportSortBy.POS)
                        searchParam.SearchFlag = "1";
                    else if (idSetupCodeInfoParam.SortType == UEI.Workflow2010.Report.Model.ReportSortBy.SearchStatistics)
                        searchParam.SearchFlag = "2";
                    
                    searchParam.IncludeRestrictedIDs            = idSetupCodeInfoParam.IncludeRestrictedIds;
                    searchParam.IncudeRecordWithoutModelName    = (idSetupCodeInfoParam.IncludeBlankModelNames) ? 1 : 0;
                    searchParam.ModelType                       = idSetupCodeInfoParam.ModelType;

                    ErrorLogging.WriteToLogFile("--Function Name: IDByRegionandSubDevice--", false);
                    ErrorLogging.WriteToLogFile("UEI2_Sp_GetIDList Input Parameters", false);
                    ErrorLogging.WriteToLogFile("##########################################################", false);                    
                    ErrorLogging.WriteToLogFile("IDS            :" + idStr, false);
                    ErrorLogging.WriteToLogFile("LOCATION XML   :" + strLocations, false);
                    ErrorLogging.WriteToLogFile("DEVICE XML     :" + strDevices, false);
                    ErrorLogging.WriteToLogFile("DATASOURCE     :" + dataSourceStr, false);
                    ErrorLogging.WriteToLogFile("##########################################################", false);                   
                    
                    DateTime spStart = DateTime.Now;
                    ErrorLogging.WriteToLogFile("UEI2_Sp_GetIDList execution time", false);
                    ErrorLogging.WriteToLogFile("Start Time: " + spStart.Hour.ToString() + ":" + spStart.Minute.ToString() + ":" + spStart.Second.ToString() + ":" + spStart.Millisecond.ToString(), false);
                    IDBrandResultCollection resultCollection = _DBAccess.GetIDByRegionResults(searchParam);
                    DateTime spEnd = DateTime.Now;
                    ErrorLogging.WriteToLogFile("End Time: " + spEnd.Hour.ToString() + ":" + spEnd.Minute.ToString() + ":" + spEnd.Second.ToString() + ":" + spEnd.Millisecond.ToString(), false);

                    totalExecTime = spEnd - spStart;
                    ErrorLogging.WriteToLogFile("Total sp Time: " + totalExecTime.Hours.ToString() + ":" + totalExecTime.Minutes.ToString() + ":" + totalExecTime.Seconds.ToString() + ":" + totalExecTime.Milliseconds.ToString(), false);

                    if (resultCollection != null)
                    {
                        return GetAllIDByRegion(resultCollection);
                    }
                }

                return null;
            }
            catch (Exception ex)
            {
                ErrorLogging.WriteToLogFile("---Exception raised---", false);
                ErrorLogging.WriteToErrorLogFile(ex, searchParam);
                return null;
            }
        }        
        private Model.IDByRegionReportModel GetAllIDByRegion(IList<IDBrandResult> searchResults)
        {
            Model.IDByRegionReportModel idByRegionReport = null;

            if (searchResults != null)
            {
                idByRegionReport = new Model.IDByRegionReportModel();

                foreach (IDBrandResult result in searchResults)
                {
                    Model.IDByRegionReportRecord record = new Model.IDByRegionReportRecord();
                    record.ID = result.ID;
                    record.ModelCount = result.Count;
                    record.TotalUnitsSold = result.Count;
                    record.SearchStatisticsCount = result.Count;
                    record.Mode = result.ID.Substring(0, 1);
                    idByRegionReport.Add(record);
                }
            }
            return idByRegionReport;
        }
        private Model.IDByRegionReportModel GetAllIDByRegion(IDBrandResultCollection searchResults)
        {
            ErrorLogging.WriteToLogFile("--Function Name: GetAllIDByRegion--", false);
            ErrorLogging.WriteToLogFile("Business logic execution time", false);
            DateTime blStart = DateTime.Now;
            ErrorLogging.WriteToLogFile("Start Time: " + blStart.Hour.ToString() + ":" + blStart.Minute.ToString() + ":" + blStart.Second.ToString() + ":" + blStart.Millisecond.ToString(), false);

            Model.IDByRegionReportModel idByRegionReport = null;

            if (searchResults != null)
            {
                idByRegionReport = new Model.IDByRegionReportModel();

                foreach (IDBrandResult result in searchResults)
                {
                    Model.IDByRegionReportRecord record = new Model.IDByRegionReportRecord();
                    record.ID = result.ID;
                    record.ModelCount = result.Count;
                    record.TotalUnitsSold = result.Count;
                    record.SearchStatisticsCount = result.Count;
                    record.Mode = result.ID.Substring(0, 1);
                    idByRegionReport.Add(record);
                }
            }

            DateTime blEnd = DateTime.Now;
            ErrorLogging.WriteToLogFile("End Time: " + blEnd.Hour.ToString() + ":" + blEnd.Minute.ToString() + ":" + blEnd.Second.ToString() + ":" + blEnd.Millisecond.ToString(), false);            
            TimeSpan totalBlTime = blEnd - blStart;
            ErrorLogging.WriteToLogFile("Total BL Time: " + totalBlTime.Hours.ToString() + ":" + totalBlTime.Minutes.ToString() + ":" + totalBlTime.Seconds.ToString() + ":" + totalBlTime.Milliseconds.ToString(), false);            
            totalExecTime = totalExecTime + totalBlTime;
            ErrorLogging.WriteToLogFile("Total Time: " + totalExecTime.Hours.ToString() + ":" + totalExecTime.Minutes.ToString() + ":" + totalExecTime.Seconds.ToString() + ":" + totalExecTime.Milliseconds.ToString(), false);                        
            return idByRegionReport;
        }
        public static DataSet GetIDByRegionDataSet(Model.IDByRegionReportModel reportModel, Model.IdSetupCodeInfo setupcodeInfoParams, Model.IDModeRecordModel allModeNames, ref IList<String> m_KeepIdList)
        {
            DataSet selectedIDList = null;
            if (reportModel != null && reportModel.Count > 0)
            {
                selectedIDList = new DataSet("IDSelection");
                selectedIDList.Tables.Add("IDLIST");
                selectedIDList.Tables["IDLIST"].Columns.Add("ID");
                if (setupcodeInfoParams.SortType == UEI.Workflow2010.Report.Model.ReportSortBy.IDNumber)
                    selectedIDList.Tables["IDLIST"].Columns.Add("Model Count");
                else if(setupcodeInfoParams.SortType == UEI.Workflow2010.Report.Model.ReportSortBy.ModelCount)
                    selectedIDList.Tables["IDLIST"].Columns.Add("Model Count");
                else if (setupcodeInfoParams.SortType == UEI.Workflow2010.Report.Model.ReportSortBy.POS)
                    selectedIDList.Tables["IDLIST"].Columns.Add("UnitsSold");
                else if (setupcodeInfoParams.SortType == UEI.Workflow2010.Report.Model.ReportSortBy.SearchStatistics)
                    selectedIDList.Tables["IDLIST"].Columns.Add("Search Count");
                IList<String> ditinctModes = reportModel.GetDistinctModes();
                selectedIDList.Tables["IDLIST"].AcceptChanges();
                foreach (String mode in ditinctModes)
                {
                    String modeName = allModeNames.getModeName(mode);
                    modeName = modeName + " (" + mode + ")";
                    selectedIDList.Tables["IDLIST"].Rows.Add(modeName, "");
                    IList<Model.IDInfo> idList = reportModel.GetIDs(mode, setupcodeInfoParams);
                    //selectedIDList.Tables["IDLIST"].Rows.Add("###Mode-"+ mode.ToUpper() + "###");
                    foreach (Model.IDInfo id in idList)
                    {
                        if (m_KeepIdList.Contains(id.ID))
                        {
                            m_KeepIdList.Remove(id.ID);
                        }
                        if (setupcodeInfoParams.SortType == UEI.Workflow2010.Report.Model.ReportSortBy.ModelCount || setupcodeInfoParams.SortType == UEI.Workflow2010.Report.Model.ReportSortBy.IDNumber)
                            selectedIDList.Tables["IDLIST"].Rows.Add(id.ID,id.ModelCount);
                        else if (setupcodeInfoParams.SortType == UEI.Workflow2010.Report.Model.ReportSortBy.POS)
                            selectedIDList.Tables["IDLIST"].Rows.Add(id.ID, id.TotalUnitsSold);
                        else if (setupcodeInfoParams.SortType == UEI.Workflow2010.Report.Model.ReportSortBy.SearchStatistics)
                            selectedIDList.Tables["IDLIST"].Rows.Add(id.ID, id.SearchStatisticsCount);
                    }
                }
                selectedIDList.AcceptChanges();
            }            
            return selectedIDList;
        }
        public static DataSet GetIDByRegionModeBasedSet(Model.IDByRegionReportModel reportModel, Model.IdSetupCodeInfo setupcodeInfoParams)
        {
            DataSet selectedIDList = null;
            if (reportModel != null && reportModel.Count > 0)
            {
                selectedIDList = new DataSet("IDSelection");
                
                IList<String> ditinctModes = reportModel.GetDistinctModes();
                foreach (String mode in ditinctModes)
                {
                    selectedIDList.Tables.Add(mode);
                    selectedIDList.Tables[mode].Columns.Add("ID");
                    //selectedIDList.Tables[mode].Columns.Add("Model Count");
                    if (setupcodeInfoParams.SortType == UEI.Workflow2010.Report.Model.ReportSortBy.IDNumber)
                        selectedIDList.Tables[mode].Columns.Add("Model Count");
                    else if (setupcodeInfoParams.SortType == UEI.Workflow2010.Report.Model.ReportSortBy.ModelCount)
                        selectedIDList.Tables[mode].Columns.Add("Model Count");
                    else if (setupcodeInfoParams.SortType == UEI.Workflow2010.Report.Model.ReportSortBy.POS)
                        selectedIDList.Tables[mode].Columns.Add("UnitsSold");
                    else if (setupcodeInfoParams.SortType == UEI.Workflow2010.Report.Model.ReportSortBy.SearchStatistics)
                        selectedIDList.Tables[mode].Columns.Add("Search Count");
                    IList<Model.IDInfo> idList = reportModel.GetIDs(mode, setupcodeInfoParams);
                    //selectedIDList.Tables["IDLIST"].Rows.Add("###Mode-"+ mode.ToUpper() + "###");
                    foreach (Model.IDInfo id in idList)
                    {
                        //selectedIDList.Tables[mode].Rows.Add(id.ID,id.ModelCount);
                        if (setupcodeInfoParams.SortType == UEI.Workflow2010.Report.Model.ReportSortBy.ModelCount || setupcodeInfoParams.SortType == UEI.Workflow2010.Report.Model.ReportSortBy.IDNumber)
                            selectedIDList.Tables[mode].Rows.Add(id.ID, id.ModelCount);
                        else if (setupcodeInfoParams.SortType == UEI.Workflow2010.Report.Model.ReportSortBy.POS)
                            selectedIDList.Tables[mode].Rows.Add(id.ID, id.TotalUnitsSold);
                        else if (setupcodeInfoParams.SortType == UEI.Workflow2010.Report.Model.ReportSortBy.SearchStatistics)
                            selectedIDList.Tables[mode].Rows.Add(id.ID, id.SearchStatisticsCount);
                    }
                }

                selectedIDList.AcceptChanges();
            }
            return selectedIDList;
        }
        #endregion

        #region Get Pick Id With Alias ID
        public IList<String> ProcessPicIDWithAlias(IList<String> m_IDList, IList<Model.AliasId> m_AliasIDList)
        {
            foreach (Model.AliasId m_AliasID in m_AliasIDList)
            {
                String m_IDExist = IdExists(m_IDList, m_AliasID.ID);
                if (m_IDExist != null)
                {
                    if (m_IDExist != m_AliasID.AliasID)
                        m_IDExist = m_AliasID.AliasID;
                }
            }
            return m_IDList;
        }
        private String IdExists(IList<String> idList, String id)
        {
            String idExists = null;
            foreach (String item in idList)
            {
                if (item == id)
                    return item;
            }
            return idExists;
        }
        public IList<Model.AliasId> GetPickIdWithAlias(String ProjectName, ArrayList m_PICIDList)
        {
            IList<Model.AliasId> m_IDList = new List<Model.AliasId>();
            try
            {
                IList<Hashtable> m_AliasIDList = PickAccessLayer.Pick2DAOFactory.Pick2DAO().GetPickIdWithAlias(ProjectName, m_PICIDList);
                if (m_AliasIDList.Count > 0)
                {
                    foreach (Hashtable ht in m_AliasIDList)
                    {
                        Model.AliasId m_ID = new Model.AliasId(ht["ID"].ToString(), ht["FromID"].ToString());
                        m_IDList.Add(m_ID);
                    }
                }
            }
            catch { }
            return m_IDList;
        }
        #endregion

        #region Key Function Report
        public Model.KeyFunctionModel GetKeyFunctionReport(IList<Model.Id> ids, IList<Model.Region> locations, List<String> selectedModes, IList<Model.DeviceType> devices, IList<Model.DataSources> dataSources, Model.IdSetupCodeInfo idSetupCodeInfoParam, int keyFunctionReportType)
        {
            IDSearchParameter searchParam = null;
            try
            {               
                searchParam = new IDSearchParameter();
                StringBuilder paramBuilder = new StringBuilder();
                String idStr = String.Empty;
                foreach (Model.Id id in ids)
                    paramBuilder.Append(id.ID + ",");
                if (paramBuilder.Length > 0)
                    paramBuilder.Remove(paramBuilder.Length - 1, 1);
                idStr = paramBuilder.ToString();

                //add locations xml
                String strLocations = String.Empty;
                if (locations != null)
                {
                    _XML = new XMLOperations("UEI2Server", XMLOperations.EncodingType.UTF16);
                    _XML.ParentNode = "Document";
                    _XML.ParentAttributeName = "Type";
                    _XML.ParentAttributeValue = "LocationSelections";
                    _XML.ParentAttributeName = "Version";
                    _XML.ParentAttributeValue = "1.0.0";
                    _XML.AppendParent();

                    foreach (Model.Region r in locations)
                    {
                        _XML.ParentNode = "Region";
                        _XML.ParentAttributeName = "Name";
                        _XML.ParentAttributeValue = r.Name;
                        _XML.ParentAttributeName = "Weight";
                        _XML.ParentAttributeValue = "0";
                        _XML.ParentAttributeName = "SystemFlags";
                        if (idSetupCodeInfoParam.IncludeNonCountryLinkedRecords == true)
                        {
                            _XML.ParentAttributeValue = "2";
                        }
                        else
                        {
                            _XML.ParentAttributeValue = "0";
                        }
                        if (r.SubRegions != null)
                        {
                            foreach (Model.Region subregion in r.SubRegions)
                            {
                                _XML.ChildNode = "SubRegion";
                                _XML.ChildAttributeName = "Name";
                                _XML.ChildAttributeValue = subregion.Name;
                                _XML.ChildAttributeName = "Weight";
                                _XML.ChildAttributeValue = "0";
                                _XML.ChildAttributeName = "SystemFlags";
                                _XML.ChildAttributeValue = "0";
                                if (r.Countries != null)
                                {
                                    foreach (Model.Country country in r.Countries)
                                    {
                                        if (subregion.Name == country.SubRegion)
                                        {
                                            _XML.ChildNode = "Country";
                                            _XML.ChildAttributeName = "Name";
                                            _XML.ChildAttributeValue = country.Name;
                                            _XML.ChildAttributeName = "Weight";
                                            _XML.ChildAttributeValue = "0";
                                            _XML.ChildAttributeName = "SystemFlags";
                                            _XML.ChildAttributeValue = "0";
                                            _XML.AppendChild();
                                        }
                                    }
                                }
                                _XML.AppendChild();
                            }
                        }
                        if (r.Countries != null)
                        {
                            foreach (Model.Country country in r.Countries)
                            {
                                if (country.SubRegion == String.Empty)
                                {
                                    _XML.ChildNode = "Country";
                                    _XML.ChildAttributeName = "Name";
                                    _XML.ChildAttributeValue = country.Name;
                                    _XML.ChildAttributeName = "Weight";
                                    _XML.ChildAttributeValue = "0";
                                    _XML.ChildAttributeName = "SystemFlags";
                                    _XML.ChildAttributeValue = "0";
                                    _XML.AppendChild();
                                }
                            }
                        }
                        _XML.AppendParent();
                    }
                }
                strLocations = _XML.XMLGenerator();
                _XML = null;

                //add device xml
                String strDevices = String.Empty;
                if (devices != null)
                {
                    _XML = new XMLOperations("UEI2Server", XMLOperations.EncodingType.UTF16);
                    _XML.ParentNode = "Document";
                    _XML.ParentAttributeName = "Type";
                    _XML.ParentAttributeValue = "DeviceSelections";
                    _XML.ParentAttributeName = "Version";
                    _XML.ParentAttributeValue = "1.0.0";
                    _XML.AppendParent();

                    foreach (String mode in selectedModes)
                    {
                        _XML.ParentNode = "DeviceMode";
                        _XML.ParentAttributeName = "Name";
                        _XML.ParentAttributeValue = mode;
                        _XML.ParentAttributeName = "SystemFlags";
                        //if (idSetupCodeInfoParam.IncludeEmptyDeviceType == true)
                        //    _XML.ParentAttributeValue = "3";
                        //else if (idSetupCodeInfoParam.IncludeEmptyDeviceType == false)
                        //    _XML.ParentAttributeValue = "1";
                        if (idSetupCodeInfoParam.IncludeNonComponentLinkedRecords == true && idSetupCodeInfoParam.IncludeEmptyDeviceType == true)
                            _XML.ParentAttributeValue = "7";
                        else if (idSetupCodeInfoParam.IncludeNonComponentLinkedRecords == true && idSetupCodeInfoParam.IncludeEmptyDeviceType == false)
                            _XML.ParentAttributeValue = "4";
                        else if (idSetupCodeInfoParam.IncludeNonComponentLinkedRecords == false && idSetupCodeInfoParam.IncludeEmptyDeviceType == true)
                            _XML.ParentAttributeValue = "3";
                        else if (idSetupCodeInfoParam.IncludeNonComponentLinkedRecords == false && idSetupCodeInfoParam.IncludeEmptyDeviceType == false)
                            _XML.ParentAttributeValue = "1";
                        foreach (Model.DeviceType device in devices)
                        {
                            if (device.Mode == mode)
                            {
                                _XML.ChildNode = "MainDevices";
                                _XML.ChildAttributeName = "Name";
                                _XML.ChildAttributeValue = device.Name;
                                _XML.ChildAttributeName = "SystemFlags";
                                _XML.ChildAttributeValue = "1";
                                if (device.SubDevices != null)
                                {
                                    foreach (Model.DeviceType subdevice in device.SubDevices)
                                    {
                                        _XML.ChildNode = "SubDevices";
                                        _XML.ChildAttributeName = "Name";
                                        _XML.ChildAttributeValue = subdevice.Name;
                                        _XML.ChildAttributeName = "SystemFlags";
                                        _XML.ChildAttributeValue = "1";
                                        if (device.Components != null)
                                        {
                                            foreach (Model.Component component in device.Components)
                                            {
                                                if (subdevice.Name == component.SubDeviceType)
                                                {
                                                    _XML.ChildNode = "Component";
                                                    _XML.ChildAttributeName = "Name";
                                                    _XML.ChildAttributeValue = component.Name;
                                                    _XML.ChildAttributeName = "SystemFlags";
                                                    _XML.ChildAttributeValue = "0";
                                                    _XML.ChildAttributeName = "SystemFlagsExt";
                                                    _XML.ChildAttributeValue = "1";
                                                    _XML.AppendChild();
                                                }
                                            }
                                        }
                                        _XML.AppendChild();
                                    }
                                }
                                if (device.Components != null)
                                {
                                    foreach (Model.Component component in device.Components)
                                    {
                                        if (component.SubDeviceType == String.Empty)
                                        {
                                            _XML.ChildNode = "Component";
                                            _XML.ChildAttributeName = "Name";
                                            _XML.ChildAttributeValue = component.Name;
                                            _XML.ChildAttributeName = "SystemFlags";
                                            _XML.ChildAttributeValue = "0";
                                            _XML.ChildAttributeName = "SystemFlagsExt";
                                            _XML.ChildAttributeValue = "1";
                                            _XML.AppendChild();
                                        }
                                    }
                                }
                                _XML.AppendChild();
                            }
                        }
                        _XML.AppendParent();
                    }
                }
                strDevices = _XML.XMLGenerator();
                _XML = null;

                paramBuilder = new StringBuilder();
                String dataSourceStr = String.Empty;
                if (dataSources != null)
                {
                    foreach (Model.DataSources l in dataSources)
                        paramBuilder.Append(l.Name + ",");
                    if (paramBuilder.Length > 0)
                        paramBuilder.Remove(paramBuilder.Length - 1, 1);

                    dataSourceStr = paramBuilder.ToString();
                }
                searchParam.Ids = idStr;
                searchParam.XMLLocations = strLocations;
                searchParam.XMLDevices = strDevices;
                searchParam.DataSources = dataSourceStr;
                searchParam.IncludeRestrictedIDs = idSetupCodeInfoParam.IncludeRestrictedIds;                
                searchParam.IncludeUnknownLocation = (idSetupCodeInfoParam.IncludeUnknownLocation == true) ? 1 : 0;
                searchParam.SearchFlag = keyFunctionReportType.ToString();

                ErrorLogging.WriteToLogFile("--Function Name: GetKeyFunctionReport--", false);
                ErrorLogging.WriteToLogFile("UEI2_Sp_KeyFunction Input Parameters", false);
                ErrorLogging.WriteToLogFile("##########################################################", false);
                ErrorLogging.WriteToLogFile("####################### Input ############################", false);
                ErrorLogging.WriteToLogFile("##########################################################", false);
                ErrorLogging.WriteToLogFile("IDS            :" + idStr, false);
                ErrorLogging.WriteToLogFile("LOCATION XML   :" + strLocations, false);
                ErrorLogging.WriteToLogFile("DEVICE XML     :" + strDevices, false);
                ErrorLogging.WriteToLogFile("DATASOURCE     :" + dataSourceStr, false);
                ErrorLogging.WriteToLogFile("##########################################################", false);

                DateTime spStart = DateTime.Now;
                ErrorLogging.WriteToLogFile("UEI2_Sp_KeyFunction execution time", false);
                ErrorLogging.WriteToLogFile("Start Time: " + spStart.Hour.ToString() + ":" + spStart.Minute.ToString() + ":" + spStart.Second.ToString() + ":" + spStart.Millisecond.ToString(), false);

                HashtableCollection resultCollection = _DBAccess.GetKeyFunction(searchParam);

                DateTime spEnd = DateTime.Now;
                ErrorLogging.WriteToLogFile("End Time: " + spEnd.Hour.ToString() + ":" + spEnd.Minute.ToString() + ":" + spEnd.Second.ToString() + ":" + spEnd.Millisecond.ToString(), false);

                totalExecTime = spEnd - spStart;
                ErrorLogging.WriteToLogFile("Total sp Time: " + totalExecTime.Hours.ToString() + ":" + totalExecTime.Minutes.ToString() + ":" + totalExecTime.Seconds.ToString() + ":" + totalExecTime.Milliseconds.ToString(), false);

                if (resultCollection != null)
                {
                    return CreateKeyFunctionModel(resultCollection);
                }
                return null;
            }
            catch (Exception ex)
            {
                ErrorLogging.WriteToLogFile("---Exception raised---", false);
                ErrorLogging.WriteToErrorLogFile(ex, searchParam);

                return null;
            }
        }
        private Model.KeyFunctionModel CreateKeyFunctionModel(HashtableCollection resultCollection)
        {
            Model.KeyFunctionModel m_Result = null;
            foreach (Hashtable item in resultCollection)
            {
                if (m_Result == null)
                    m_Result = new Model.KeyFunctionModel();

                Model.KeyFunctionRecord record = new Model.KeyFunctionRecord();
                if (item.ContainsKey("ID"))
                    record.ID = item["ID"].ToString();
                if (item.ContainsKey("Function"))
                    record.Function = item["Function"].ToString();
                if (item.ContainsKey("MainDevice"))
                    record.MainDevice = item["MainDevice"].ToString();
                if (item.ContainsKey("SubDevice"))
                    record.SubDevice = item["SubDevice"].ToString();
                if (item.ContainsKey("Count"))
                    record.FunctionCount = Convert.ToInt32(item["Count"].ToString());
                if (m_Result.KeyFunctions.ContainsKey(item["Mode"].ToString()))
                {
                    if (!String.IsNullOrEmpty(record.Function))
                    {
                        List<Model.KeyFunctionRecord> records = m_Result.KeyFunctions[item["Mode"].ToString()];
                        records.Add(record);
                    }
                }
                else
                {
                    if (!String.IsNullOrEmpty(record.Function))
                    {
                        List<Model.KeyFunctionRecord> records = new List<Model.KeyFunctionRecord>();
                        records.Add(record);
                        m_Result.KeyFunctions.Add(item["Mode"].ToString(), records);
                    }
                }
            }                       
            return m_Result;
        }
        #endregion

        #region BSC
        public Model.IDBSCReportModel GetBSCIDList(IList<Model.Id> ids, IList<Model.Region> locations, String DeviceAliasRegion, List<String> selectedModes, IList<Model.DeviceType> devices, IList<Model.DataSources> dataSources, Model.IdSetupCodeInfo idSetupCodeInfoParam)
        {
            IDSearchParameter searchParam = null;
            try
            {
                //Add replaces Identical Ids to the the id list
                if (idSetupCodeInfoParam.IdenticalIdList != null && idSetupCodeInfoParam.IncludeIdenticalIdList == true)
                {
                    foreach (Model.AliasId identicalId in idSetupCodeInfoParam.IdenticalIdList)
                    {
                        if (identicalId.AliasID.Length > 0)
                        {
                            String[] idList = identicalId.AliasID.Split(',');
                            foreach (String id in idList)
                            {
                                Model.Id idInfo = getIDInfo(ids, id);
                                if (idInfo == null)
                                {
                                    ids.Add(new Model.Id(id));
                                }
                            }
                        }
                    }
                }

                searchParam = new IDSearchParameter();
                StringBuilder paramBuilder = new StringBuilder();
                String idStr = String.Empty;
                foreach (Model.Id id in ids)
                    paramBuilder.Append(id.ID + ",");
                if (paramBuilder.Length > 0)
                    paramBuilder.Remove(paramBuilder.Length - 1, 1);
                idStr = paramBuilder.ToString();

                //add locations xml
                String strLocations = String.Empty;
                if (locations != null)
                {
                    _XML = new XMLOperations("UEI2Server", XMLOperations.EncodingType.UTF16);
                    _XML.ParentNode = "Document";
                    _XML.ParentAttributeName = "Type";
                    _XML.ParentAttributeValue = "LocationSelections";
                    _XML.ParentAttributeName = "Version";
                    _XML.ParentAttributeValue = "1.0.0";
                    _XML.AppendParent();

                    foreach (Model.Region r in locations)
                    {
                        _XML.ParentNode = "Region";
                        _XML.ParentAttributeName = "Name";
                        _XML.ParentAttributeValue = r.Name;
                        _XML.ParentAttributeName = "Weight";
                        _XML.ParentAttributeValue = "0";
                        _XML.ParentAttributeName = "SystemFlags";
                        if (idSetupCodeInfoParam.IncludeNonCountryLinkedRecords == true)
                        {
                            _XML.ParentAttributeValue = "2";
                        }
                        else
                        {
                            _XML.ParentAttributeValue = "0";
                        }
                        if (r.SubRegions != null)
                        {
                            foreach (Model.Region subregion in r.SubRegions)
                            {
                                _XML.ChildNode = "SubRegion";
                                _XML.ChildAttributeName = "Name";
                                _XML.ChildAttributeValue = subregion.Name;
                                _XML.ChildAttributeName = "Weight";
                                _XML.ChildAttributeValue = "0";
                                _XML.ChildAttributeName = "SystemFlags";
                                _XML.ChildAttributeValue = "0";
                                if (r.Countries != null)
                                {
                                    foreach (Model.Country country in r.Countries)
                                    {
                                        if (subregion.Name == country.SubRegion)
                                        {
                                            _XML.ChildNode = "Country";
                                            _XML.ChildAttributeName = "Name";
                                            _XML.ChildAttributeValue = country.Name;
                                            _XML.ChildAttributeName = "Weight";
                                            _XML.ChildAttributeValue = "0";
                                            _XML.ChildAttributeName = "SystemFlags";
                                            _XML.ChildAttributeValue = "0";
                                            _XML.AppendChild();
                                        }
                                    }
                                }
                                _XML.AppendChild();
                            }
                        }
                        if (r.Countries != null)
                        {
                            foreach (Model.Country country in r.Countries)
                            {
                                if (country.SubRegion == String.Empty)
                                {
                                    _XML.ChildNode = "Country";
                                    _XML.ChildAttributeName = "Name";
                                    _XML.ChildAttributeValue = country.Name;
                                    _XML.ChildAttributeName = "Weight";
                                    _XML.ChildAttributeValue = "0";
                                    _XML.ChildAttributeName = "SystemFlags";
                                    _XML.ChildAttributeValue = "0";
                                    _XML.AppendChild();
                                }
                            }
                        }
                        _XML.AppendParent();
                    }
                }
                strLocations = _XML.XMLGenerator();
                _XML = null;

                //add device xml
                String strDevices = String.Empty;
                if (devices != null)
                {
                    _XML = new XMLOperations("UEI2Server", XMLOperations.EncodingType.UTF16);
                    _XML.ParentNode = "Document";
                    _XML.ParentAttributeName = "Type";
                    _XML.ParentAttributeValue = "DeviceSelections";
                    _XML.ParentAttributeName = "Version";
                    _XML.ParentAttributeValue = "1.0.0";
                    _XML.AppendParent();

                    foreach (String mode in selectedModes)
                    {
                        _XML.ParentNode = "DeviceMode";
                        _XML.ParentAttributeName = "Name";
                        _XML.ParentAttributeValue = mode;
                        _XML.ParentAttributeName = "SystemFlags";
                        //if (idSetupCodeInfoParam.IncludeEmptyDeviceType == true)
                        //    _XML.ParentAttributeValue = "3";
                        //else if (idSetupCodeInfoParam.IncludeEmptyDeviceType == false)
                        //    _XML.ParentAttributeValue = "1";
                        if(idSetupCodeInfoParam.IncludeNonComponentLinkedRecords == true && idSetupCodeInfoParam.IncludeEmptyDeviceType == true)
                            _XML.ParentAttributeValue = "7";
                        else if(idSetupCodeInfoParam.IncludeNonComponentLinkedRecords == true && idSetupCodeInfoParam.IncludeEmptyDeviceType == false)
                            _XML.ParentAttributeValue = "4";
                        else if(idSetupCodeInfoParam.IncludeNonComponentLinkedRecords == false && idSetupCodeInfoParam.IncludeEmptyDeviceType == true)
                            _XML.ParentAttributeValue = "3";
                        else if(idSetupCodeInfoParam.IncludeNonComponentLinkedRecords == false && idSetupCodeInfoParam.IncludeEmptyDeviceType == false)
                            _XML.ParentAttributeValue = "1";
                        foreach (Model.DeviceType device in devices)
                        {
                            if (device.Mode == mode)
                            {
                                _XML.ChildNode = "MainDevices";
                                _XML.ChildAttributeName = "Name";
                                _XML.ChildAttributeValue = device.Name;
                                _XML.ChildAttributeName = "SystemFlags";
                                _XML.ChildAttributeValue = "1";
                                if (device.SubDevices != null)
                                {
                                    foreach (Model.DeviceType subdevice in device.SubDevices)
                                    {
                                        _XML.ChildNode = "SubDevices";
                                        _XML.ChildAttributeName = "Name";
                                        _XML.ChildAttributeValue = subdevice.Name;
                                        _XML.ChildAttributeName = "SystemFlags";
                                        _XML.ChildAttributeValue = "1";
                                        if (device.Components != null)
                                        {
                                            foreach (Model.Component component in device.Components)
                                            {
                                                if (subdevice.Name == component.SubDeviceType)
                                                {
                                                    _XML.ChildNode = "Component";
                                                    _XML.ChildAttributeName = "Name";
                                                    _XML.ChildAttributeValue = component.Name;
                                                    _XML.ChildAttributeName = "SystemFlags";
                                                    _XML.ChildAttributeValue = "0";
                                                    _XML.ChildAttributeName = "SystemFlagsExt";
                                                    _XML.ChildAttributeValue = "1";
                                                    _XML.AppendChild();
                                                }
                                            }
                                        }
                                        _XML.AppendChild();
                                    }
                                }
                                if (device.Components != null)
                                {
                                    foreach (Model.Component component in device.Components)
                                    {
                                        if (component.SubDeviceType == String.Empty)
                                        {
                                            _XML.ChildNode = "Component";
                                            _XML.ChildAttributeName = "Name";
                                            _XML.ChildAttributeValue = component.Name;
                                            _XML.ChildAttributeName = "SystemFlags";
                                            _XML.ChildAttributeValue = "0";
                                            _XML.ChildAttributeName = "SystemFlagsExt";
                                            _XML.ChildAttributeValue = "1";
                                            _XML.AppendChild();
                                        }
                                    }
                                }
                                _XML.AppendChild();
                            }
                        }
                        _XML.AppendParent();
                    }
                }
                strDevices = _XML.XMLGenerator();
                _XML = null;

                paramBuilder = new StringBuilder();
                String dataSourceStr = String.Empty;
                if (dataSources != null)
                {
                    foreach (Model.DataSources l in dataSources)
                        paramBuilder.Append(l.Name + ",");
                    if (paramBuilder.Length > 0)
                        paramBuilder.Remove(paramBuilder.Length - 1, 1);

                    dataSourceStr = paramBuilder.ToString();
                }
                searchParam.Ids = idStr;
                searchParam.XMLLocations                    = strLocations;
                searchParam.XMLDevices                      = strDevices;
                searchParam.DeviceAliasRegion               = DeviceAliasRegion;
                searchParam.DataSources                     = dataSourceStr;
                searchParam.IncludeRestrictedIDs            = idSetupCodeInfoParam.IncludeRestrictedIds;
                searchParam.DisplayMajorBrandsOnly          = idSetupCodeInfoParam.DisplayMajorBrandsOnly;
                searchParam.IncludeAliasBrand               = (idSetupCodeInfoParam.InclideAliasBrand) ? 1 : 0;
                searchParam.IncudeRecordWithoutModelName    = (idSetupCodeInfoParam.IncludeBlankModelNames) ? 1 : 0;
                searchParam.ModelType                       = idSetupCodeInfoParam.ModelType;
                searchParam.IncludeUnknownLocation          = (idSetupCodeInfoParam.IncludeUnknownLocation == true) ? 1 : 0;

                ErrorLogging.WriteToLogFile("--Function Name: GetBSCIDList--", false);
                ErrorLogging.WriteToLogFile("UEI2_Sp_BSCbyBrand Input Parameters", false);
                ErrorLogging.WriteToLogFile("##########################################################", false);
                ErrorLogging.WriteToLogFile("####################### Input ############################", false);
                ErrorLogging.WriteToLogFile("##########################################################", false);
                ErrorLogging.WriteToLogFile("IDS            :" + idStr, false);
                ErrorLogging.WriteToLogFile("LOCATION XML   :" + strLocations, false);
                ErrorLogging.WriteToLogFile("DEVICE XML     :" + strDevices, false);
                ErrorLogging.WriteToLogFile("DATASOURCE     :" + dataSourceStr, false);
                ErrorLogging.WriteToLogFile("##########################################################", false);

                DateTime spStart = DateTime.Now;
                ErrorLogging.WriteToLogFile("UEI2_Sp_BSCbyBrand execution time", false);
                ErrorLogging.WriteToLogFile("Start Time: " + spStart.Hour.ToString() + ":" + spStart.Minute.ToString() + ":" + spStart.Second.ToString() + ":" + spStart.Millisecond.ToString(), false);

                IDBrandResultCollection resultCollection = _DBAccess.GetAllBSCIDList(searchParam);

                DateTime spEnd = DateTime.Now;
                ErrorLogging.WriteToLogFile("End Time: " + spEnd.Hour.ToString() + ":" + spEnd.Minute.ToString() + ":" + spEnd.Second.ToString() + ":" + spEnd.Millisecond.ToString(), false);
                
                totalExecTime = spEnd - spStart;
                ErrorLogging.WriteToLogFile("Total sp Time: " + totalExecTime.Hours.ToString() + ":" + totalExecTime.Minutes.ToString() + ":" + totalExecTime.Seconds.ToString() + ":" + totalExecTime.Milliseconds.ToString(), false);

                if (resultCollection != null)
                {
                    //Revert Back Alias ID Replacement
                    if(idSetupCodeInfoParam.AliasIDList != null)
                    {
                        resultCollection = ProcessIDWithAlias(resultCollection, idSetupCodeInfoParam);
                    }
                    resultCollection.Sort(true, "Brand");
                    return GetBSCBrandIDReport(resultCollection);
                }
                return null;
            }
            catch(Exception ex)
            {
                ErrorLogging.WriteToLogFile("---Exception raised---", false);
                ErrorLogging.WriteToErrorLogFile(ex, searchParam);
                
                return null;
            }            
        }
        private IDBrandResultCollection ProcessIDWithAlias(IDBrandResultCollection resultCollection, Model.IdSetupCodeInfo setupcodeInfoParams)
        {
            IDBrandResultCollection newresultCollection = new IDBrandResultCollection();
            foreach(IDBrandResult m_AliasID in resultCollection)
            {
                List<Model.AliasId> m_IDExist = IdExists(setupcodeInfoParams.AliasIDList, m_AliasID.ID);
                if(m_IDExist.Count > 0)
                {
                    foreach(Model.AliasId Item in m_IDExist)
                    {
                        IDBrandResult m_Item = new IDBrandResult();
                        m_Item.ID = Item.AliasID;                                            
                        m_Item.Availability = m_AliasID.Availability;
                        m_Item.Brand = m_AliasID.Brand;                        
                        m_Item.AliasBrand = m_AliasID.AliasBrand;
                        m_Item.AliasType = m_AliasID.AliasType;
                        m_Item.BrandNumber = m_AliasID.BrandNumber;
                        m_Item.Comment = m_AliasID.Comment;
                        m_Item.Count = m_AliasID.Count;
                        m_Item.Country = m_AliasID.Country;
                        m_Item.DataSource = m_AliasID.DataSource;
                        m_Item.DeviceType = m_AliasID.DeviceType;
                        m_Item.Mode = m_AliasID.Mode;
                        m_Item.Model = m_AliasID.Model;
                        m_Item.ModelCount = m_AliasID.ModelCount;
                        m_Item.Region = m_AliasID.Region;
                        m_Item.TN = m_AliasID.TN;
                        m_Item.TRModel = m_AliasID.TRModel;
                        m_Item.MainDevice = m_AliasID.MainDevice;
                        m_Item.RegionalName = m_AliasID.RegionalName;
                        m_Item.SubDevice = m_AliasID.SubDevice;
                        m_Item.SubRegion = m_AliasID.SubRegion;
                        m_Item.TrustLevel = m_AliasID.TrustLevel;
                        m_Item.DeviceTypeFlag = m_AliasID.DeviceTypeFlag;
                        m_Item.Component = m_AliasID.Component;
                        m_Item.ComponentStatus = m_AliasID.ComponentStatus;                        
                        newresultCollection.Add(m_Item);
                        m_Item = null;
                    }
                }
                else
                {
                    newresultCollection.Add(m_AliasID);
                }
            }
            return newresultCollection;
        }
        private List<Model.AliasId> IdExists(IList<Model.AliasId> idList, String id)
        {
            List<Model.AliasId> idExists = new List<Model.AliasId>();
            foreach(Model.AliasId item in idList)
            {
                if(item.ID == id)
                {
                    idExists.Add(item);
                }
            }
            return idExists;
        }
        private Model.IDBSCReportModel GetBSCBrandIDReport(IDBrandResultCollection resultCollection)
        {

            ErrorLogging.WriteToLogFile("--Function Name: GetBSCBrandIDReport--", false);
            ErrorLogging.WriteToLogFile("Business logic execution time", false);
            DateTime blStart = DateTime.Now;
            ErrorLogging.WriteToLogFile("Start Time: " + blStart.Hour.ToString() + ":" + blStart.Minute.ToString() + ":" + blStart.Second.ToString() + ":" + blStart.Millisecond.ToString(), false);
            
            Model.IDBSCReportModel brandidReport = null;
            if (resultCollection != null)
            {
                brandidReport = new Model.IDBSCReportModel();
                foreach (IDBrandResult result in resultCollection)
                {
                    //Result Collection contains brand information for all input ids
                    //Brand name with 0 treated as not valid ids for the selected criteria.
                    if (result.Brand != "0")
                    {
                        String mode = result.ID.Substring(0, 1);
                        Model.IDBSCReportRecords record = brandidReport.GetBrandIDRecords(result.Brand,result.DeviceTypeFlag, mode);
                        if (record == null)
                        {
                            record = new Model.IDBSCReportRecords();                            
                            //Add model list if identical id need to be handled in application level
                            //not yet implemented

                            //Add id
                            Model.IDInfo idInfo = new Model.IDInfo();
                            idInfo.ID = result.ID;
                            idInfo.LowPriorityID = result.LowPriorityID;
                            idInfo.ModelCount = result.ModelCount;

                            //Add device type
                            Model.DeviceInfo deviceInfo = new Model.DeviceInfo();
                            //deviceInfo.Name = result.DeviceType;
                            deviceInfo.MainDevice = result.MainDevice;
                            deviceInfo.SubDevice = result.SubDevice;
                            deviceInfo.Component = result.Component;
                            deviceInfo.RegionalName = result.RegionalName;
                            deviceInfo.IdList.Add(idInfo);

                            //Add brand to record collection
                            record.Mode = mode;
                            record.Brand = result.Brand;
                            record.DeviceTypeFlag = result.DeviceTypeFlag;
                            record.BrandNumber = result.BrandNumber;
                            record.DeviceList.Add(deviceInfo);
                           
                            brandidReport.Add(record);
                        }
                        else
                        {
                            //check if device type exists for the same brand
                            //if not append new device alse update id to existing device
                            Model.DeviceInfo deviceInfo = GetDeviceInfo(record.DeviceList, result.MainDevice,result.SubDevice,result.Component);
                            if (deviceInfo == null)
                            {                                
                                Model.IDInfo idInfo = new Model.IDInfo();
                                idInfo.ID = result.ID;
                                idInfo.LowPriorityID = result.LowPriorityID;
                                idInfo.ModelCount = result.ModelCount;

                                deviceInfo = new Model.DeviceInfo();
                                //deviceInfo.Name = result.DeviceType;
                                deviceInfo.MainDevice = result.MainDevice;
                                deviceInfo.SubDevice = result.SubDevice;
                                deviceInfo.Component = result.Component;
                                deviceInfo.RegionalName = result.RegionalName;
                                deviceInfo.IdList.Add(idInfo);
                                record.DeviceList.Add(deviceInfo);
                            }
                            else
                            {
                                //check if id exits for the same device and brand
                                //if exists increase model count else add new id to the device
                                Model.IDInfo idInfo = getIDInfo((List<Model.IDInfo>)deviceInfo.IdList, result.ID);
                                if (idInfo == null)
                                {
                                    idInfo = new Model.IDInfo();
                                    idInfo.ID = result.ID;
                                    idInfo.LowPriorityID = result.LowPriorityID;
                                    idInfo.ModelCount = result.ModelCount;
                                    deviceInfo.IdList.Add(idInfo);
                                }
                                else
                                {
                                    idInfo.ModelCount = idInfo.ModelCount + result.ModelCount;
                                }
                            }
                        }
                    }
                }
            }

            DateTime blEnd = DateTime.Now;
            ErrorLogging.WriteToLogFile("End Time: " + blEnd.Hour.ToString() + ":" + blEnd.Minute.ToString() + ":" + blEnd.Second.ToString() + ":" + blEnd.Millisecond.ToString(), false);
            
            TimeSpan totalBlTime = blEnd - blStart;
            ErrorLogging.WriteToLogFile("Total BL Time: " + totalBlTime.Hours.ToString() + ":" + totalBlTime.Minutes.ToString() + ":" + totalBlTime.Seconds.ToString() + ":" + totalBlTime.Milliseconds.ToString(), false);
            
            totalExecTime = totalExecTime + totalBlTime;
            ErrorLogging.WriteToLogFile("Total Time: " + totalExecTime.Hours.ToString() + ":" + totalExecTime.Minutes.ToString() + ":" + totalExecTime.Seconds.ToString() + ":" + totalExecTime.Milliseconds.ToString(), false);            
            return brandidReport;
        }
        private Model.DeviceInfo GetDeviceInfo(IList<Model.DeviceInfo> deviceList, String mainDevice, String subDevice, String component)
        {
            foreach (Model.DeviceInfo deviceInfo in deviceList)
            {
                if (deviceInfo.MainDevice == mainDevice && deviceInfo.SubDevice == subDevice && deviceInfo.Component == component)
                    return deviceInfo;
            }
            return null;
        }
        private Model.ModelInfo GetModelInfo(IList<Model.ModelInfo> modelList, String model)
        {
            foreach (Model.ModelInfo modelInfo in modelList)
            {
                if (modelInfo.Name == model)
                    return modelInfo;
            }
            return null;
        }
        public IList<String> GetDeviceGroupList()
        {
            try
            {
                return _DBAccess.GetDeviceGroupList();
            }            
            catch (Exception ex)
            {
                ErrorLogging.WriteToLogFile("---Exception raised in Function GetDeviceGroupList ---", false);
                ErrorLogging.WriteToErrorLogFile(ex);
                return null;
            } 
        }
        public IList<String> GetModeListForDeviceGroup(String deviceGroup)
        {
            try
            {
                return _DBAccess.GetModeListForDeviceGroup(deviceGroup);
            }            
            catch (Exception ex)
            {
                ErrorLogging.WriteToLogFile("---Exception raised in Function GetModeListForDeviceGroup ---", false);
                ErrorLogging.WriteToErrorLogFile(ex);
                return null;
            } 
        }
        public Model.IDModeRecordModel GetAllModeNames()
        {            
            try
            {
                Model.IDModeRecordModel resultModeNames = new Model.IDModeRecordModel();
                ErrorLogging.WriteToLogFile("--Function Name: GetAllModeNames --", false);
                DateTime spStart = DateTime.Now;
                ErrorLogging.WriteToLogFile("mode_lst_english execution time", false);
                ErrorLogging.WriteToLogFile("Start Time: " + spStart.Hour.ToString() + ":" + spStart.Minute.ToString() + ":" + spStart.Second.ToString() + ":" + spStart.Millisecond.ToString(), false);

                HashtableCollection allModeNames = _DBAccess.GetAllModeNames(null, "");

                foreach (Hashtable mode in allModeNames)
                {
                    resultModeNames.Add(new Model.IDModeRecord(mode["Mode"].ToString(), mode["Name"].ToString()));
                }
                DateTime spEnd = DateTime.Now;
                ErrorLogging.WriteToLogFile("End Time: " + spEnd.Hour.ToString() + ":" + spEnd.Minute.ToString() + ":" + spEnd.Second.ToString() + ":" + spEnd.Millisecond.ToString(), false);

                totalExecTime = spEnd - spStart;
                ErrorLogging.WriteToLogFile("Total sp Time: " + totalExecTime.Hours.ToString() + ":" + totalExecTime.Minutes.ToString() + ":" + totalExecTime.Seconds.ToString() + ":" + totalExecTime.Milliseconds.ToString(), false);
                return resultModeNames;
            }
            catch (Exception ex)
            {
                ErrorLogging.WriteToLogFile("---Exception raised in Function GetAllModeNames ---", false);
                ErrorLogging.WriteToErrorLogFile(ex);
                return null;
            }            
        }
        public static DataSet GetBSCReportDataSet(Model.IDBSCReportModel reportModel, Model.IdSetupCodeInfo setupcodeInfoParams, Model.IDModeRecordModel allModeNames, ref IList<String> m_KeepIdList)
        {
            DataSet reportDataSet = null;
            if (reportModel != null && reportModel.Count > 0)
            {
                reportDataSet = new DataSet();
                reportDataSet.Tables.Add("BSCList");
                reportDataSet.Tables["BSCList"].Columns.Add("Brand");
                if(setupcodeInfoParams.DisplayBrandNumber)
                {
                    reportDataSet.Tables["BSCList"].Columns.Add("Brand Number");
                }
                reportDataSet.Tables["BSCList"].Columns.Add("ID");
                IList<String> distinctModes = reportModel.GetDistinctModes();
                foreach (String mode in distinctModes)
                {
                    DataRow dr = reportDataSet.Tables["BSCList"].NewRow();
                    String modeName = allModeNames.getModeName(mode);
                    //dr[0] = mode;
                    dr[0] = modeName + " ("+ mode +")";
                    reportDataSet.Tables["BSCList"].Rows.Add(dr);
                    dr = null;

                    Model.IDBSCReportModel model = reportModel.GetBrandIDModel(mode, "N");
                    foreach (Model.IDBSCReportRecords record in model)
                    {                        
                        dr = reportDataSet.Tables["BSCList"].NewRow();
                        dr["Brand"] = record.Brand;
                        if (setupcodeInfoParams.DisplayBrandNumber)
                        {
                            dr["Brand Number"] = String.Format("{0:0000}", record.BrandNumber);
                        }
                        List<Model.IDInfo> idList = record.GetBSCIDList(record.Brand);
                        //dr["ID"] = GetSortedIDs((List<Model.IDInfo>)record.IDList, setupcodeInfoParams, record.Brand);
                        dr["ID"] = GetSortedIDs(idList, setupcodeInfoParams, record.Brand,ref m_KeepIdList);
                        reportDataSet.Tables["BSCList"].Rows.Add(dr);
                        dr = null;
                    }
                }
                reportDataSet.Tables["BSCList"].AcceptChanges();
            }
            return reportDataSet;
        }
        public static DataSet GetBSCReportModeBasedSet(Model.IDBSCReportModel reportModel, Model.IdSetupCodeInfo setupcodeInfoParams, Model.DeviceCategoryRecords deviceCategories, Boolean subdeviceFlag, ref IList<String> m_KeepIdList)
        {
            if(subdeviceFlag == true)
                return GetBSCReportModeBasedSetWithSubDevices(reportModel,setupcodeInfoParams,deviceCategories,ref m_KeepIdList);
            else
                return GetBSCReportModeBasedSetWithoutSubDevices(reportModel, setupcodeInfoParams, deviceCategories,ref m_KeepIdList);
        }
        public static DataSet GetBSCReportModeBasedSetWithSubDevices(Model.IDBSCReportModel reportModel, Model.IdSetupCodeInfo setupcodeInfoParams, Model.DeviceCategoryRecords deviceCategories, ref IList<String> m_KeepIdList)
        {
            DataSet reportSet = null;
            if (reportModel != null && reportModel.Count > 0 && deviceCategories != null)
            {
                reportSet = new DataSet("BSCList");
                List<int> worksheetNumberList = deviceCategories.GetDistinctWorksheetNumbers();
                DataTable table = null;         
                foreach (Int32 number in worksheetNumberList)
                {
                    bool mergeIDFlag;
                    string mappingName = deviceCategories.GetMappingName(number,out mergeIDFlag);
                    table = reportSet.Tables.Add(mappingName);
                    //add columns to dataset
                    table.Columns.Add("Brand");
                    if (setupcodeInfoParams.DisplayBrandNumber)
                    {
                        table.Columns.Add("Brand Number");
                    }
                    table.Columns.Add("ID");
                    List<String> modeList = deviceCategories.GetModeListUnder(number);
                    if (modeList.Count > 0)
                    {
                        Model.IDBSCReportModel refinedReportModel= null;                        
                        if (mergeIDFlag)
                        {
                            refinedReportModel = reportModel.GetBrandIDModel(modeList, "Y");
                            //add to data table.
                            foreach (Model.IDBSCReportRecords bscRecord in refinedReportModel)
                            {
                                //add to table
                                DataRow dr = table.NewRow();
                                dr["Brand"] = bscRecord.Brand;
                                if (setupcodeInfoParams.DisplayBrandNumber)
                                    dr["Brand Number"] = String.Format("{0:0000}", bscRecord.BrandNumber);//bscRecord.BrandNumber;
                                List<Model.IDInfo> idList = bscRecord.GetBSCIDList(bscRecord.Brand);
                                //dr["ID"] = GetSortedIDs((List<Model.IDInfo>)bscRecord.IDList, setupcodeInfoParams, bscRecord.Brand);
                                dr["ID"] = GetSortedIDs(idList, setupcodeInfoParams, bscRecord.Brand,ref m_KeepIdList);
                                table.Rows.Add(dr);
                                table.AcceptChanges();
                            }
                        }
                        else
                        {                          
                            //add to data table.
                            foreach (string mode in modeList)
                            {
                                foreach (Model.DeviceCategory subdevices in deviceCategories)
                                {
                                    if (subdevices.Modes == mode)
                                    {
                                        foreach (String dev in subdevices.SubDevices)
                                        {
                                            refinedReportModel = reportModel.GetBSCModel(mode, dev, "Y");
                                           DataRow dr = table.NewRow();
                                           dr["Brand"] = dev;
                                           table.Rows.Add(dr);
                                           table.AcceptChanges();

                                           foreach (Model.IDBSCReportRecords bscRecord in refinedReportModel)
                                           {
                                               //add to table
                                               dr = table.NewRow();
                                               dr["Brand"] = bscRecord.Brand;
                                               if (setupcodeInfoParams.DisplayBrandNumber)
                                                   dr["Brand Number"] = String.Format("{0:0000}", bscRecord.BrandNumber);//bscRecord.BrandNumber;
                                               List<Model.IDInfo> idList = bscRecord.GetBSCIDList(bscRecord.Brand);
                                               dr["ID"] = GetSortedIDs(idList, setupcodeInfoParams, bscRecord.Brand,ref m_KeepIdList);
                                               //dr["ID"] = GetSortedIDs((List<Model.IDInfo>)bscRecord.IDList, setupcodeInfoParams, bscRecord.Brand);
                                               table.Rows.Add(dr);
                                               table.AcceptChanges();

                                           }
                                        }
                                    }
                                }
                            }
                        }                        
                    }                                      
                }
                reportSet.AcceptChanges();
            }
            return reportSet;
        }
        public static DataSet GetBSCReportModeBasedSetWithoutSubDevices(Model.IDBSCReportModel reportModel, Model.IdSetupCodeInfo setupcodeInfoParams, Model.DeviceCategoryRecords deviceCategories,ref IList<String> m_KeepIdList)
        {
            DataSet reportSet = null;
            if (reportModel != null && reportModel.Count > 0 && deviceCategories != null)
            {
                reportSet = new DataSet("BSCList");

                List<int> worksheetNumberList = deviceCategories.GetDistinctWorksheetNumbers();

                DataTable table = null;
                foreach (Int32 number in worksheetNumberList)
                {
                    bool mergeIDFlag;

                    string mappingName = deviceCategories.GetMappingName(number, out mergeIDFlag);

                    table = reportSet.Tables.Add(mappingName);
                    //add columns to dataset
                    table.Columns.Add("Brand");
                    if (setupcodeInfoParams.DisplayBrandNumber)
                    {
                        table.Columns.Add("Brand Number");
                    }
                    table.Columns.Add("ID");

                    List<String> modeList = deviceCategories.GetModeListUnder(number);

                    if (modeList.Count > 0)
                    {
                        Model.IDBSCReportModel refinedReportModel = null;

                        if (mergeIDFlag)
                        {
                            refinedReportModel = reportModel.GetBrandIDModel(modeList,"N");
                            
                        }
                        else
                        {
                            refinedReportModel = new Model.IDBSCReportModel();
                            foreach (string mode in modeList)
                            {
                                refinedReportModel.Add(reportModel.GetBrandIDModel(mode, "N"));
                            }
                        }

                        //add to data table.
                        foreach (Model.IDBSCReportRecords bscRecord in refinedReportModel)
                        {

                            //add to table
                            DataRow dr = table.NewRow();
                            dr["Brand"] = bscRecord.Brand;
                            if (setupcodeInfoParams.DisplayBrandNumber)
                                dr["Brand Number"] = String.Format("{0:0000}", bscRecord.BrandNumber);//bscRecord.BrandNumber;

                            List<Model.IDInfo> idList = bscRecord.GetBSCIDList(bscRecord.Brand);
                            dr["ID"] = GetSortedIDs(idList, setupcodeInfoParams, bscRecord.Brand, ref m_KeepIdList);
                            //dr["ID"] = GetSortedIDs((List<Model.IDInfo>)bscRecord.IDList, setupcodeInfoParams, bscRecord.Brand);
                            table.Rows.Add(dr);
                            table.AcceptChanges();
                        }
                    }
                }
                reportSet.AcceptChanges();
            }
            return reportSet;
        }
        private static Model.IDInfo getIDInfo(List<Model.IDInfo> allidList, String id)
        {
            foreach (Model.IDInfo idInfo in allidList)
            {
                if (idInfo.ID == id)
                    return idInfo;
            }
            return null;
        }
        private static Model.Id getIDInfo(IList<Model.Id> allidList, String id)
        {
            foreach (Model.Id idInfo in allidList)
            {
                if (idInfo.ID == id)
                    return idInfo;
            }
            return null;
        }
        private static void removePreservedIdFromList(List<Model.IDInfo> idList, String Id)
        {
            Int32 intTotalIds = idList.Count;
            for (Int32 intCount = 0; intCount < intTotalIds; intCount++)
            {
                if (idList[intCount].ID == Id)
                {
                    idList.RemoveAt(intCount);
                    intTotalIds--;
                }
            }
        }
        private static Model.Id checkIDPresence(IList<Model.Id> allidList, String id)
        {
            foreach (Model.Id idInfo in allidList)
            {
                if (idInfo.ID == id)
                    return idInfo;
            }
            return null;
        }
        private static string GetSortedIDs(List<Model.IDInfo> idList, Model.IdSetupCodeInfo setupcodeInfoParams, String brand, ref IList<String> m_KeepIdList)
        {
            StringBuilder sb = new StringBuilder();
            StringBuilder sbErrorCodes = new StringBuilder();
            if (idList.Count > 0)
            {                                
                //Apply Identical ID List Elemination
                if (setupcodeInfoParams.IdenticalIdList != null && setupcodeInfoParams.IncludeIdenticalIdList == true)
                {
                    foreach (Model.AliasId identicalId in setupcodeInfoParams.IdenticalIdList)
                    {
                        if (identicalId.AliasID != String.Empty)
                        {
                            String[] retainedIds = identicalId.AliasID.Split(',');
                            foreach (String retainedId in retainedIds)
                            {
                                Model.IDInfo replaceIdInfo = getIDInfo(idList, retainedId);
                                if (replaceIdInfo != null)
                                {
                                    //Remove Preserve ID from ID List
                                    removePreservedIdFromList(idList, retainedId);
                                    //Check if id is allready exists in the list
                                    //if not exist then add it
                                    Model.IDInfo retainId = getIDInfo(idList, identicalId.ID);
                                    if (retainId == null)
                                    {
                                        Model.IDInfo insertId = new Model.IDInfo();
                                        insertId.ID = identicalId.ID;
                                        insertId.ModelCount = replaceIdInfo.ModelCount;
                                        idList.Add(insertId);
                                        insertId = null;
                                    }
                                }
                            }
                        }                       
                    }
                }

                //Create New ID List Object
                List<Model.Id> newidList = new List<Model.Id>();
                //Apply Priority ID List File
                if (setupcodeInfoParams.PriorityIDList != null)
                {
                    foreach (Model.Id id in setupcodeInfoParams.PriorityIDList)
                    {
                        Model.IDInfo idInfo = getIDInfo(idList, id.ID);
                        if (idInfo != null)
                        {
                            newidList.Add(new Model.Id(id.ID));
                        }
                    }
                    //check if same id all ready exists then remove it from id list                    
                }
                //Low Priority ID List
                List<Model.Id> lowPriorityIdList = new List<Model.Id>();
                                
                //Sort ID List                
                Model.IDComparer comparer = null;
                if (setupcodeInfoParams.SortType == Model.ReportSortBy.ModelCount)
                    comparer = new Model.IDComparer(Model.ReportSortBy.ModelCount);
                else
                    comparer = new Model.IDComparer(Model.ReportSortBy.IDNumber);
                idList.Sort(comparer);
                foreach (Model.IDInfo id in idList)
                {
                    if (id.LowPriorityID != null)
                    {
                        lowPriorityIdList.Add(new Model.Id(id.ID));
                    }
                    else
                    {
                        Model.Id idInfo = checkIDPresence(newidList, id.ID);
                        if (idInfo == null)
                        {
                            newidList.Add(new Model.Id(id.ID));
                        }
                    }
                }
                if (lowPriorityIdList.Count > 0)
                {
                    foreach (Model.Id id in lowPriorityIdList)
                    {
                        Model.Id idInfo = checkIDPresence(newidList, id.ID);
                        if (idInfo == null)
                        {
                            newidList.Add(new Model.Id(id.ID));
                        }
                    }
                }
                //Apply ID Offset
                if (setupcodeInfoParams.IdOffset != 0)
                {
                    foreach (Model.Id id in newidList)
                    {
                        Int32 intPartofID = Int32.Parse(id.ID.Remove(0,1));
                        //Add Offset Value
                        Int32 newintPartofID = intPartofID + setupcodeInfoParams.IdOffset;
                        String newID = id.ID.Substring(0,1) + String.Format("{0:0000}", newintPartofID);
                        id.ID = newID;
                    }
                }
                //Apply Set Setup Code format
                foreach (Model.Id id in newidList)
                {
                    if(m_KeepIdList.Contains(id.ID))
                    {
                        m_KeepIdList.Remove(id.ID);
                    }
                    String formattedCode = String.Empty;
                    if (setupcodeInfoParams.SetupCodeFormat == "Base 10")
                    {
                        if (setupcodeInfoParams.IncludeModeChar)
                            formattedCode = id.ID;
                        else
                            formattedCode = id.ID.Remove(0, 1);
                    }
                    else
                    {
                        if (setupcodeInfoParams.IncludeModeChar)
                            formattedCode = id.ID.Substring(0,1) + FormatSetupCode(id.ID.Remove(0, 1), setupcodeInfoParams);
                        else
                            formattedCode = FormatSetupCode(id.ID.Remove(0, 1), setupcodeInfoParams);
                    }
                    if (!String.IsNullOrEmpty(formattedCode))
                    {
                        sb.Append(formattedCode);
                        sb.Append(", ");
                    }
                    else
                    {
                        setupcodeInfoParams.SetupCodeFormatErrorFlag = true;
                        sbErrorCodes.Append(id.ID + ",");
                    }
                }                                
                if(sb.Length > 0)
                    sb.Remove(sb.Length - 2, 2);
                if (sbErrorCodes.Length > 0)
                {
                    sbErrorCodes.Remove(sbErrorCodes.Length - 1, 1);
                    setupcodeInfoParams.SetSetupCodesWithFormatError(brand,sbErrorCodes.ToString());
                }
            }
            return sb.ToString();
        }       
        #endregion   

        #region Get Brand Search
        public Model.BrandSearchModel GetBrandSearchReport(List<String> m_Brands)
        {
            Model.BrandSearchModel resultCollection = null;
            try
            {
                IDSearchParameter searchParam = new IDSearchParameter();
                StringBuilder paramBuilder = new StringBuilder();
                foreach (String brand in m_Brands)
                {
                    paramBuilder.Append(brand + ",");
                }
                if (paramBuilder.Length > 0)
                    paramBuilder.Remove(paramBuilder.Length - 1, 1);

                searchParam.Brands = paramBuilder.ToString();

                HashtableCollection result = _DBAccess.GetAllBrands(searchParam);
                if (result.Count == 0)
                {
                    Hashtable table = new Hashtable();
                    table.Add("StandardBrand", "");
                    table.Add("Brand", "");
                    table.Add("BrandType", "");
                    table.Add("Mode", "");
                    table.Add("DataSource", "");
                    table.Add("Region", "");
                    table.Add("Country", "");
                    result.Add(table);
                }
                resultCollection = new Model.BrandSearchModel();
                DataTable dtBrands = new DataTable();
                dtBrands.Columns.Add("StandardBrand");
                dtBrands.Columns.Add("Brand");
                dtBrands.Columns.Add("BrandType");
                dtBrands.Columns.Add("Mode");
                dtBrands.Columns.Add("DataSource");
                dtBrands.Columns.Add("Region");
                dtBrands.Columns.Add("Country");
                DataRow dr = null;
                foreach (Hashtable item in result)
                {
                    DataRow[] rows = dtBrands.Select("StandardBrand='" + item["StandardBrand"].ToString().Replace("'", "''") + "' and Brand='" + item["Brand"].ToString().Replace("'", "''") + "' and BrandType='" + item["BrandType"].ToString() + "'");
                    if (rows.Length > 0)
                    {
                        foreach (DataRow d in rows)
                        {
                            if (d["Mode"].ToString() == String.Empty)
                            {
                                d["Mode"] = item["Mode"].ToString();
                            }
                            else
                            {
                                if (!d["Mode"].ToString().Contains(item["Mode"].ToString()))
                                {
                                    d["Mode"] = d["Mode"].ToString() + "," + item["Mode"].ToString();
                                }
                            }
                            if (d["DataSource"].ToString() == String.Empty)
                            {
                                d["DataSource"] = item["DataSource"].ToString();
                            }
                            else
                            {
                                if (!d["DataSource"].ToString().Contains(item["DataSource"].ToString()))
                                {
                                    d["DataSource"] = d["DataSource"].ToString() + "," + item["DataSource"].ToString();
                                }
                            }
                            if (d["Region"].ToString() == String.Empty)
                            {
                                d["Region"] = item["Region"].ToString();
                            }
                            else
                            {
                                if (!d["Region"].ToString().Contains(item["Region"].ToString()))
                                {
                                    d["Region"] = d["Region"].ToString() + "," + item["Region"].ToString();
                                }
                            }
                            if (d["Country"].ToString() == String.Empty)
                            {
                                d["Country"] = item["Country"].ToString();
                            }
                            else
                            {
                                if (!d["Country"].ToString().Contains(item["Country"].ToString()))
                                {
                                    d["Country"] = d["Country"].ToString() + "," + item["Country"].ToString();
                                }
                            }
                            dtBrands.AcceptChanges();
                        }
                    }
                    else
                    {
                        dr = dtBrands.NewRow();
                        dr["StandardBrand"] = item["StandardBrand"].ToString();
                        dr["Brand"]         = item["Brand"].ToString();
                        dr["BrandType"]     = item["BrandType"].ToString();
                        dr["Mode"]          = item["Mode"].ToString();
                        dr["DataSource"]    = item["DataSource"].ToString();
                        dr["Region"]        = item["Region"].ToString();
                        dr["Country"]       = item["Country"].ToString();
                        dtBrands.Rows.Add(dr);
                        dr = null;
                    }
                }
                dtBrands.AcceptChanges();
                foreach (String item in m_Brands)
                {
                    Model.BrandSearchRecord m_Brand = new Model.BrandSearchRecord();
                    m_Brand.Brand = item;
                    DataRow[] brandExists = dtBrands.Select("StandardBrand='" + item.Replace("'", "''") + "'");
                    if (brandExists.Length > 0)
                    {
                        m_Brand.BrandType = 0;
                        m_Brand.Mode = brandExists[0]["Mode"].ToString();
                        m_Brand.DataSource = brandExists[0]["DataSource"].ToString();
                        m_Brand.Region = brandExists[0]["Region"].ToString();
                        m_Brand.Country = brandExists[0]["Country"].ToString();
                    }
                    else
                    {
                        brandExists = dtBrands.Select("Brand='" + item.Replace("'", "''") + "'");
                        if (brandExists.Length > 0)
                        {
                            if (brandExists[0]["BrandType"].ToString() == "AliasBrand")
                            {
                                m_Brand.BrandType = 1;
                            }
                            else if (brandExists[0]["BrandType"].ToString() == "BrandVariation")
                            {
                                m_Brand.BrandType = 2;
                            }
                            else if (brandExists[0]["BrandType"].ToString() == "StandardBrand")
                            {
                                m_Brand.BrandType = 0;
                            }
                            m_Brand.Mode = brandExists[0]["Mode"].ToString();
                            m_Brand.DataSource = brandExists[0]["DataSource"].ToString();
                            m_Brand.Region = brandExists[0]["Region"].ToString();
                            m_Brand.Country = brandExists[0]["Country"].ToString();
                        }
                        else
                        {
                            m_Brand.BrandType = 3;
                        }
                    }
                    resultCollection.Add(m_Brand);
                    m_Brand = null;
                }
            }
            catch { }
            return resultCollection;
        }
        #endregion

        #region ID Brand
        private IDBrandResultCollection GetIDBrandRawData(IList<Model.Id> ids, IList<Model.Region> locations,String DeviceAliasRegion, List<String> selectedModes,
        IList<Model.DeviceType> devices, IList<Model.DataSources> dataSources, Model.IdSetupCodeInfo idSetupCodeInfoParam)
        {
            IDBrandResultCollection resultCollection = null;
            IDSearchParameter searchParameter = null;
            try
            {
                resultCollection = new IDBrandResultCollection();
                searchParameter = new IDSearchParameter();
                StringBuilder paramBuilder = new StringBuilder();
                //add the ids
                String strIds = String.Empty;
                foreach (Model.Id id in ids)
                    paramBuilder.Append(id.ID + ",");
                if (paramBuilder.Length > 0)
                    paramBuilder.Remove(paramBuilder.Length - 1, 1);
                strIds = paramBuilder.ToString();

                //add locations xml
                String strLocations = String.Empty;
                if (locations != null)
                {
                    _XML = new XMLOperations("UEI2Server", XMLOperations.EncodingType.UTF16);
                    _XML.ParentNode = "Document";
                    _XML.ParentAttributeName = "Type";
                    _XML.ParentAttributeValue = "LocationSelections";
                    _XML.ParentAttributeName = "Version";
                    _XML.ParentAttributeValue = "1.0.0";
                    _XML.AppendParent();

                    foreach (Model.Region r in locations)
                    {
                        _XML.ParentNode = "Region";
                        _XML.ParentAttributeName = "Name";
                        _XML.ParentAttributeValue = r.Name;
                        _XML.ParentAttributeName = "Weight";
                        _XML.ParentAttributeValue = "0";
                        _XML.ParentAttributeName = "SystemFlags";
                        if (idSetupCodeInfoParam.IncludeNonCountryLinkedRecords == true)
                        {
                            _XML.ParentAttributeValue = "2";
                        }
                        else
                        {
                            _XML.ParentAttributeValue = "0";
                        }
                        if (r.SubRegions != null)
                        {
                            foreach (Model.Region subregion in r.SubRegions)
                            {
                                _XML.ChildNode = "SubRegion";
                                _XML.ChildAttributeName = "Name";
                                _XML.ChildAttributeValue = subregion.Name;
                                _XML.ChildAttributeName = "Weight";
                                _XML.ChildAttributeValue = "0";
                                _XML.ChildAttributeName = "SystemFlags";
                                _XML.ChildAttributeValue = "0";
                                if (r.Countries != null)
                                {
                                    foreach (Model.Country country in r.Countries)
                                    {
                                        if (subregion.Name == country.SubRegion)
                                        {
                                            _XML.ChildNode = "Country";
                                            _XML.ChildAttributeName = "Name";
                                            _XML.ChildAttributeValue = country.Name;
                                            _XML.ChildAttributeName = "Weight";
                                            _XML.ChildAttributeValue = "0";
                                            _XML.ChildAttributeName = "SystemFlags";
                                            _XML.ChildAttributeValue = "0";
                                            _XML.AppendChild();
                                        }
                                    }
                                }
                                _XML.AppendChild();
                            }
                        }
                        if (r.Countries != null)
                        {
                            foreach (Model.Country country in r.Countries)
                            {
                                if (country.SubRegion == String.Empty)
                                {
                                    _XML.ChildNode = "Country";
                                    _XML.ChildAttributeName = "Name";
                                    _XML.ChildAttributeValue = country.Name;
                                    _XML.ChildAttributeName = "Weight";
                                    _XML.ChildAttributeValue = "0";
                                    _XML.ChildAttributeName = "SystemFlags";
                                    _XML.ChildAttributeValue = "0";
                                    _XML.AppendChild();
                                }
                            }
                        }
                        _XML.AppendParent();
                    }
                }
                strLocations = _XML.XMLGenerator();
                _XML = null;

                //add device xml
                String strDevices = String.Empty;
                if (devices != null)
                {
                    _XML = new XMLOperations("UEI2Server", XMLOperations.EncodingType.UTF16);
                    _XML.ParentNode = "Document";
                    _XML.ParentAttributeName = "Type";
                    _XML.ParentAttributeValue = "DeviceSelections";
                    _XML.ParentAttributeName = "Version";
                    _XML.ParentAttributeValue = "1.0.0";
                    _XML.AppendParent();

                    foreach (String mode in selectedModes)
                    {
                        _XML.ParentNode = "DeviceMode";
                        _XML.ParentAttributeName = "Name";
                        _XML.ParentAttributeValue = mode;
                        _XML.ParentAttributeName = "SystemFlags";
                        //if (idSetupCodeInfoParam.IncludeEmptyDeviceType == true)
                        //    _XML.ParentAttributeValue = "3";
                        //else if (idSetupCodeInfoParam.IncludeEmptyDeviceType == false)
                        //    _XML.ParentAttributeValue = "1";
                        if(idSetupCodeInfoParam.IncludeNonComponentLinkedRecords == true && idSetupCodeInfoParam.IncludeEmptyDeviceType == true)
                            _XML.ParentAttributeValue = "7";
                        else if(idSetupCodeInfoParam.IncludeNonComponentLinkedRecords == true && idSetupCodeInfoParam.IncludeEmptyDeviceType == false)
                            _XML.ParentAttributeValue = "4";
                        else if(idSetupCodeInfoParam.IncludeNonComponentLinkedRecords == false && idSetupCodeInfoParam.IncludeEmptyDeviceType == true)
                            _XML.ParentAttributeValue = "3";
                        else if(idSetupCodeInfoParam.IncludeNonComponentLinkedRecords == false && idSetupCodeInfoParam.IncludeEmptyDeviceType == false)
                            _XML.ParentAttributeValue = "1";
                        foreach (Model.DeviceType device in devices)
                        {
                            if (device.Mode == mode)
                            {
                                _XML.ChildNode = "MainDevices";
                                _XML.ChildAttributeName = "Name";
                                _XML.ChildAttributeValue = device.Name;
                                _XML.ChildAttributeName = "SystemFlags";
                                _XML.ChildAttributeValue = "1";
                                if (device.SubDevices != null)
                                {
                                    foreach (Model.DeviceType subdevice in device.SubDevices)
                                    {
                                        _XML.ChildNode = "SubDevices";
                                        _XML.ChildAttributeName = "Name";
                                        _XML.ChildAttributeValue = subdevice.Name;
                                        _XML.ChildAttributeName = "SystemFlags";
                                        _XML.ChildAttributeValue = "1";
                                        if (device.Components != null)
                                        {
                                            foreach (Model.Component component in device.Components)
                                            {
                                                if (subdevice.Name == component.SubDeviceType)
                                                {
                                                    _XML.ChildNode = "Component";
                                                    _XML.ChildAttributeName = "Name";
                                                    _XML.ChildAttributeValue = component.Name;
                                                    _XML.ChildAttributeName = "SystemFlags";
                                                    _XML.ChildAttributeValue = "0";
                                                    _XML.ChildAttributeName = "SystemFlagsExt";
                                                    _XML.ChildAttributeValue = "1";
                                                    _XML.AppendChild();
                                                }
                                            }
                                        }
                                        _XML.AppendChild();
                                    }
                                }
                                if (device.Components != null)
                                {
                                    foreach (Model.Component component in device.Components)
                                    {
                                        if (component.SubDeviceType == String.Empty)
                                        {
                                            _XML.ChildNode = "Component";
                                            _XML.ChildAttributeName = "Name";
                                            _XML.ChildAttributeValue = component.Name;
                                            _XML.ChildAttributeName = "SystemFlags";
                                            _XML.ChildAttributeValue = "0";
                                            _XML.ChildAttributeName = "SystemFlagsExt";
                                            _XML.ChildAttributeValue = "1";
                                            _XML.AppendChild();
                                        }
                                    }
                                }
                                _XML.AppendChild();
                            }
                        }
                        _XML.AppendParent();
                    }
                }
                strDevices = _XML.XMLGenerator();
                _XML = null;

                //add the datasource locations
                String strDataSources = String.Empty;
                paramBuilder = new StringBuilder();
                if (dataSources != null)
                    foreach (Model.DataSources source in dataSources)
                        paramBuilder.Append(source.Name + ",");
                if (paramBuilder.Length > 0)
                    paramBuilder.Remove(paramBuilder.Length - 1, 1);
                strDataSources = paramBuilder.ToString();

                searchParameter.Ids                             = strIds;
                searchParameter.DeviceAliasRegion               = DeviceAliasRegion;
                searchParameter.XMLLocations                    = strLocations;
                searchParameter.XMLDevices                      = strDevices;
                searchParameter.DataSources                     = strDataSources;
                searchParameter.IncludeRestrictedIDs            = idSetupCodeInfoParam.IncludeRestrictedIds;
                searchParameter.IncludeAliasBrand               = (idSetupCodeInfoParam.InclideAliasBrand) ? 0 : 1;
                searchParameter.DisplayMajorBrandsOnly          = idSetupCodeInfoParam.DisplayMajorBrandsOnly;
                searchParameter.IncudeRecordWithoutModelName    = (idSetupCodeInfoParam.IncludeBlankModelNames) ? 1 : 0;
                searchParameter.ModelType                       = idSetupCodeInfoParam.ModelType;
                searchParameter.IncludeUnknownLocation          = (idSetupCodeInfoParam.IncludeUnknownLocation == true) ? 1 : 0;

                ErrorLogging.WriteToLogFile("--Function Name: GetIDBrandRawData--", false);
                ErrorLogging.WriteToLogFile("UEI2_Sp_BSCbyID Input Parameters", false);
                ErrorLogging.WriteToLogFile("##########################################################", false);
                ErrorLogging.WriteToLogFile("IDS            :" + strIds, false);
                ErrorLogging.WriteToLogFile("LOCATION XML   :" + strLocations, false);
                ErrorLogging.WriteToLogFile("DEVICE XML     :" + strDevices, false);
                ErrorLogging.WriteToLogFile("DATASOURCE     :" + strDataSources, false);
                ErrorLogging.WriteToLogFile("##########################################################", false);                   

                DateTime spStart = DateTime.Now;
                ErrorLogging.WriteToLogFile("UEI2_Sp_BSCbyID execution time", false);
                ErrorLogging.WriteToLogFile("Start Time: " + spStart.Hour.ToString() + ":" + spStart.Minute.ToString() + ":" + spStart.Second.ToString() + ":" + spStart.Millisecond.ToString(), false);
                resultCollection = _DBAccess.GetIDBrandResults(searchParameter);

                DateTime spEnd = DateTime.Now;
                ErrorLogging.WriteToLogFile("End Time: " + spEnd.Hour.ToString() + ":" + spEnd.Minute.ToString() + ":" + spEnd.Second.ToString() + ":" + spEnd.Millisecond.ToString(), false);

                totalExecTime = spEnd - spStart;
                ErrorLogging.WriteToLogFile("Total sp Time: " + totalExecTime.Hours.ToString() + ":" + totalExecTime.Minutes.ToString() + ":" + totalExecTime.Seconds.ToString() + ":" + totalExecTime.Milliseconds.ToString(), false);

                return resultCollection;
            }
            catch (Exception ex)
            {
                ErrorLogging.WriteToErrorLogFile(ex, searchParameter);

                return null;
            }
            
        }
        private Model.IdBrandReportModel GetIDBrandReportModel(IDBrandResultCollection idBrandList)
        {
            ErrorLogging.WriteToLogFile("--Function Name: GetIDBrandReportModel--", false);
            ErrorLogging.WriteToLogFile("Business logic execution time", false);
            DateTime blStart = DateTime.Now;
            ErrorLogging.WriteToLogFile("Start Time: " + blStart.Hour.ToString() + ":" + blStart.Minute.ToString() + ":" + blStart.Second.ToString() + ":" + blStart.Millisecond.ToString(), false);
            
            Model.IdBrandReportModel idBrandReport = null;
            
            if (idBrandList != null && idBrandList.Count > 0)
            {
                idBrandReport = new Model.IdBrandReportModel();

               
                foreach (IDBrandResult result in idBrandList)
                {
                    //Rescult Collection gives brand information for all input ids
                    //Brand with name as "0" is the brands for the ids not available for the given input criteria
                    if (result.Brand != "0")
                    {
                       
                        Model.IDBrandReportRecord record = idBrandReport.GetIDBrandRecord(result.ID);
                        
                        if (record == null)
                        {
                            //create a brandinfo.
                            Model.BrandInfo brand = new Model.BrandInfo();
                            brand.DeviceTypeFlag = result.DeviceTypeFlag;
                            brand.Name = result.Brand;
                            brand.ModelCount = result.ModelCount;                            
                            //create deviceinfo object and add brandinfo
                            Model.DeviceInfo deviceInfo = new Model.DeviceInfo();
                            //deviceInfo.Name = result.DeviceType;
                            deviceInfo.MainDevice = result.MainDevice;
                            deviceInfo.SubDevice = result.SubDevice;
                            deviceInfo.Component = result.Component;
                            deviceInfo.RegionalName = result.RegionalName;
                            deviceInfo.BrandList.Add(brand);
                            record = new Model.IDBrandReportRecord();
                            record.ID = result.ID;
                            //record.BrandList.Add(brand);
                            record.DeviceList.Add(deviceInfo);
                            //add to report
                            idBrandReport.Add(record);
                        }
                        else
                        {
                            Model.BrandInfo brand = new Model.BrandInfo();
                            brand.DeviceTypeFlag = result.DeviceTypeFlag;
                            brand.Name = result.Brand;
                            brand.ModelCount = result.ModelCount;

                            //check whether the device exists under the id
                            Model.DeviceInfo deviceInfo = record.GetDeviceInfo(result.MainDevice,result.SubDevice,result.Component);

                            if (deviceInfo == null)
                            {
                                deviceInfo = new Model.DeviceInfo();
                                //deviceInfo.Name = result.DeviceType;
                                deviceInfo.MainDevice = result.MainDevice;
                                deviceInfo.SubDevice = result.SubDevice;
                                deviceInfo.Component = result.Component;
                                deviceInfo.RegionalName = result.RegionalName;
                                deviceInfo.BrandList.Add(brand);
                                record.DeviceList.Add(deviceInfo);
                            }
                            else
                            {
                                deviceInfo.BrandList.Add(brand);
                            }
                            //record.BrandList.Add(brand);
                            //record.DeviceList.Add(deviceInfo);
                        }
                    }
                }
            }

            DateTime blEnd = DateTime.Now;
            ErrorLogging.WriteToLogFile("End Time: " + blEnd.Hour.ToString() + ":" + blEnd.Minute.ToString() + ":" + blEnd.Second.ToString() + ":" + blEnd.Millisecond.ToString(), false);            
            TimeSpan totalBlTime = blEnd - blStart;
            ErrorLogging.WriteToLogFile("Total BL Time: " + totalBlTime.Hours.ToString() + ":" + totalBlTime.Minutes.ToString() + ":" + totalBlTime.Seconds.ToString() + ":" + totalBlTime.Milliseconds.ToString(), false);
            totalExecTime = totalExecTime + totalBlTime;
            ErrorLogging.WriteToLogFile("Total Time: " + totalExecTime.Hours.ToString() + ":" + totalExecTime.Minutes.ToString() + ":" + totalExecTime.Seconds.ToString() + ":" + totalExecTime.Milliseconds.ToString(), false);           
            return idBrandReport;
        }
        public Model.IdBrandReportModel GetIDBrandReport(IList<Model.Id> ids, IList<Model.Region> locations,String DeviceAliasRegion, List<String> selectedModes,
        IList<Model.DeviceType> devices, IList<Model.DataSources> dataSources, Model.IdSetupCodeInfo setupcodeInfoParams)
        {
            try
            {
                if (ids != null && locations != null && dataSources != null)
                {
                    IDBrandResultCollection results = GetIDBrandRawData(ids, locations,DeviceAliasRegion, selectedModes, devices, dataSources, setupcodeInfoParams);
                    return GetIDBrandReportModel(results);
                }
                return null;
            }
            catch
            {
                ErrorLogging.WriteToLogFile("---Exception raised---", false);
                //ErrorLogging.WriteToErrorLogFile(ex);
                return null;
            }
        }        
        #endregion

        #region ModelInfoReport
        public DataSet GetModelInfoReport_ArdKey(IList<Model.Id> ids, IList<Model.Region> locations, IList<Model.DataSources> dataSources, List<String> selectedModes, IList<Model.DeviceType> deviceTypes, Model.BrandFilter brands, Model.ModelFilter models, Model.ModelInfoSpecific miscFilters, Model.IdSetupCodeInfo idSetupCodeInfoParam)
        {
            try
            {
                if (ids != null && locations != null && dataSources != null && ids.Count > 0)
                {
                    DataSet resultCollection = GetModelInformation_ArdKey(ids, locations, dataSources, selectedModes, deviceTypes, brands, models, miscFilters, idSetupCodeInfoParam);
                    return resultCollection;
                }

                return null;
            }
            catch (Exception ex)
            {
                ErrorLogging.WriteToErrorLogFile(ex);

                throw;
            }
        }
        private DataSet GetModelInformation_ArdKey(IList<Model.Id> ids, IList<Model.Region> locations, IList<Model.DataSources> dataSources, List<String> selectedModes, IList<Model.DeviceType> deviceTypes, Model.BrandFilter brands, Model.ModelFilter models, Model.ModelInfoSpecific miscFilters, Model.IdSetupCodeInfo idSetupCodeInfoParam)
        {
            DataSet resultCollection = null;
            IDSearchParameter searchParameter = null;

            try
            {                
                searchParameter = new IDSearchParameter();

                //Add replaced Identical Ids to the the id list
                if (miscFilters.IdenticalIdList != null && miscFilters.IncludeIdenticalIdList == true)
                {
                    foreach (Model.AliasId identicalId in miscFilters.IdenticalIdList)
                    {
                        if (identicalId.AliasID.Length > 0)
                        {
                            String[] idList = identicalId.AliasID.Split(',');
                            foreach (String id in idList)
                            {
                                Model.Id idInfo = getIDInfo(ids, id);
                                if (idInfo == null)
                                {
                                    ids.Add(new Model.Id(id));
                                }
                            }
                        }
                    }
                }

                StringBuilder paramBuilder = new StringBuilder();
                //create id string
                foreach (Model.Id id in ids)
                    paramBuilder.Append(id.ID + ",");
                if (paramBuilder.Length > 0)
                    paramBuilder.Remove(paramBuilder.Length - 1, 1);

                searchParameter.Ids = paramBuilder.ToString();

                //create region string.
                #region LocationXmlCreation
                if (locations != null)
                {
                    _XML = new XMLOperations("UEI2Server", XMLOperations.EncodingType.UTF16);
                    _XML.ParentNode = "Document";
                    _XML.ParentAttributeName = "Type";
                    _XML.ParentAttributeValue = "LocationSelections";
                    _XML.ParentAttributeName = "Version";
                    _XML.ParentAttributeValue = "1.0.0";
                    _XML.AppendParent();

                    foreach (Model.Region region in locations)
                    {
                        _XML.ParentNode = "Region";
                        _XML.ParentAttributeName = "Name";
                        _XML.ParentAttributeValue = region.Name;
                        _XML.ParentAttributeName = "Weight";
                        _XML.ParentAttributeValue = "0";
                        _XML.ParentAttributeName = "SystemFlags";
                        if (idSetupCodeInfoParam.IncludeNonCountryLinkedRecords == true)
                        {
                            _XML.ParentAttributeValue = "2";
                        }
                        else
                        {
                            _XML.ParentAttributeValue = "0";
                        }

                        if (region.SubRegions != null)
                        {
                            foreach (Model.Region subRegion in region.SubRegions)
                            {
                                _XML.ChildNode = "SubRegion";
                                _XML.ChildAttributeName = "Name";
                                _XML.ChildAttributeValue = subRegion.Name;
                                _XML.ChildAttributeName = "Weight";
                                _XML.ChildAttributeValue = "0";
                                _XML.ChildAttributeName = "SystemFlags";
                                _XML.ChildAttributeValue = "0";

                                if (region.Countries != null)
                                {
                                    foreach (Model.Country country in region.Countries)
                                    {
                                        if (subRegion.Name == country.SubRegion)
                                        {
                                            _XML.ChildNode = "Country";
                                            _XML.ChildAttributeName = "Name";
                                            _XML.ChildAttributeValue = country.Name;
                                            _XML.ChildAttributeName = "Weight";
                                            _XML.ChildAttributeValue = "0";
                                            _XML.ChildAttributeName = "SystemFlags";
                                            _XML.ChildAttributeValue = "0";
                                            _XML.AppendChild();
                                        }
                                    }
                                }
                                _XML.AppendChild();
                            }
                        }
                        if (region.Countries != null)
                        {
                            foreach (Model.Country country in region.Countries)
                            {
                                if (country.SubRegion == String.Empty)
                                {
                                    _XML.ChildNode = "Country";
                                    _XML.ChildAttributeName = "Name";
                                    _XML.ChildAttributeValue = country.Name;
                                    _XML.ChildAttributeName = "Weight";
                                    _XML.ChildAttributeValue = "0";
                                    _XML.ChildAttributeName = "SystemFlags";
                                    _XML.ChildAttributeValue = "0";
                                    _XML.AppendChild();
                                }
                            }
                        }
                        _XML.AppendParent();
                    }

                }
                #endregion
                searchParameter.XMLLocations = _XML.XMLGenerator();
                _XML = null;

                //add the datasource locations
                paramBuilder = new StringBuilder();
                if (dataSources != null)
                    foreach (Model.DataSources source in dataSources)
                        paramBuilder.Append(source.Name + ",");
                if (paramBuilder.Length > 0)
                    paramBuilder.Remove(paramBuilder.Length - 1, 1);

                searchParameter.DataSources = paramBuilder.ToString();

                //add device types
                #region DeviceXmlCreation
                if (deviceTypes != null)
                {
                    _XML = new XMLOperations("UEI2Server", XMLOperations.EncodingType.UTF16);
                    _XML.ParentNode = "Document";
                    _XML.ParentAttributeName = "Type";
                    _XML.ParentAttributeValue = "DeviceSelections";
                    _XML.ParentAttributeName = "Version";
                    _XML.ParentAttributeValue = "1.0.0";
                    _XML.AppendParent();

                    foreach (string mode in selectedModes)
                    {
                        _XML.ParentNode = "DeviceMode";
                        _XML.ParentAttributeName = "Name";
                        _XML.ParentAttributeValue = mode;
                        _XML.ParentAttributeName = "SystemFlags";

                        //if(idSetupCodeInfoParam.IncludeEmptyDeviceType == true)
                        //    _XML.ParentAttributeValue = "3";
                        //else if(idSetupCodeInfoParam.IncludeEmptyDeviceType == false)
                        //    _XML.ParentAttributeValue = "1";

                        if (idSetupCodeInfoParam.IncludeNonComponentLinkedRecords == true && idSetupCodeInfoParam.IncludeEmptyDeviceType == true)
                            _XML.ParentAttributeValue = "7";
                        else if (idSetupCodeInfoParam.IncludeNonComponentLinkedRecords == true && idSetupCodeInfoParam.IncludeEmptyDeviceType == false)
                            _XML.ParentAttributeValue = "4";
                        else if (idSetupCodeInfoParam.IncludeNonComponentLinkedRecords == false && idSetupCodeInfoParam.IncludeEmptyDeviceType == true)
                            _XML.ParentAttributeValue = "3";
                        else if (idSetupCodeInfoParam.IncludeNonComponentLinkedRecords == false && idSetupCodeInfoParam.IncludeEmptyDeviceType == false)
                            _XML.ParentAttributeValue = "1";

                        foreach (Model.DeviceType device in deviceTypes)
                        {
                            if (device.Mode == mode)
                            {
                                _XML.ChildNode = "MainDevices";
                                _XML.ChildAttributeName = "Name";
                                _XML.ChildAttributeValue = device.Name;
                                _XML.ChildAttributeName = "SystemFlags";//RemoveIDSForSelectedSubDevices is always false in modelinforeport.
                                _XML.ChildAttributeValue = "1";

                                if (device.SubDevices != null)
                                {
                                    foreach (Model.DeviceType subdevice in device.SubDevices)
                                    {
                                        _XML.ChildNode = "SubDevices";
                                        _XML.ChildAttributeName = "Name";
                                        _XML.ChildAttributeValue = subdevice.Name;
                                        _XML.ChildAttributeName = "SystemFlags";//RemoveIDSForSelectedSubDevices is always false in modelinforeport.
                                        _XML.ChildAttributeValue = "1";

                                        if (device.Components != null)
                                        {
                                            foreach (Model.Component component in device.Components)
                                            {
                                                if (subdevice.Name == component.SubDeviceType)
                                                {
                                                    _XML.ChildNode = "Component";
                                                    _XML.ChildAttributeName = "Name";
                                                    _XML.ChildAttributeValue = component.Name;
                                                    _XML.ChildAttributeName = "SystemFlags";
                                                    _XML.ChildAttributeValue = "0";
                                                    _XML.ChildAttributeName = "SystemFlagsExt";
                                                    _XML.ChildAttributeValue = "1";

                                                    _XML.AppendChild();
                                                }
                                            }
                                        }
                                        _XML.AppendChild();

                                    }
                                }
                                if (device.Components != null)
                                {
                                    foreach (Model.Component component in device.Components)
                                    {
                                        if (component.SubDeviceType == String.Empty)
                                        {
                                            _XML.ChildNode = "Component";
                                            _XML.ChildAttributeName = "Name";
                                            _XML.ChildAttributeValue = component.Name;
                                            _XML.ChildAttributeName = "SystemFlags";
                                            _XML.ChildAttributeValue = "0";
                                            _XML.ChildAttributeName = "SystemFlagsExt";
                                            _XML.ChildAttributeValue = "1";

                                            _XML.AppendChild();
                                        }
                                    }
                                }
                                _XML.AppendChild();
                            }
                        }
                        _XML.AppendParent();
                    }
                    searchParameter.XMLDevices = _XML.XMLGenerator();
                    searchParameter.EmptyDeviceTypeFilter = 0;
                }
                else
                {
                    searchParameter.XMLDevices = String.Empty;
                    searchParameter.EmptyDeviceTypeFilter = 1;
                }
                #endregion


                searchParameter.IncludeUnknownLocation = (idSetupCodeInfoParam.IncludeUnknownLocation == true) ? 1 : 0;

                //brand filter section
                if (brands != null)
                {
                    paramBuilder = new StringBuilder();
                    foreach (string brand in brands.SelectedBrands)
                        paramBuilder.Append(brand + ",");
                    if (paramBuilder.Length > 0)
                        paramBuilder.Remove(paramBuilder.Length - 1, 1);

                    searchParameter.Brands = paramBuilder.ToString();
                    searchParameter.ExcludeSelectedBrands = (brands.MatchSelectedBrands == true) ? 0 : 1;
                    // searchParameter.IncludeRecordWithoutBrandNumber = (brands.RecordWithoutBrandNumber == true) ? 0 : 1; 
                }

                //model filter params
                if (models != null)
                {
                    paramBuilder = new StringBuilder();
                    foreach (string model in models.SelectedModels)
                        paramBuilder.Append(model + ",");
                    if (paramBuilder.Length > 0)
                        paramBuilder.Remove(paramBuilder.Length - 1, 1);

                    searchParameter.Models = paramBuilder.ToString();
                    searchParameter.ExcludeSelectedModels = (models.MatchSelectedModels == true) ? 0 : 1;
                    //searchParameter.IncudeRecordWithoutModelName = (models.RecordWithoutModelName == true) ? 0 : 1;
                    //searchParameter.RemoveNonCodeBookModels = (models.RemoveNonCodeBookModels == true) ? 1 : 0;
                }
                searchParameter.IncudeRecordWithoutModelName = (models.RecordWithoutModelName == true) ? 0 : 1;
                searchParameter.RemoveNonCodeBookModels = (models.RemoveNonCodeBookModels == true) ? 1 : 0;

                //misc filters
                if (miscFilters != null)
                {
                    searchParameter.IncludeRestrictedIDs = miscFilters.IncludeRestrictedIds;
                    searchParameter.DuplicatedOutputs = miscFilters.KeepDuplicatedOutputs;
                    searchParameter.TNLink = (miscFilters.SelectTNLink == true) ? 0 : 1;
                    searchParameter.SearchFlag = "0";
                    if (miscFilters.DateBasedSearchEnabled)
                    {
                        searchParameter.SearchFlag = Convert.ToInt32(miscFilters.ModelInfoOptions).ToString();
                        searchParameter.StartDate = miscFilters.FromDate;
                        searchParameter.EndDate = miscFilters.ToDate;
                    }

                }

                ErrorLogging.WriteToLogFile("--Function Name: GetModelInformation--", false);
                ErrorLogging.WriteToLogFile("UEI2_Sp_GetModelReport_ARD Input Parameters", false);
                ErrorLogging.WriteToLogFile("####################### Input ############################", false);
                ErrorLogging.WriteToLogFile("IDS            :" + searchParameter.Ids, false);
                ErrorLogging.WriteToLogFile("LOCATION XML   :" + searchParameter.XMLLocations, false);
                ErrorLogging.WriteToLogFile("DEVICE XML     :" + searchParameter.XMLDevices, false);
                ErrorLogging.WriteToLogFile("DATASOURCE     :" + searchParameter.DataSources, false);
                ErrorLogging.WriteToLogFile("##########################################################", false);

                DateTime spStart = DateTime.Now;
                ErrorLogging.WriteToLogFile("UEI2_Sp_GetModelReport_ARD execution time", false);
                ErrorLogging.WriteToLogFile("Start Time: " + spStart.Hour.ToString() + ":" + spStart.Minute.ToString() + ":" + spStart.Second.ToString() + ":" + spStart.Millisecond.ToString(), false);

                SetDBAccesslayerType(DBLayerType.ADONet);
                _DBAccess.DBInit(DBConnectionString.QuickSet);

                resultCollection = _DBAccess.GetModelInformation_ArdKey(searchParameter);

                DateTime spEnd = DateTime.Now;
                ErrorLogging.WriteToLogFile("End Time: " + spEnd.Hour.ToString() + ":" + spEnd.Minute.ToString() + ":" + spEnd.Second.ToString() + ":" + spEnd.Millisecond.ToString(), false);

                totalExecTime = spEnd - spStart;
                ErrorLogging.WriteToLogFile("Total sp Time: " + totalExecTime.Hours.ToString() + ":" + totalExecTime.Minutes.ToString() + ":" + totalExecTime.Seconds.ToString() + ":" + totalExecTime.Milliseconds.ToString(), false);

                //if (resultCollection != null)
                //{
                //    //Revert Back Alias ID Replacement
                //    if (idSetupCodeInfoParam.AliasIDList != null)
                //    {
                //        resultCollection = ProcessIDWithAlias(resultCollection, idSetupCodeInfoParam);
                //    }
                //}
                return resultCollection;

            }
            catch (Exception ex)
            {
                ex.Data.Add("parameter", searchParameter);
                throw ex;
            }
        }
        private IDBrandResultCollection GetModelInformation(IList<Model.Id> ids, IList<Model.Region> locations, IList<Model.DataSources> dataSources, List<String> selectedModes, IList<Model.DeviceType> deviceTypes, Model.BrandFilter brands, Model.ModelFilter models, Model.ModelInfoSpecific miscFilters, Model.IdSetupCodeInfo idSetupCodeInfoParam)
        {
            IDBrandResultCollection resultCollection = null;
            IDSearchParameter searchParameter = null;

            try
            {
                resultCollection = new IDBrandResultCollection();
                searchParameter = new IDSearchParameter();

                //Add replaced Identical Ids to the the id list
                if (miscFilters.IdenticalIdList != null && miscFilters.IncludeIdenticalIdList == true)
                {
                    foreach (Model.AliasId identicalId in miscFilters.IdenticalIdList)
                    {
                        if (identicalId.AliasID.Length > 0)
                        {
                            String[] idList = identicalId.AliasID.Split(',');
                            foreach (String id in idList)
                            {
                                Model.Id idInfo = getIDInfo(ids, id);
                                if (idInfo == null)
                                {
                                    ids.Add(new Model.Id(id));
                                }
                            }
                        }
                    }
                }

                StringBuilder paramBuilder = new StringBuilder();
                //create id string
                foreach (Model.Id id in ids)
                    paramBuilder.Append(id.ID + ",");
                if (paramBuilder.Length > 0)
                    paramBuilder.Remove(paramBuilder.Length - 1, 1);

                searchParameter.Ids = paramBuilder.ToString();

                //create region string.
                #region LocationXmlCreation
                if (locations != null)
                {
                    _XML = new XMLOperations("UEI2Server", XMLOperations.EncodingType.UTF16);
                    _XML.ParentNode = "Document";
                    _XML.ParentAttributeName = "Type";
                    _XML.ParentAttributeValue = "LocationSelections";
                    _XML.ParentAttributeName = "Version";
                    _XML.ParentAttributeValue = "1.0.0";
                    _XML.AppendParent();

                    foreach (Model.Region region in locations)
                    {
                        _XML.ParentNode = "Region";
                        _XML.ParentAttributeName = "Name";
                        _XML.ParentAttributeValue = region.Name;
                        _XML.ParentAttributeName = "Weight";
                        _XML.ParentAttributeValue = "0";
                        _XML.ParentAttributeName = "SystemFlags";
                        if (idSetupCodeInfoParam.IncludeNonCountryLinkedRecords == true)
                        {
                            _XML.ParentAttributeValue = "2";
                        }
                        else
                        {
                            _XML.ParentAttributeValue = "0";
                        }

                        if (region.SubRegions != null)
                        {
                            foreach (Model.Region subRegion in region.SubRegions)
                            {
                                _XML.ChildNode = "SubRegion";
                                _XML.ChildAttributeName = "Name";
                                _XML.ChildAttributeValue = subRegion.Name;
                                _XML.ChildAttributeName = "Weight";
                                _XML.ChildAttributeValue = "0";
                                _XML.ChildAttributeName = "SystemFlags";
                                _XML.ChildAttributeValue = "0";

                                if (region.Countries != null)
                                {
                                    foreach (Model.Country country in region.Countries)
                                    {
                                        if (subRegion.Name == country.SubRegion)
                                        {
                                            _XML.ChildNode = "Country";
                                            _XML.ChildAttributeName = "Name";
                                            _XML.ChildAttributeValue = country.Name;
                                            _XML.ChildAttributeName = "Weight";
                                            _XML.ChildAttributeValue = "0";
                                            _XML.ChildAttributeName = "SystemFlags";
                                            _XML.ChildAttributeValue = "0";
                                            _XML.AppendChild();
                                        }
                                    }
                                }
                                _XML.AppendChild();
                            }
                        }
                        if (region.Countries != null)
                        {
                            foreach (Model.Country country in region.Countries)
                            {
                                if (country.SubRegion == String.Empty)
                                {
                                    _XML.ChildNode = "Country";
                                    _XML.ChildAttributeName = "Name";
                                    _XML.ChildAttributeValue = country.Name;
                                    _XML.ChildAttributeName = "Weight";
                                    _XML.ChildAttributeValue = "0";
                                    _XML.ChildAttributeName = "SystemFlags";
                                    _XML.ChildAttributeValue = "0";
                                    _XML.AppendChild();
                                }
                            }
                        }
                        _XML.AppendParent();
                    }

                }
                #endregion
                searchParameter.XMLLocations = _XML.XMLGenerator();
                _XML = null;
                              
                //add the datasource locations
                paramBuilder = new StringBuilder();
                if (dataSources != null)
                    foreach (Model.DataSources source in dataSources)
                        paramBuilder.Append(source.Name + ",");
                if (paramBuilder.Length > 0)
                    paramBuilder.Remove(paramBuilder.Length - 1, 1);

                searchParameter.DataSources = paramBuilder.ToString();

                //add device types
                #region DeviceXmlCreation
                if(deviceTypes != null)
                {
                    _XML = new XMLOperations("UEI2Server", XMLOperations.EncodingType.UTF16);
                    _XML.ParentNode = "Document";
                    _XML.ParentAttributeName = "Type";
                    _XML.ParentAttributeValue = "DeviceSelections";
                    _XML.ParentAttributeName = "Version";
                    _XML.ParentAttributeValue = "1.0.0";
                    _XML.AppendParent();

                    foreach(string mode in selectedModes)
                    {
                        _XML.ParentNode = "DeviceMode";
                        _XML.ParentAttributeName = "Name";
                        _XML.ParentAttributeValue = mode;
                        _XML.ParentAttributeName = "SystemFlags";

                        //if(idSetupCodeInfoParam.IncludeEmptyDeviceType == true)
                        //    _XML.ParentAttributeValue = "3";
                        //else if(idSetupCodeInfoParam.IncludeEmptyDeviceType == false)
                        //    _XML.ParentAttributeValue = "1";

                        if(idSetupCodeInfoParam.IncludeNonComponentLinkedRecords == true && idSetupCodeInfoParam.IncludeEmptyDeviceType == true)
                            _XML.ParentAttributeValue = "7";
                        else if(idSetupCodeInfoParam.IncludeNonComponentLinkedRecords == true && idSetupCodeInfoParam.IncludeEmptyDeviceType == false)
                            _XML.ParentAttributeValue = "4";
                        else if(idSetupCodeInfoParam.IncludeNonComponentLinkedRecords == false && idSetupCodeInfoParam.IncludeEmptyDeviceType == true)
                            _XML.ParentAttributeValue = "3";
                        else if(idSetupCodeInfoParam.IncludeNonComponentLinkedRecords == false && idSetupCodeInfoParam.IncludeEmptyDeviceType == false)
                            _XML.ParentAttributeValue = "1";

                        foreach(Model.DeviceType device in deviceTypes)
                        {
                            if(device.Mode == mode)
                            {
                                _XML.ChildNode = "MainDevices";
                                _XML.ChildAttributeName = "Name";
                                _XML.ChildAttributeValue = device.Name;
                                _XML.ChildAttributeName = "SystemFlags";//RemoveIDSForSelectedSubDevices is always false in modelinforeport.
                                _XML.ChildAttributeValue = "1";

                                if(device.SubDevices != null)
                                {
                                    foreach(Model.DeviceType subdevice in device.SubDevices)
                                    {
                                        _XML.ChildNode = "SubDevices";
                                        _XML.ChildAttributeName = "Name";
                                        _XML.ChildAttributeValue = subdevice.Name;
                                        _XML.ChildAttributeName = "SystemFlags";//RemoveIDSForSelectedSubDevices is always false in modelinforeport.
                                        _XML.ChildAttributeValue = "1";

                                        if(device.Components != null)
                                        {
                                            foreach(Model.Component component in device.Components)
                                            {
                                                if(subdevice.Name == component.SubDeviceType)
                                                {
                                                    _XML.ChildNode = "Component";
                                                    _XML.ChildAttributeName = "Name";
                                                    _XML.ChildAttributeValue = component.Name;
                                                    _XML.ChildAttributeName = "SystemFlags";
                                                    _XML.ChildAttributeValue = "0";
                                                    _XML.ChildAttributeName = "SystemFlagsExt";
                                                    _XML.ChildAttributeValue = "1";

                                                    _XML.AppendChild();
                                                }
                                            }
                                        }
                                        _XML.AppendChild();

                                    }
                                }
                                if(device.Components != null)
                                {
                                    foreach(Model.Component component in device.Components)
                                    {
                                        if(component.SubDeviceType == String.Empty)
                                        {
                                            _XML.ChildNode = "Component";
                                            _XML.ChildAttributeName = "Name";
                                            _XML.ChildAttributeValue = component.Name;
                                            _XML.ChildAttributeName = "SystemFlags";
                                            _XML.ChildAttributeValue = "0";
                                            _XML.ChildAttributeName = "SystemFlagsExt";
                                            _XML.ChildAttributeValue = "1";

                                            _XML.AppendChild();
                                        }
                                    }
                                }
                                _XML.AppendChild();
                            }
                        }
                        _XML.AppendParent();
                    }
                    searchParameter.XMLDevices = _XML.XMLGenerator();
                    searchParameter.EmptyDeviceTypeFilter = 0;
                }
                else
                {
                    searchParameter.XMLDevices = String.Empty;
                    searchParameter.EmptyDeviceTypeFilter = 1;
                }
                #endregion
                

                searchParameter.IncludeUnknownLocation = (idSetupCodeInfoParam.IncludeUnknownLocation == true) ? 1 : 0;

                //brand filter section
                if (brands != null)
                {
                    paramBuilder = new StringBuilder();
                    foreach (string brand in brands.SelectedBrands)
                        paramBuilder.Append(brand + ",");
                    if (paramBuilder.Length > 0)
                        paramBuilder.Remove(paramBuilder.Length - 1, 1);

                    searchParameter.Brands = paramBuilder.ToString();
                    searchParameter.ExcludeSelectedBrands = (brands.MatchSelectedBrands == true) ? 0 : 1;
                    // searchParameter.IncludeRecordWithoutBrandNumber = (brands.RecordWithoutBrandNumber == true) ? 0 : 1; 
                }

                //model filter params
                if (models != null)
                {
                    paramBuilder = new StringBuilder();
                    foreach (string model in models.SelectedModels)
                        paramBuilder.Append(model + ",");
                    if (paramBuilder.Length > 0)
                        paramBuilder.Remove(paramBuilder.Length - 1, 1);

                    searchParameter.Models = paramBuilder.ToString();
                    searchParameter.ExcludeSelectedModels = (models.MatchSelectedModels == true) ? 0 : 1;
                    //searchParameter.IncudeRecordWithoutModelName = (models.RecordWithoutModelName == true) ? 0 : 1;
                    //searchParameter.RemoveNonCodeBookModels = (models.RemoveNonCodeBookModels == true) ? 1 : 0;
                }
                searchParameter.IncudeRecordWithoutModelName = (models.RecordWithoutModelName == true) ? 0 : 1;
                searchParameter.RemoveNonCodeBookModels = (models.RemoveNonCodeBookModels == true) ? 1 : 0;

                //misc filters
                if (miscFilters != null)
                {
                    searchParameter.IncludeRestrictedIDs = miscFilters.IncludeRestrictedIds;
                    searchParameter.DuplicatedOutputs = miscFilters.KeepDuplicatedOutputs;
                    searchParameter.TNLink = (miscFilters.SelectTNLink == true) ? 0 : 1;
                    searchParameter.SearchFlag = "0";
                    if (miscFilters.DateBasedSearchEnabled)
                    {
                        searchParameter.SearchFlag = Convert.ToInt32(miscFilters.ModelInfoOptions).ToString();
                        searchParameter.StartDate = miscFilters.FromDate;
                        searchParameter.EndDate = miscFilters.ToDate;
                    }

                }
                
                ErrorLogging.WriteToLogFile("--Function Name: GetModelInformation--", false);
                ErrorLogging.WriteToLogFile("UEI2_Sp_GetModelReport Input Parameters", false);
                ErrorLogging.WriteToLogFile("####################### Input ############################", false);
                ErrorLogging.WriteToLogFile("IDS            :" + searchParameter.Ids, false);
                ErrorLogging.WriteToLogFile("LOCATION XML   :" + searchParameter.XMLLocations, false);
                ErrorLogging.WriteToLogFile("DEVICE XML     :" + searchParameter.XMLDevices, false);
                ErrorLogging.WriteToLogFile("DATASOURCE     :" + searchParameter.DataSources, false);
                ErrorLogging.WriteToLogFile("##########################################################", false);   
                
                DateTime spStart = DateTime.Now;
                ErrorLogging.WriteToLogFile("UEI2_Sp_GetModelReport execution time", false);
                ErrorLogging.WriteToLogFile("Start Time: " + spStart.Hour.ToString() + ":" + spStart.Minute.ToString() + ":" + spStart.Second.ToString() + ":" + spStart.Millisecond.ToString(), false);
                resultCollection = _DBAccess.GetModelInformation(searchParameter);

                DateTime spEnd = DateTime.Now;
                ErrorLogging.WriteToLogFile("End Time: " + spEnd.Hour.ToString() + ":" + spEnd.Minute.ToString() + ":" + spEnd.Second.ToString() + ":" + spEnd.Millisecond.ToString(), false);

                totalExecTime = spEnd - spStart;
                ErrorLogging.WriteToLogFile("Total sp Time: " + totalExecTime.Hours.ToString() + ":" + totalExecTime.Minutes.ToString() + ":" + totalExecTime.Seconds.ToString() + ":" + totalExecTime.Milliseconds.ToString(), false);

                if(resultCollection != null)
                {
                    //Revert Back Alias ID Replacement
                    if(idSetupCodeInfoParam.AliasIDList != null)
                    {
                        resultCollection = ProcessIDWithAlias(resultCollection, idSetupCodeInfoParam);
                    }
                }
                return resultCollection;

            }
            catch (Exception ex)
            {
                ex.Data.Add("parameter", searchParameter);

                throw ex;
            }
        }
        private Model.ModelInfoRecordCollection CreateModelInfoReport(IDBrandResultCollection resultCollection)
        {
            ErrorLogging.WriteToLogFile("--Function Name: CreateModelInfoReport--", false);
            ErrorLogging.WriteToLogFile("Business logic execution time", false);
            DateTime blStart = DateTime.Now;
            ErrorLogging.WriteToLogFile("Start Time: " + blStart.Hour.ToString() + ":" + blStart.Minute.ToString() + ":" + blStart.Second.ToString() + ":" + blStart.Millisecond.ToString(), false);

            Model.ModelInfoRecordCollection modelInfoReport = new Model.ModelInfoRecordCollection();


            if (resultCollection != null)
                foreach (IDBrandResult result in resultCollection)
                {
                    Model.ModelInfoRecord record = new UEI.Workflow2010.Report.Model.ModelInfoRecord();
                    record.Mode = result.Mode;
                    record.Brand = result.Brand;
                    record.AliasBrand = result.AliasBrand;
                    record.AliasType = result.AliasType;
                    record.IsTarget = result.TRModel;
                    record.Model = result.Model;
                    record.ID = result.ID;
                    record.DataSource = result.DataSource;
                    record.TNLink = result.TN;
                    record.Region = result.Region;
                    record.SubRegion = result.SubRegion;
                    record.Country = result.Country;
                    //record.DeviceType = result.DeviceType;
                    record.MainDevice = result.MainDevice;
                    record.SubDevice = result.SubDevice;
                    record.Component = result.Component;
                    record.ComponentStatus = (result.ComponentStatus == 0) ? "" : "Built In";
                    record.TrustLevel = result.TrustLevel;

                    modelInfoReport.Add(record);
                }
                    //modelInfoReport.Add(new UEI.Workflow2010.Report.Model.ModelInfoRecord(result.Brand, result.IsTargetModel, result.Model, result.ID, result.DataSource, result.TNLink));



            DateTime blEnd = DateTime.Now;
            ErrorLogging.WriteToLogFile("End Time: " + blEnd.Hour.ToString() + ":" + blEnd.Minute.ToString() + ":" + blEnd.Second.ToString() + ":" + blEnd.Millisecond.ToString(), false);

            TimeSpan totalBlTime = blEnd - blStart;
            ErrorLogging.WriteToLogFile("Total BL Time: " + totalBlTime.Hours.ToString() + ":" + totalBlTime.Minutes.ToString() + ":" + totalBlTime.Seconds.ToString() + ":" + totalBlTime.Milliseconds.ToString(), false);

            totalExecTime = totalExecTime + totalBlTime;
            ErrorLogging.WriteToLogFile("Total Time: " + totalExecTime.Hours.ToString() + ":" + totalExecTime.Minutes.ToString() + ":" + totalExecTime.Seconds.ToString() + ":" + totalExecTime.Milliseconds.ToString(), false);


            return modelInfoReport;
        }
        public Model.ModelInfoRecordCollection GetModelInfoReport(IList<Model.Id> ids, IList<Model.Region> locations, IList<Model.DataSources> dataSources, List<String> selectedModes, IList<Model.DeviceType> deviceTypes, Model.BrandFilter brands, Model.ModelFilter models, Model.ModelInfoSpecific miscFilters, Model.IdSetupCodeInfo idSetupCodeInfoParam)
        {
            try
            {
                if (ids != null && locations != null && dataSources != null && ids.Count > 0)
                {
                    IDBrandResultCollection resultCollection = GetModelInformation(ids, locations, dataSources,selectedModes, deviceTypes,  brands, models, miscFilters,idSetupCodeInfoParam);
                    return CreateModelInfoReport(resultCollection);
                }

                return null;
            }
            catch(Exception ex)
            {
                ErrorLogging.WriteToErrorLogFile(ex);

                throw;
            }
        }       
        #endregion

        #region Quickset Reports
        public Model.ModelInfoRecordCollection GeQuickSetMergeReport(Model.ModelInfoRecordCollection oldRecords,Model.ModelInfoRecordCollection newRecords)
        {
            try
            {
                if (oldRecords != null)
                {
                    foreach (Model.ModelInfoRecord item in oldRecords)
                    {
                        newRecords.Add(item);
                    }
                    return newRecords;
                }
                return null;
            }
            catch (Exception ex)
            {
                ErrorLogging.WriteToErrorLogFile(ex);
                throw;
            }
        } 
        public Model.ModelInfoRecordCollection GeQuickSetReport(IList<Model.Id> ids, IList<Model.Region> locations, IList<Model.DataSources> dataSources, List<String> selectedModes, IList<Model.DeviceType> deviceTypes, Model.BrandFilter brands, Model.ModelFilter models, Model.ModelInfoSpecific miscFilters, Model.IdSetupCodeInfo idSetupCodeInfoParam)
        {
            try
            {
                if (ids != null && locations != null && dataSources != null && ids.Count > 0)
                {
                    //Get Model Info Report
                    IDBrandResultCollection modelInfoReport = GetModelInformation(ids, locations, dataSources, selectedModes, deviceTypes, brands, models, miscFilters, idSetupCodeInfoParam);
                    return CreateModelInfoReport(modelInfoReport);                    
                }

                return null;
            }
            catch (Exception ex)
            {
                ErrorLogging.WriteToErrorLogFile(ex);

                throw;
            }
        }       
        #endregion

        #region BrandModelSearch

        public Model.ModelInfoRecordCollection GetBrandModelReport(IList<string> ids, IList<Model.Region> regions, IList<Model.DataSources> dataSources,List<string> brands,List<string> models)
        {
            IDSearchParameter searchParam = null;
            IDBrandResultCollection resultCollection = null;
            try
            {

                if (ids != null && regions != null && dataSources != null && brands != null && models != null)
                {
                    searchParam = CreateBrandModelSearchParam(ids, regions, dataSources, brands, models);

                    if (searchParam != null)
                    {

                        ErrorLogging.WriteToLogFile("--Function Name: GetBrandModelReport--", false);
                        ErrorLogging.WriteToLogFile("UEI2_Sp_GetModelReport execution time", false);
                        DateTime spStart = DateTime.Now;
                        ErrorLogging.WriteToLogFile("Start Time: " + spStart.Hour.ToString() + ":" + spStart.Minute.ToString() + ":" + spStart.Second.ToString() + ":" + spStart.Millisecond.ToString(), false);

                        resultCollection = _DBAccess.GetModelInformation(searchParam);

                        DateTime spEnd = DateTime.Now;
                        ErrorLogging.WriteToLogFile("End Time: " + spEnd.Hour.ToString() + ":" + spEnd.Minute.ToString() + ":" + spEnd.Second.ToString() + ":" + spEnd.Millisecond.ToString(), false);

                        totalExecTime = spEnd - spStart;
                        ErrorLogging.WriteToLogFile("Total sp Time: " + totalExecTime.Hours.ToString() + ":" + totalExecTime.Minutes.ToString() + ":" + totalExecTime.Seconds.ToString() + ":" + totalExecTime.Milliseconds.ToString(), false);

                        if (resultCollection != null)
                        {
                            //The method is shared between ModelInfoReport and BrandModelSearch since
                            //both have same model class.
                            return CreateModelInfoReport(resultCollection);
                        }
                    }

                }

                return null;
            }
            catch(Exception ex)
            {
                ErrorLogging.WriteToErrorLogFile(ex, searchParam);

                throw ex;
            }
        }
        /// <summary>
        /// Creates the parameter object for passing to db classes.
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="regions"></param>
        /// <param name="dataSources"></param>
        /// <param name="brands"></param>
        /// <param name="models"></param>
        /// <returns></returns>
        private IDSearchParameter CreateBrandModelSearchParam(IList<string> ids, IList<Model.Region> regions, IList<Model.DataSources> dataSources,List<string> brands,List<string> models)
        {
            try
            {
                if (ids.Count > 0 && regions.Count > 0 && dataSources.Count > 0 && brands.Count > 0 && models.Count > 0)
                {
                    IDSearchParameter searchParameter = new IDSearchParameter();

                    StringBuilder paramBuilder = new StringBuilder();

                    //create id string
                    foreach (string id in ids)
                        paramBuilder.Append(id + ",");
                    if (paramBuilder.Length > 0)
                        paramBuilder.Remove(paramBuilder.Length - 1, 1);

                    searchParameter.Ids = paramBuilder.ToString();

                    //create region string.
                    paramBuilder = new StringBuilder();
                    foreach (Model.Region region in regions)
                        paramBuilder.Append(region.Name + ",");
                    if (paramBuilder.Length > 0)
                        paramBuilder.Remove(paramBuilder.Length - 1, 1);

                    searchParameter.Regions = paramBuilder.ToString();

                    //add the datasource locations
                    paramBuilder = new StringBuilder();
                    if (dataSources != null)
                        foreach (Model.DataSources source in dataSources)
                            paramBuilder.Append(source.Name + ",");
                    if (paramBuilder.Length > 0)
                        paramBuilder.Remove(paramBuilder.Length - 1, 1);

                    searchParameter.DataSources = paramBuilder.ToString();

                    //add brands
                    paramBuilder = new StringBuilder();
                    foreach (string brand in brands)
                        paramBuilder.Append(brand + ",");
                    if (paramBuilder.Length > 0)
                        paramBuilder.Remove(paramBuilder.Length - 1, 1);

                    searchParameter.Brands = paramBuilder.ToString();

                    //add models
                    paramBuilder = new StringBuilder();
                    foreach (string model in models)
                        paramBuilder.Append(model + ",");
                    if (paramBuilder.Length > 0)
                        paramBuilder.Remove(paramBuilder.Length - 1, 1);

                    searchParameter.Models = paramBuilder.ToString();

                    //searchParameter.SearchFlag = "1";

                    return searchParameter;

                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region Get TN For Selected ID
        public Dictionary<String, List<Model.LabelIntronSearchRecord>> GetTNforSelectedID(String strID)
        {           
            Dictionary<String, List<Model.LabelIntronSearchRecord>> listofTns = null;
            try
            {
                IDFunctionCollection IDFunctionData = DAOFactory.ReadOnly().IDFunctionSelect(strID);
                if (IDFunctionData != null)
                {
                    foreach (IDFunction item in IDFunctionData)
                    {
                        if (item.IDTNCaptureList != null)
                        {
                            foreach (IDTNCapture item1 in item.IDTNCaptureList)
                            {
                                Model.LabelIntronSearchRecord newrecord = new Model.LabelIntronSearchRecord();
                                newrecord.Label = item1.Label;
                                newrecord.Data = item1.Data;
                                
                                if (listofTns == null)
                                    listofTns = new Dictionary<string, List<Model.LabelIntronSearchRecord>>();
                                String strTN = String.Format("TN{0:D5}", item1.TN);
                                if (!listofTns.ContainsKey(strTN))
                                {
                                    listofTns.Add(strTN, new List<Model.LabelIntronSearchRecord>());
                                    listofTns[strTN].Add(newrecord);
                                }
                                else
                                {
                                    listofTns[strTN].Add(newrecord);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLogging.WriteToErrorLogFile(ex);
            }
            return listofTns;
        }
        #endregion

        #region ExecIDTnInfo
        public List<Int32>  GetExecutorList()
        {
            try
            {
                LogText("--Function name: GetExecutorList--");
                LogText("--Executing query--");

                DateTime start = DateTime.Now;

                List<Int32> executorCodes = _DBAccess.GetExecutorCodes();

                DateTime stop = DateTime.Now;
                TimeSpan queryExSpan = stop - start;

                LogTime("Start Time : ", start);
                LogTime("End Time : ", stop);
                LogTimeSpan("Total execution time : ", queryExSpan);

                LogText("--End of function--");
                return executorCodes;
            }
            catch
            {
                return null;
            }
        }
        public List<Model.IDInfo> GetIDForExecutor(int executorCode)
        {
            List<Model.IDInfo> idList = new List<UEI.Workflow2010.Report.Model.IDInfo>();
            try
            {
                LogText("--Function name: GetIDForExecutor--");
                LogText("--Executing query--");

                DateTime start = DateTime.Now;
                
                List<string> ids = _DBAccess.GetIDListByExecutor(executorCode);

                DateTime stop = DateTime.Now;
                TimeSpan queryExSpan = stop - start;

                LogTime("Start Time : ", start);
                LogTime("End Time : ", stop);
                LogTimeSpan("Query execution time : ", queryExSpan);


                LogText("--Executing business logic--");
                start = DateTime.Now;

                foreach (string id in ids)
                {
                    Model.IDInfo idModel = new UEI.Workflow2010.Report.Model.IDInfo();
                    idModel.ID = id;
                    idModel.Executor = executorCode;
                    idList.Add(idModel);
                }
                
                stop = DateTime.Now;
                TimeSpan blSpan = stop - start;

                LogTime("Start Time : ", start);
                LogTime("End Time : ", stop);
                LogTimeSpan("BL execution time : ", blSpan);

                LogTimeSpan("Total execution : ",queryExSpan + blSpan);
                LogText("--End of function--");

                return idList;
            }
            catch
            {
                throw;
            }
        }      
        public void GetModelRecordForID(Model.IDInfo idModel)
        {
            if(idModel !=  null)
            {
               idModel.Prefixes = GetIDPrefix(idModel.ID);
                idModel.ModelRecords = GetModelInformation(idModel.ID);
            }
        }
        private Model.IDPrefixInfoCollection GetIDPrefix(string id)
        {
            Model.IDPrefixInfoCollection idPrefixList = new Model.IDPrefixInfoCollection();
            try
            {
                LogText("--Function Name: GetIDPrefix--");
                LogText("--Executing procedure [UEI2_BO_Sp_GetProtocolPrefixbyID]--");

                DateTime start = DateTime.Now;

                PrefixCollection prefixCollection =  _DBAccess.GetPrefixInfo(id);

                DateTime stop = DateTime.Now;
                TimeSpan queryExSpan = stop - start;

                LogTime("Start Time : ", start);
                LogTime("End Time : ", stop);
                LogTimeSpan("Procedure execution time : ", queryExSpan);

                LogText("--Executing business logic--");
                start = DateTime.Now;

                foreach (Prefix prefix in prefixCollection)
                {
                    Model.IDPrefixInfo idPrefix = new UEI.Workflow2010.Report.Model.IDPrefixInfo();
                    idPrefix.Data = prefix.Data;
                    idPrefix.Description = prefix.Description;
                    idPrefixList.Add(idPrefix);
                }

                stop = DateTime.Now;
                TimeSpan blSpan = stop - start;

                LogTime("Start Time : ", start);
                LogTime("End Time : ", stop);
                LogTimeSpan("BL execution time : ", blSpan);

                LogTimeSpan("Total execution : ", queryExSpan + blSpan);
                LogText("--End of function--");


                return idPrefixList;
            }
            catch
            {
                throw;
            }
        }
        private Model.ModelInfoRecordCollection GetModelInformation(string id)
        {
            Model.ModelInfoRecordCollection recordCollection = new Model.ModelInfoRecordCollection();

            LogText("--Function Name: GetModelInformation--");
            LogText("--Executing query--");

            DateTime start = DateTime.Now;
            IDBrandResultCollection resultCollection = _DBAccess.GetModelInfoById(id);

            DateTime stop = DateTime.Now;
            TimeSpan queryExSpan = stop - start;

            LogTime("Start Time : ", start);
            LogTime("End Time : ", stop);
            LogTimeSpan("Query execution time : ", queryExSpan);

            LogText("--Executing business logic--");
            start = DateTime.Now;

            if (resultCollection != null)
            {
                foreach (IDBrandResult result in resultCollection)
                {
                    Model.ModelInfoRecord modelRecord = new UEI.Workflow2010.Report.Model.ModelInfoRecord();
                    modelRecord.Brand = result.Brand;
                    modelRecord.Model = result.Model;
                    modelRecord.DataSource = result.DataSource;
                    modelRecord.IsTarget = result.TRModel;
                    modelRecord.TNLink = result.TN;
                    modelRecord.Country = result.Country;
                    modelRecord.Region = result.Region;
                    modelRecord.Comment = result.Comment;

                    //recordCollection.Add(new UEI.Workflow2010.Report.Model.ModelInfoRecord(result.Brand, result.IsTargetModel, result.Model, result.ID, result.DataSource, result.TNLink));
                    recordCollection.Add(modelRecord);

                }
            }

            stop = DateTime.Now;
            TimeSpan blSpan = stop - start;

            LogTime("Start Time : ", start);
            LogTime("End Time : ", stop);
            LogTimeSpan("BL execution time : ", blSpan);

            LogTimeSpan("Total execution : ", queryExSpan + blSpan);
            LogText("--End of function--");

            return recordCollection;
        }
        #endregion

        #region RestrictedID Report
        public Model.RestrictedIdModel GetRestrictedIDModel()
        {
            try
            {
                LogText("--Function Name:GetRestrictedIDModel--");
                LogText("--Executing Query--");

                Model.RestrictedIdModel idModel = new UEI.Workflow2010.Report.Model.RestrictedIdModel();

                DateTime start = DateTime.Now;

                List<String> resIdList = _DBAccess.GetRestrictedIDList();

                DateTime stop = DateTime.Now;
                TimeSpan queryExSpan = stop - start;

                LogTime("Start Time : ", start);
                LogTime("End Time : ", stop);
                LogTimeSpan("Query execution time : ", queryExSpan);

                foreach (String resId in resIdList)
                {
                    idModel.AddId(resId);                                       
                }

                LogText("--End of function--");
                return idModel;
            }
            catch(Exception ex)
            {                
                ErrorLogging.WriteToErrorLogFile(ex);
                return null;
            }
        }
        public static DataSet GetRestrictedIDDSForDisplay(Model.RestrictedIdModel idModel, Model.IDModeRecordModel allModeNames)
        {
            DataSet idSet = null;

            if (idModel != null)
            {
                idSet = new DataSet();
                
                DataTable idTable = idSet.Tables.Add("RestrictedIDs");
                idTable.Columns.Add("ID");

                List<string> modeList = idModel.GetDistinctModes();

                foreach (string mode in modeList)
                {
                    //add the mode header\
                    String modeName = allModeNames.getModeName(mode) + " (" + mode + ")";
                    
                    DataRow row = idTable.NewRow();
                    row[0] = modeName;
                    idTable.Rows.Add(row);

                    List<string> resIdList = idModel.GetIdsByMode(mode);

                    if (resIdList != null)
                    {
                        foreach (string id in resIdList)
                        {
                            row = idTable.NewRow();
                            row[0] = id;
                            idTable.Rows.Add(row);
                        }
                    }

                }
                idSet.AcceptChanges();

            }

            return idSet;
        }
        public static DataSet GetRestrictedIDDSForSaving(Model.RestrictedIdModel idModel,Model.IDModeRecordModel allModeNames)
        {
            DataSet idSet = null;

            if (idModel != null)
            {
                idSet = new DataSet();

                List<string> modeList = idModel.GetDistinctModes();

                foreach (string mode in modeList)
                {
                    DataTable idTable = idSet.Tables.Add(allModeNames.getModeName(mode));
                    idTable.Columns.Add("ID");

                    List<string> resIdList =  idModel.GetIdsByMode(mode);

                    if (resIdList != null)
                    {
                        foreach (string id in resIdList)
                        {
                            DataRow row = idTable.NewRow();
                            row[0] = id;
                            idTable.Rows.Add(row);
                        }
                    }
                    
                }
                idSet.AcceptChanges();
                
            }

            return idSet;
        }
        public static DataSet GetRestrictedIDDSForSaving(Model.RestrictedIdModel idModel)
        {
            DataSet idSet = null;

            if (idModel != null)
            {
                idSet = new DataSet();

                List<string> modeList = idModel.GetDistinctModes();

                foreach (string mode in modeList)
                {
                    DataTable idTable = idSet.Tables.Add(mode);
                    idTable.Columns.Add("ID");

                    List<string> resIdList = idModel.GetIdsByMode(mode);

                    if (resIdList != null)
                    {
                        foreach (string id in resIdList)
                        {
                            DataRow row = idTable.NewRow();
                            row[0] = id;
                            idTable.Rows.Add(row);
                        }
                    }

                }
                idSet.AcceptChanges();

            }

            return idSet;
        }        
        #endregion

        #region Empty/Non Empty Report

        public Model.Empty_NonEmptyIdModel GetEmptyNonEmptyIdReport(List<String> idList, bool isEmptyIDReport)
        {
            Model.Empty_NonEmptyIdModel reportModel = null;
            
            if (idList != null)
            {
                LogText("--Function Name: GetEmptyIDsFromList --");
                
                
                IDSearchParameter searchParameter = new IDSearchParameter();

                StringBuilder paramBuilder = new StringBuilder();
                //add the ids
                foreach (String id in idList)
                    paramBuilder.Append(id + ",");
                if (paramBuilder.Length > 0)
                    paramBuilder.Remove(paramBuilder.Length - 1, 1);

                searchParameter.Ids = paramBuilder.ToString();

                searchParameter.ReturnEmptyID = (isEmptyIDReport == true) ? 0 : 1;

                LogText("--UEI2_Sp_GetIDbyStatus  execution time--");

                DateTime start = DateTime.Now;

                List<String> filteredIDList = _DBAccess.FilterEmptyNonEmptyIDs(searchParameter);

                DateTime stop = DateTime.Now;
                TimeSpan queryExSpan = stop - start;

                LogTime("Start Time : ", start);
                LogTime("End Time : ", stop);
                LogTimeSpan("Query execution time : ", queryExSpan);

                
                if (filteredIDList != null && filteredIDList.Count > 0)
                {
                    LogText("--Executing business logic--");

                    start = DateTime.Now;

                    reportModel = new Model.Empty_NonEmptyIdModel(isEmptyIDReport);

                    foreach (String id in filteredIDList)
                        reportModel.AddId(id);

                    stop = DateTime.Now;
                    TimeSpan blSpan = stop - start;

                    LogTime("Start Time : ", start);
                    LogTime("End Time : ", stop);
                    LogTimeSpan("BL execution time : ", blSpan);

                    LogTimeSpan("Total execution : ", queryExSpan + blSpan);
                    
                }   
                

            }
            LogText("--End of function--");

            return reportModel;
        }

        public static DataSet GetEmptyNonEmptyIDDSForDisplay(Model.Empty_NonEmptyIdModel idModel, Model.IDModeRecordModel allModeNames)
        {
            DataSet idSet = null;

            if (idModel != null)
            {
                idSet = new DataSet();

                string dtName = "Non Empty IDs";
                if (idModel.IsEmptyIdModel)
                    dtName = "Empty IDs";

                DataTable idTable = idSet.Tables.Add(dtName);
                idTable.Columns.Add("ID");

                List<string> modeList = idModel.GetDistinctModes();

                foreach (string mode in modeList)
                {
                    //add the mode header\
                    String modeName = allModeNames.getModeName(mode) + " (" + mode + ")";

                    DataRow row = idTable.NewRow();
                    row[0] = modeName;
                    idTable.Rows.Add(row);

                    List<string> resIdList = idModel.GetIdsByMode(mode);

                    if (resIdList != null)
                    {
                        foreach (string id in resIdList)
                        {
                            row = idTable.NewRow();
                            row[0] = id;
                            idTable.Rows.Add(row);
                        }
                    }

                }
                idSet.AcceptChanges();

            }

            return idSet;
        }
        public static DataSet GetEmptyNonEmptyIDDSForSaving(Model.Empty_NonEmptyIdModel idModel, Model.IDModeRecordModel allModeNames)
        {
            DataSet idSet = null;
            if (idModel != null)
            {
                idSet = new DataSet();
                List<string> modeList = idModel.GetDistinctModes();
                foreach (string mode in modeList)
                {
                    DataTable idTable = idSet.Tables.Add(allModeNames.getModeName(mode));
                    idTable.Columns.Add("ID");

                    List<string> resIdList = idModel.GetIdsByMode(mode);

                    if (resIdList != null)
                    {
                        foreach (string id in resIdList)
                        {
                            DataRow row = idTable.NewRow();
                            row[0] = id;
                            idTable.Rows.Add(row);
                        }
                    }
                }
                idSet.AcceptChanges();
            }
            return idSet;
        }
        public static DataSet GetEmptyNonEmptyIDDSForSaving(Model.Empty_NonEmptyIdModel idModel)
        {
            DataSet idSet = null;

            if (idModel != null)
            {
                idSet = new DataSet();
                List<string> modeList = idModel.GetDistinctModes();
                foreach (string mode in modeList)
                {
                    DataTable idTable = idSet.Tables.Add(mode);
                    idTable.Columns.Add("ID");

                    List<string> resIdList = idModel.GetIdsByMode(mode);

                    if (resIdList != null)
                    {
                        foreach (string id in resIdList)
                        {
                            DataRow row = idTable.NewRow();
                            row[0] = id;
                            idTable.Rows.Add(row);
                        }
                    }
                }
                idSet.AcceptChanges();
            }
            return idSet;
        }
        #endregion

        #region LabelIntronSearch Report

        public Model.LabelIntronSearchReport GetLabelIntronSearchReport(bool isIntronSearch, IList<String> idList, string searchPattern,bool completeMatch)
        {
            try
            {
                LogText("--Function Name: GetLabelIntronSearchReport --");

                if (idList != null && idList.Count > 0)
                {
                   
                    //create the parameter
                    IDSearchParameter searchParameter = new IDSearchParameter();
                    
                    searchParameter.LabelOrIntronPattern = searchPattern;
                    searchParameter.LabelIntronSearchFlag = (isIntronSearch == true) ? 1 : 0;
                    searchParameter.CompleteLabelIntronMatch = (completeMatch == true) ? 1 : 0;


                    StringBuilder paramBuilder = new StringBuilder();
                    //add the ids
                    foreach (String id in idList)
                        paramBuilder.Append(id + ",");
                    if (paramBuilder.Length > 0)
                        paramBuilder.Remove(paramBuilder.Length - 1, 1);

                    searchParameter.Ids = paramBuilder.ToString();

                    LogText("--UEI2_Sp_IntronLabelSearch   execution time--");
                    DateTime start = DateTime.Now;
                    
                    FunctionCollection functions = _DBAccess.IntronLabelSearch(searchParameter);
                    
                    DateTime stop = DateTime.Now;
                    TimeSpan queryExSpan = stop - start;

                    LogTime("Start Time : ", start);
                    LogTime("End Time : ", stop);
                    LogTimeSpan("Query execution time : ", queryExSpan);

                    LogText("--Executing business logic--");
                    start = DateTime.Now;

                    Model.LabelIntronSearchReport report = CreateLabelIntronSearchReport(functions,isIntronSearch);

                    stop = DateTime.Now;
                    TimeSpan blSpan = stop - start;

                    LogTime("Start Time : ", start);
                    LogTime("End Time : ", stop);
                    LogTimeSpan("BL execution time : ", blSpan);

                    LogTimeSpan("Total execution : ", queryExSpan + blSpan);

                    LogText("--End of function--");

                    return report;


                }
                LogText("--End of function--");

                return null; 
            }
            catch(Exception ex)
            {
                ErrorLogging.WriteToErrorLogFile(ex);
                throw;
            }
        }
        public Model.LabelIntronSearchReport GetExcludeLabelIntronSearchReport(bool isIntronSearch, IList<String> idList, string searchPattern, bool completeMatch)
        {
            try
            {
                LogText("--Function Name: GetExcludeLabelIntronSearchReport --");

                if (idList != null && idList.Count > 0)
                {

                    //create the parameter
                    IDSearchParameter searchParameter = new IDSearchParameter();

                    searchParameter.LabelOrIntronPattern = searchPattern;
                    searchParameter.LabelIntronSearchFlag = (isIntronSearch == true) ? 1 : 0;
                    searchParameter.CompleteLabelIntronMatch = (completeMatch == true) ? 1 : 0;


                    StringBuilder paramBuilder = new StringBuilder();
                    //add the ids
                    foreach (String id in idList)
                        paramBuilder.Append(id + ",");
                    if (paramBuilder.Length > 0)
                        paramBuilder.Remove(paramBuilder.Length - 1, 1);

                    searchParameter.Ids = paramBuilder.ToString();

                    LogText("--UEI2_Sp_IntronLabelSearch_Test   execution time--");
                    DateTime start = DateTime.Now;

                    FunctionCollection functions = _DBAccess.ExcludeIntronLabelSearch(searchParameter);

                    DateTime stop = DateTime.Now;
                    TimeSpan queryExSpan = stop - start;

                    LogTime("Start Time : ", start);
                    LogTime("End Time : ", stop);
                    LogTimeSpan("Query execution time : ", queryExSpan);

                    LogText("--Executing business logic--");
                    start = DateTime.Now;

                    Model.LabelIntronSearchReport report = CreateLabelIntronSearchReport(functions, isIntronSearch);

                    stop = DateTime.Now;
                    TimeSpan blSpan = stop - start;

                    LogTime("Start Time : ", start);
                    LogTime("End Time : ", stop);
                    LogTimeSpan("BL execution time : ", blSpan);

                    LogTimeSpan("Total execution : ", queryExSpan + blSpan);

                    LogText("--End of function--");

                    return report;


                }
                LogText("--End of function--");

                return null;
            }
            catch (Exception ex)
            {
                ErrorLogging.WriteToErrorLogFile(ex);
                throw;
            }
        }

        private Model.LabelIntronSearchReport CreateLabelIntronSearchReport(FunctionCollection functions, bool isIntronSearch)
        {

            if (functions != null && functions.Count > 0)
            {
                Model.LabelIntronSearchReport report = new Model.LabelIntronSearchReport(isIntronSearch);

                foreach (Function function in functions)
                {
                  //create record
                    Model.LabelIntronSearchRecord record = new Model.LabelIntronSearchRecord();
                    record.ID = function.ID;
                    record.Intron = function.Intron;
                    record.Label = function.Label;
                    record.Priority = function.IntronPriority;
                    record.Synth = function.Synth;
                    record.Data = function.Data;

                   report.AddRecord(record);   
                }

               
                return report;
            }

            return null;
        }

        
        public Model.IntronLabelDictionary GetIntronLabelDictionary()
        {
            try
            {
                LogText("--Function Name: GetLabelIntronDictionary --");
                Model.IntronLabelDictionary dictionary = new UEI.Workflow2010.Report.Model.IntronLabelDictionary();
            
                HashtableCollection data = _DBAccess.GetIntronLabelDictionary();

                foreach (Hashtable content in data)
                    dictionary.AddItem(content);


               LogText("--End of function--");

               return dictionary;
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    ErrorLogging.WriteToErrorLogFile(ex.InnerException);
                else
                    ErrorLogging.WriteToErrorLogFile(ex);

                throw;
            }


        }
        #endregion

        #region Load Regions
        public IList<Model.Region> GetRegions()
        {            
            CountryCollection _countryList = _DBAccess.GetAllRegionList();
            IList<Model.Region> _regionList = new List<Model.Region>();
            if (_countryList.Count > 0)
            {
                //_regionList.Add(new Model.Region("0", "All Markets"));
                foreach (BusinessObject.Country _country in _countryList)
                {
                    if (Convert.ToString(_country.Region) != null)
                    {
                        _regionList.Add(new Model.Region(_country.Code.ToString(), _country.Region.ToString()));
                    }
                }
            }
            return _regionList;
        }
        #endregion

        #region Locations
        public Model.LocationModel GetLocations()
        {
            Model.LocationModel locationModel = null;
            ErrorLogging.WriteToLogFile("Function Name: LoadCompleteLocations", false);
            DateTime spStart = DateTime.Now;
            CountryCollection resultList = _DBAccess.GetLocations();
            DateTime spEnd = DateTime.Now;
            totalExecTime = spEnd - spStart;
            ErrorLogging.WriteToLogFile("dbo.UEI2_Sp_GetLocations execution time", false);
            ErrorLogging.WriteToLogFile("Start Time: " + spStart.Hour.ToString() + ":" + spStart.Minute.ToString() + ":" + spStart.Second.ToString() + ":" + spStart.Millisecond.ToString(), false);
            ErrorLogging.WriteToLogFile("End Time: " + spEnd.Hour.ToString() + ":" + spEnd.Minute.ToString() + ":" + spEnd.Second.ToString() + ":" + spEnd.Millisecond.ToString(), false);
            ErrorLogging.WriteToLogFile("Total sp Time: " + totalExecTime.Hours.ToString() + ":" + totalExecTime.Minutes.ToString() + ":" + totalExecTime.Seconds.ToString() + ":" + totalExecTime.Milliseconds.ToString(), false);
            if (resultList != null)
            {
                locationModel = GetLocationModel(resultList);
            }
            return locationModel;
        }
        private Model.LocationModel GetLocationModel(CountryCollection resultList)
        {
            Model.LocationModel locationsModel = new Model.LocationModel();
            if (resultList.Count > 0)
            {
                foreach (Country item in resultList)
                {
                    locationsModel.Add(new Model.Location(item.Region, item.SubRegion, item.AlternateRegion, item.Name)); 
                }
            }
            return locationsModel;
        }
        #endregion

        #region Load Sub Regions
        public IList<Model.Region> GetSubRegions(Model.Region _region)
        {
            IDSearchParameter param = new IDSearchParameter();
            param.Regions = _region.Name;

            CountryCollection _countryList = _DBAccess.GetAllSubRegionList(param);
            IList<Model.Region> _subregionList = new List<Model.Region>();
            if (_countryList.Count > 0)
            {
               // _subregionList.Add(new Model.Region("0", "Select All"));
                foreach (BusinessObject.Country _country in _countryList)
                {
                    if (Convert.ToString(_country.Name) != null)
                    {
                        _subregionList.Add(new Model.Region(_country.Code.ToString(), _country.Name.ToString()));
                    }
                }
            }
            return _subregionList;
        }
        #endregion

        #region Load Countries
        public IList<Model.Country> GetCountries(Model.Region _regions, Model.Region _subregions,Boolean searchFlag)
        {            
            IDSearchParameter param = new IDSearchParameter();
            param.Regions = _regions.Name;
            if (_subregions != null)
                param.SubRegions = _subregions.Name;

            //Search Flag for Region Name or Alternate Region Name
            //0 Indicates Region as Input
            //1 Indicates Alternate Region as Input
            if (searchFlag == true)
                param.SearchFlag = "0";
            else
                param.SearchFlag = "1";
            CountryCollection _countryList = _DBAccess.GetAllCountryListByRegion(param);

            IList<Model.Country> _countriesList = new List<Model.Country>();
            foreach (BusinessObject.Country _country in _countryList)
            {
                if (Convert.ToString(_country.Name) != null)
                {
                    _countriesList.Add(new Model.Country(_country.Code.ToString(), _country.Name.ToString()));
                }
            }
            return _countriesList;
        }
        #endregion       

        #region Load Device Types
        public Model.DeviceModel LoadCompleteDevices(String _selectedType,List<String> _selectedModes, IList<Model.Id> ids, IList<Model.DataSources> _dataSources, IList<Model.Region> locations, Model.IdSetupCodeInfo idSetupCodeInfoParam)
        {
            try
            {
                StringBuilder paramBuilder = new StringBuilder();
                String strIds = String.Empty;
                foreach (Model.Id id in ids)
                    paramBuilder.Append(id.ID + ",");
                if (paramBuilder.Length > 0)
                    paramBuilder.Remove(paramBuilder.Length - 1, 1);
                strIds = paramBuilder.ToString();

                paramBuilder = new StringBuilder();
                String strModes = String.Empty;
                foreach (String mode in _selectedModes)
                    paramBuilder.Append(mode + ",");
                if (paramBuilder.Length > 0)
                    paramBuilder.Remove(paramBuilder.Length - 1, 1);
                strModes = paramBuilder.ToString();

                //add locations xml
                String strLocations = String.Empty;
                if (locations != null)
                {
                    _XML = new XMLOperations("UEI2Server", XMLOperations.EncodingType.UTF16);
                    _XML.ParentNode = "Document";
                    _XML.ParentAttributeName = "Type";
                    _XML.ParentAttributeValue = "LocationSelections";
                    _XML.ParentAttributeName = "Version";
                    _XML.ParentAttributeValue = "1.0.0";
                    _XML.AppendParent();

                    foreach (Model.Region r in locations)
                    {
                        _XML.ParentNode = "Region";
                        _XML.ParentAttributeName = "Name";
                        _XML.ParentAttributeValue = r.Name;
                        _XML.ParentAttributeName = "Weight";
                        _XML.ParentAttributeValue = "0";
                        _XML.ParentAttributeName = "SystemFlags";
                        if (idSetupCodeInfoParam.IncludeNonCountryLinkedRecords == true)
                        {
                            _XML.ParentAttributeValue = "2";
                        }
                        else
                        {
                            _XML.ParentAttributeValue = "0";
                        }
                        if (r.SubRegions != null)
                        {
                            foreach (Model.Region subregion in r.SubRegions)
                            {
                                _XML.ChildNode = "SubRegion";
                                _XML.ChildAttributeName = "Name";
                                _XML.ChildAttributeValue = subregion.Name;
                                _XML.ChildAttributeName = "Weight";
                                _XML.ChildAttributeValue = "0";
                                _XML.ChildAttributeName = "SystemFlags";
                                _XML.ChildAttributeValue = "0";
                                if (r.Countries != null)
                                {
                                    foreach (Model.Country country in r.Countries)
                                    {
                                        if (subregion.Name == country.SubRegion)
                                        {
                                            _XML.ChildNode = "Country";
                                            _XML.ChildAttributeName = "Name";
                                            _XML.ChildAttributeValue = country.Name;
                                            _XML.ChildAttributeName = "Weight";
                                            _XML.ChildAttributeValue = "0";
                                            _XML.ChildAttributeName = "SystemFlags";
                                            _XML.ChildAttributeValue = "0";
                                            _XML.AppendChild();
                                        }
                                    }
                                }
                                _XML.AppendChild();
                            }
                        }
                        if (r.Countries != null)
                        {
                            foreach (Model.Country country in r.Countries)
                            {
                                if (country.SubRegion == String.Empty)
                                {
                                    _XML.ChildNode = "Country";
                                    _XML.ChildAttributeName = "Name";
                                    _XML.ChildAttributeValue = country.Name;
                                    _XML.ChildAttributeName = "Weight";
                                    _XML.ChildAttributeValue = "0";
                                    _XML.ChildAttributeName = "SystemFlags";
                                    _XML.ChildAttributeValue = "0";
                                    _XML.AppendChild();
                                }
                            }
                        }
                        _XML.AppendParent();
                    }
                }
                strLocations = _XML.XMLGenerator();
                _XML = null;

                String strdataSources = String.Empty;
                foreach (Model.DataSources dataSource in _dataSources)
                    strdataSources += dataSource.Name + ",";
                if (strdataSources.Length > 0)
                    strdataSources = strdataSources.Remove(strdataSources.Length - 1, 1);                
                

                IDSearchParameter param = new IDSearchParameter();
                switch (_selectedType)
                {
                    case "RANGE":
                        param.Ids = strIds;
                        param.IDModeFilter = 0;
                        break;
                    case "MODE":
                        param.Ids = strModes;
                        param.IDModeFilter = 1;
                        break;
                    case "FILE":
                        param.Ids = strIds;
                        param.IDModeFilter = 0;
                        break;
                }                
                param.XMLLocations      = strLocations;
                param.DataSources       = strdataSources;
                param.IncludeUnknownLocation = (idSetupCodeInfoParam.IncludeUnknownLocation == true) ? 1 : 0;                                
                
                ErrorLogging.WriteToLogFile("Function Name: LoadCompleteDevices", false);
                ErrorLogging.WriteToLogFile("dbo.UEI2_Sp_ModeIDDeviceClassification Input Parameters", false);
                ErrorLogging.WriteToLogFile("##########################################################", false);
                ErrorLogging.WriteToLogFile("IDS            :" + strIds, false);
                ErrorLogging.WriteToLogFile("LOCATION XML   :" + strLocations, false);
                ErrorLogging.WriteToLogFile("DATASOURCE     :" + strdataSources, false);
                ErrorLogging.WriteToLogFile("##########################################################", false);

                DateTime spStart = DateTime.Now;
                DeviceTypeCollection resultList = _DBAccess.GetCompleteDevices(param);
                DateTime spEnd = DateTime.Now;
                totalExecTime = spEnd - spStart;

                ErrorLogging.WriteToLogFile("dbo.UEI2_Sp_ModeIDDeviceClassification execution time", false);
                ErrorLogging.WriteToLogFile("Start Time: " + spStart.Hour.ToString() + ":" + spStart.Minute.ToString() + ":" + spStart.Second.ToString() + ":" + spStart.Millisecond.ToString(), false);
                ErrorLogging.WriteToLogFile("End Time: " + spEnd.Hour.ToString() + ":" + spEnd.Minute.ToString() + ":" + spEnd.Second.ToString() + ":" + spEnd.Millisecond.ToString(), false);
                ErrorLogging.WriteToLogFile("Total sp Time: " + totalExecTime.Hours.ToString() + ":" + totalExecTime.Minutes.ToString() + ":" + totalExecTime.Seconds.ToString() + ":" + totalExecTime.Milliseconds.ToString(), false);

                List<Model.DeviceType> mainDeviceList = new List<Model.DeviceType>();

                if (resultList != null)
                    return GetDeviceModel(resultList);

                return new Model.DeviceModel();
            }
            catch(Exception ex)
            {
                ErrorLogging.WriteToErrorLogFile(ex);
                throw;
            }
            //return mainDeviceList;
        }
        private Model.DeviceModel GetDeviceModel(DeviceTypeCollection deviceRecords)
        {
            Model.DeviceModel deviceModel = new UEI.Workflow2010.Report.Model.DeviceModel();           
            foreach (DeviceType deviceType in deviceRecords)
            {
                Model.DeviceType mainDevice = deviceModel.FindDevice(deviceType.Mode, deviceType.Name);
                if (mainDevice == null)
                {
                    Model.DeviceType newMainDevice = new UEI.Workflow2010.Report.Model.DeviceType();
                    newMainDevice.Name = deviceType.Name;
                    newMainDevice.Mode = deviceType.Mode;
                    //add subdevice
                    Model.DeviceType newSubDevice = new UEI.Workflow2010.Report.Model.DeviceType();
                    newSubDevice.Name = deviceType.SubDevice;
                    newSubDevice.Mode = deviceType.Mode;
                    // component
                    if (!String.IsNullOrEmpty(deviceType.Component))
                    {
                        Model.Component newDeviceComp = new UEI.Workflow2010.Report.Model.Component();
                        newDeviceComp.SubDeviceType = deviceType.SubDevice;
                        newDeviceComp.Name = deviceType.Component;
                        newDeviceComp.Status = deviceType.ComponentStatus;

                        newSubDevice.Components.Add(newDeviceComp);
                    }
                    newMainDevice.SubDevices.Add(newSubDevice);
                    deviceModel.AddDevice(newMainDevice);
                }
                else
                {
                    //check for subdevice
                    Model.DeviceType subDevice = mainDevice.FindSubDevice(deviceType.SubDevice);
                    if (subDevice == null)
                    {
                        //add subdevice
                        Model.DeviceType newSubDevice = new UEI.Workflow2010.Report.Model.DeviceType();
                        newSubDevice.Name = deviceType.SubDevice;
                        newSubDevice.Mode = deviceType.Mode;
                        // component
                        if (!String.IsNullOrEmpty(deviceType.Component))
                        {
                            Model.Component newDeviceComp = new UEI.Workflow2010.Report.Model.Component();
                            newDeviceComp.SubDeviceType = deviceType.SubDevice;
                            newDeviceComp.Name = deviceType.Component;
                            newDeviceComp.Status = deviceType.ComponentStatus;

                            newSubDevice.Components.Add(newDeviceComp);

                        }
                        mainDevice.SubDevices.Add(newSubDevice);
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(deviceType.Component))
                        {
                            Model.Component newDeviceComp = new UEI.Workflow2010.Report.Model.Component();
                            newDeviceComp.SubDeviceType = deviceType.SubDevice;
                            newDeviceComp.Name = deviceType.Component;
                            newDeviceComp.Status = deviceType.ComponentStatus;

                            subDevice.Components.Add(newDeviceComp);

                        }
                    }

                }

            }

            return deviceModel;
        }
        private Model.DeviceType deviceToMatch = null;
        private bool FindDevice(Model.DeviceType device)
        {
            if (deviceToMatch != null)
            {
                if (device.Name == deviceToMatch.Name && device.Mode == deviceToMatch.Mode)
                    return true;
                else
                    return false;
            }

            return false;
        }        
        public IList<Model.DeviceType> LoadDeviceTypes(IList<String> _modes,IList<Model.DataSources> _dataSources, IList<Model.Region> _regions)
        {
            String modes = String.Empty;
            foreach (String mode in _modes)
                modes += mode + ",";
            if(modes.Length>0)
                modes = modes.Remove(modes.Length - 1, 1);

            String dataSources = String.Empty;
            foreach (Model.DataSources dataSource in _dataSources)
                dataSources += dataSource.Name + ",";
            if(dataSources.Length>0)
                dataSources = dataSources.Remove(dataSources.Length - 1, 1);
                            
            String regions = String.Empty;
            String subregions = String.Empty;
            String countries = String.Empty;            
            foreach (Model.Region region in _regions)
            {
                regions += region.Name + ",";
                if (region.SubRegions != null)
                {
                    foreach (Model.Region subregion in region.SubRegions)
                        subregions += subregion.Name + ",";
                }
                if (region.Countries != null)
                {
                    foreach (Model.Country country in region.Countries)
                        countries += country.Name + ",";
                }
            }
            if(regions.Length>0)
                regions = regions.Remove(regions.Length - 1, 1);

            if (subregions.Length > 0)
                subregions = subregions.Remove(subregions.Length - 1, 1);

            //foreach (Model.Country country in _countries)
            //    countries += country.Name + ",";
            if (countries.Length > 0)
                countries = countries.Remove(countries.Length - 1, 1);
            
            IDSearchParameter param = new IDSearchParameter();
            param.Ids = modes;
            param.Regions = regions;
            param.SubRegions = subregions;
            param.Countries = countries;
            param.DataSources = dataSources;
            param.IDModeFilter = 1;
            param.RegionType = 0;
            ErrorLogging.WriteToLogFile("Function Name: LoadDeviceTypes", false);
            DateTime spStart = DateTime.Now;
            DeviceTypeCollection resultList = _DBAccess.GetDeviceTypeList(param);
            DateTime spEnd = DateTime.Now;
            totalExecTime = spEnd - spStart;

            ErrorLogging.WriteToLogFile("UEI2_Sp_ModeDevicetypeList execution time", false);
            ErrorLogging.WriteToLogFile("Start Time: " + spStart.Hour.ToString() + ":" + spStart.Minute.ToString() + ":" + spStart.Second.ToString() + ":" + spStart.Millisecond.ToString(), false);
            ErrorLogging.WriteToLogFile("End Time: " + spEnd.Hour.ToString() + ":" + spEnd.Minute.ToString() + ":" + spEnd.Second.ToString() + ":" + spEnd.Millisecond.ToString(), false);
            ErrorLogging.WriteToLogFile("Total sp Time: " + totalExecTime.Hours.ToString() + ":" + totalExecTime.Minutes.ToString() + ":" + totalExecTime.Seconds.ToString() + ":" + totalExecTime.Milliseconds.ToString(), false);

            IList<Model.DeviceType> subdeviceList = new List<Model.DeviceType>();
            //if(resultList.Count > 1)
            //    deviceList.Add(new Model.DeviceType("0", "All"));
            
            foreach (DeviceType deviceType in resultList)
            {
                if (deviceType.Device != String.Empty)
                    subdeviceList.Add(new Model.DeviceType(deviceType.Mode, deviceType.DeviceID.ToString(), deviceType.Device.ToString()));
            }
            return subdeviceList;
        }
        #endregion

        #region Load Sub Devices
        public IList<Model.DeviceType> LoadSubDeviceTypes(IList<String> _modes, IList<Model.DataSources> _dataSources, IList<Model.Region> _regions, Model.DeviceType _device)
        {
            String modes = String.Empty;
            foreach (String mode in _modes)
                modes += mode + ",";
            if (modes.Length > 0)
                modes = modes.Remove(modes.Length - 1, 1);

            String dataSources = String.Empty;
            foreach (Model.DataSources dataSource in _dataSources)
                dataSources += dataSource.Name + ",";
            if (dataSources.Length > 0)
                dataSources = dataSources.Remove(dataSources.Length - 1, 1);

            String regions = String.Empty;
            String subregions = String.Empty;
            String countries = String.Empty;
            foreach (Model.Region region in _regions)
            {
                regions += region.Name + ",";
                if (region.SubRegions != null)
                {
                    foreach (Model.Region subregion in region.SubRegions)
                        subregions += subregion.Name + ",";
                }
                if (region.Countries != null)
                {
                    foreach (Model.Country country in region.Countries)
                        countries += country.Name + ",";
                }
            }
            if (regions.Length > 0)
                regions = regions.Remove(regions.Length - 1, 1);

            if (subregions.Length > 0)
                subregions = subregions.Remove(subregions.Length - 1, 1);

            if (countries.Length > 0)
                countries = countries.Remove(countries.Length - 1, 1);
            
            IDSearchParameter param = new IDSearchParameter();
            param.Ids = modes;
            param.DeviceTypes = _device.Name;
            param.Regions = regions;
            param.SubRegions = subregions;
            param.Countries = countries;
            param.DataSources = dataSources;
            param.IDModeFilter = 1;
            param.RegionType = 0;
            ErrorLogging.WriteToLogFile("Function Name: LoadSubDeviceTypes", false);
            DateTime spStart = DateTime.Now;
            DeviceTypeCollection resultList = _DBAccess.GetSubDeviceTypeList(param);
            DateTime spEnd = DateTime.Now;
            totalExecTime = spEnd - spStart;

            ErrorLogging.WriteToLogFile("UEI2_Sp_ModeSubDevicetypeList execution time", false);
            ErrorLogging.WriteToLogFile("Start Time: " + spStart.Hour.ToString() + ":" + spStart.Minute.ToString() + ":" + spStart.Second.ToString() + ":" + spStart.Millisecond.ToString(), false);
            ErrorLogging.WriteToLogFile("End Time: " + spEnd.Hour.ToString() + ":" + spEnd.Minute.ToString() + ":" + spEnd.Second.ToString() + ":" + spEnd.Millisecond.ToString(), false);
            ErrorLogging.WriteToLogFile("Total sp Time: " + totalExecTime.Hours.ToString() + ":" + totalExecTime.Minutes.ToString() + ":" + totalExecTime.Seconds.ToString() + ":" + totalExecTime.Milliseconds.ToString(), false);

            IList<Model.DeviceType> deviceList = new List<Model.DeviceType>();
            //if(resultList.Count > 1)
            //    deviceList.Add(new Model.DeviceType("0", "All"));

            foreach (DeviceType deviceType in resultList)
            {
                if (deviceType.SubDevice != String.Empty)
                    deviceList.Add(new Model.DeviceType(deviceType.SubDeviceID.ToString(), deviceType.SubDevice.ToString()));
            }
            return deviceList;
        }
        #endregion

        #region Load Components
        public IList<Model.Component> LoadComponents(IList<String> _modes, IList<Model.DataSources> _dataSources, IList<Model.Region> _regions, Model.DeviceType _device, Model.DeviceType _subdevice)
        {
            String modes = String.Empty;
            foreach (String mode in _modes)
                modes += mode + ",";
            if (modes.Length > 0)
                modes = modes.Remove(modes.Length - 1, 1);

            String dataSources = String.Empty;
            foreach (Model.DataSources dataSource in _dataSources)
                dataSources += dataSource.Name + ",";
            if (dataSources.Length > 0)
                dataSources = dataSources.Remove(dataSources.Length - 1, 1);

            String regions = String.Empty;
            String subregions = String.Empty;
            String countries = String.Empty;
            foreach (Model.Region region in _regions)
            {
                regions += region.Name + ",";
                if (region.SubRegions != null)
                {
                    foreach (Model.Region subregion in region.SubRegions)
                        subregions += subregion.Name + ",";
                }
                if (region.Countries != null)
                {
                    foreach (Model.Country country in region.Countries)
                        countries += country.Name + ",";
                }
            }
            if (regions.Length > 0)
                regions = regions.Remove(regions.Length - 1, 1);

            if (subregions.Length > 0)
                subregions = subregions.Remove(subregions.Length - 1, 1);

            if (countries.Length > 0)
                countries = countries.Remove(countries.Length - 1, 1);

            IDSearchParameter param = new IDSearchParameter();
            param.Ids = modes;
            param.DeviceTypes = _device.Name;

            if (_subdevice != null)
                param.SubDeviceTypes = _subdevice.Name;

            param.Regions = regions;
            param.SubRegions = subregions;
            param.Countries = countries;
            param.DataSources = dataSources;
            param.IDModeFilter = 1;
            param.RegionType = 0;
            ErrorLogging.WriteToLogFile("Function Name: LoadComponents", false);
            DateTime spStart = DateTime.Now;
            DeviceTypeCollection resultList = _DBAccess.GetComponentList(param);
            DateTime spEnd = DateTime.Now;
            totalExecTime = spEnd - spStart;

            ErrorLogging.WriteToLogFile("UEI2_Sp_ModeDeviceComponents execution time", false);
            ErrorLogging.WriteToLogFile("Start Time: " + spStart.Hour.ToString() + ":" + spStart.Minute.ToString() + ":" + spStart.Second.ToString() + ":" + spStart.Millisecond.ToString(), false);
            ErrorLogging.WriteToLogFile("End Time: " + spEnd.Hour.ToString() + ":" + spEnd.Minute.ToString() + ":" + spEnd.Second.ToString() + ":" + spEnd.Millisecond.ToString(), false);
            ErrorLogging.WriteToLogFile("Total sp Time: " + totalExecTime.Hours.ToString() + ":" + totalExecTime.Minutes.ToString() + ":" + totalExecTime.Seconds.ToString() + ":" + totalExecTime.Milliseconds.ToString(), false);

            IList<Model.Component> componentList = new List<Model.Component>();
            //if(resultList.Count > 1)
            //    deviceList.Add(new Model.DeviceType("0", "All"));

            foreach (DeviceType deviceType in resultList)
            {
                if (deviceType.Component != String.Empty)
                    componentList.Add(new Model.Component(deviceType.ComponentID.ToString(), deviceType.Component.ToString()));
            }
            return componentList;
        }
        #endregion

        #region Load Identical ID Elimination List
        public IList<Model.AliasId> GetIdenticalIDList(String ProjectName)
        {            
            IList<Hashtable> idEliminationList = PickAccessLayer.PickRetrieveFunctions.ViewEliminatedIdList(ProjectName);
            return (ProcessIdEliminationList(idEliminationList));
        }
        private IList<Model.AliasId> ProcessIdEliminationList(IList<Hashtable> idEliminationList)
        {
            IList<Model.AliasId> IdList = new List<Model.AliasId>();
            String prevId = String.Empty;
            String eliminatedId = String.Empty;
            foreach (Hashtable ht in idEliminationList)
            {
                if(ht["Alias_ID"] != null)
                {
                    if(ht["ReplacementID"].ToString() == prevId)
                    {
                        if(!eliminatedId.Contains(ht["Alias_ID"].ToString()))
                            eliminatedId += ht["Alias_ID"].ToString() + ",";
                    }
                    else if(prevId == String.Empty)
                    {
                        prevId = ht["ReplacementID"].ToString();
                        if(!eliminatedId.Contains(ht["Alias_ID"].ToString()))
                            eliminatedId = ht["Alias_ID"].ToString() + ",";
                    }
                    else
                    {
                        if(eliminatedId.Length > 0)
                            eliminatedId = eliminatedId.Substring(0, eliminatedId.Length - 1);
                        if(prevId != String.Empty && eliminatedId != String.Empty)
                        {
                            Model.AliasId idExist = isEliminatedIdExists(IdList, prevId);
                            if(idExist == null)
                                IdList.Add(new Model.AliasId(prevId, eliminatedId));
                            else
                                idExist.AliasID = idExist.AliasID + "," + eliminatedId;
                        }
                        eliminatedId = String.Empty;
                        prevId = ht["ReplacementID"].ToString();
                        if(!eliminatedId.Contains(ht["Alias_ID"].ToString()))
                            eliminatedId = ht["Alias_ID"].ToString() + ",";
                    }
                }
            }
            if (eliminatedId.Length > 0)
                eliminatedId = eliminatedId.Substring(0, eliminatedId.Length - 1);
            if (prevId != String.Empty && eliminatedId != String.Empty)
            {
                Model.AliasId idExist = isEliminatedIdExists(IdList, prevId);
                if (idExist == null)
                    IdList.Add(new Model.AliasId(prevId, eliminatedId));
                else
                    idExist.AliasID = idExist.AliasID + "," + eliminatedId;
            }
            return IdList;
        }
        private Model.AliasId isEliminatedIdExists(IList<Model.AliasId> idList, String prevId)
        {
            Model.AliasId idExists = null;
            foreach (Model.AliasId item in idList)
            {
                if (item.ID == prevId)
                    return item;
            }
            return idExists;
        }
        #endregion

        #region Load Data Source Location
        public IList<Model.DataSources> GetAllDataSources()
        {
            StringCollection _allDataSourceLocationList = _DBAccess.GetAllDataSources();
            IList<Model.DataSources> _dataSourceLocationList = new List<Model.DataSources>();
            foreach (String _dataSource in _allDataSourceLocationList)
            {
                _dataSourceLocationList.Add(new Model.DataSources(_dataSource));
            }
            return _dataSourceLocationList;
        }
        #endregion

        #region setupcodeformatting
        /// <summary>
        /// Formats the setupcode in base10 format to the format specified in IdSetupCodeInfo object.
        /// </summary>
        /// <remarks>Created by binil on 25/10/2010.</remarks>
        /// <param name="setupCodeInBase10"></param>
        /// <param name="setupCodeInfo"></param>
        /// <returns></returns>
        private static string FormatSetupCode(string setupCodeInBase10, Model.IdSetupCodeInfo setupCodeInfo)
        {
            try
            {
                int setupCode = int.Parse(setupCodeInBase10);
                if (!String.IsNullOrEmpty(setupCodeInBase10) && setupCodeInfo != null && !String.IsNullOrEmpty(setupCodeInfo.SetupCodeFormat) && setupCodeInfo.SetupCodeFormat != "Base 10")
                {
                    //check the pattern.
                    string[] format = setupCodeInfo.SetupCodeFormat.Split('-');
                    if (format.Length == 2)
                    {
                        System.Text.RegularExpressions.Match match = System.Text.RegularExpressions.Regex.Match(format[0], @"\d");
                        int basedigit = int.Parse(match.Value);                        
                        match = System.Text.RegularExpressions.Regex.Match(format[1], @"\d");
                        int setupdigit = int.Parse(match.Value);                        
                        string formattedText = ConvertBase10ToBase(setupCode, basedigit, setupdigit,setupCodeInfo.StartingDigitofSetupCode);
                        if (formattedText != "error")
                            return formattedText;
                    }
                }
            }
            catch{}
            return string.Empty;
        }

        private static string ConvertBase10ToBase(int Decimal, int basedigit, int setupDigit)
        {
            string convertedData = string.Empty;
            System.Collections.Stack data = new System.Collections.Stack();
            int remainder;
            int num = Decimal;
            try
            {
                while (num >= basedigit)
                {
                    num = Math.DivRem(num, basedigit, out remainder);
                    data.Push((int)remainder);
                }
                data.Push(num);
                if (data.Count > setupDigit)
                {
                    convertedData = "error";
                }
                else
                {
                    foreach (int i in data)
                    {
                        convertedData += (i + 1).ToString();
                    }
                }
                convertedData = Tweakthelength(convertedData, setupDigit, "1");
                return convertedData;
            }
            catch
            {
                return "error";
            }

        }

        private static string ConvertBase10ToBase(int Decimal, int basedigit, int setupDigit, int startingDigitOfBase)
        {
            string convertedData = string.Empty;
            System.Collections.Stack data = new System.Collections.Stack();
            int remainder;
            int num = Decimal;
            try
            {
                while (num >= basedigit)
                {
                    num = Math.DivRem(num, basedigit, out remainder);
                    data.Push((int)remainder);
                }
                data.Push(num);
                if (data.Count > setupDigit)
                {
                    convertedData = "error";
                }
                else
                {
                    foreach (int i in data)
                    {
                        convertedData += (i + startingDigitOfBase).ToString();
                    }
                }
                // convertedData = Tweakthelength(convertedData, setupDigit, "1");
                convertedData = convertedData.PadLeft(setupDigit, Convert.ToChar(startingDigitOfBase.ToString()));

                return convertedData;
            }
            catch
            {
                return "error";
            }

        }

        private static string Tweakthelength(string converteddata, int setupdigit, string appendValue)
        {
            int length = converteddata.Length;
            for (int len = (setupdigit - length); len > 0; len--)
            {
                converteddata = appendValue + converteddata;
            }
            return converteddata;
        }
        #endregion            

        #region Brand Table Rom Report
        public IList<Model.ModeBrandItem> GetLookupBrandNumbers(IList<Model.ModeBrandItem> m_BrandList)
        {
            BrandCollection m_AllBrands = DBFunctions.GetAllBrandList();
            IList<String> m_AllBrandNames = new List<String>();
            foreach(Brand item in m_AllBrands)
                m_AllBrandNames.Add(item.Name);

            foreach(Model.ModeBrandItem list in m_BrandList)
            {
                for(Int32 bli = 0; bli < list.BrandInfo.Count; bli++)
                {
                    if(m_AllBrandNames.Contains(list.BrandInfo[bli].Brand))
                    {
                        Int32 abi = m_AllBrandNames.IndexOf(list.BrandInfo[bli].Brand);
                        list.BrandInfo[bli].BrandNum = m_AllBrands[abi].Code;
                    }
                    //else
                    // log error
                }
            }
            return m_BrandList;
        }
        #endregion

        #region Duplicate Intron Report
        public IList<String> GetIdListforDuplcaiteIntronReport()
        {
            DAOFactory.ResetDBConnection(DBConnectionString.UEIPUBLIC);

            LogText("--Function Name:GetIdListforDuplcaiteIntronReport--");
            LogText("Query Execution-DBAccess.NonBlockingGetAllIDList()");

            DateTime start = DateTime.Now;
            IList<String> m_AllIDList = _DBAccess.NonBlockingGetAllIDList();

            DateTime stop = DateTime.Now;
            TimeSpan queryExSpan = stop - start;

            LogTime("Start Time : ", start);
            LogTime("End Time : ", stop);
            LogTimeSpan("Query Execution Time : ", queryExSpan);
            if(m_AllIDList != null)
            {
                return m_AllIDList;
            }
            return null;
        }
        public Model.LabelIntronSearchReport GetDuplicateIntronReport(IList<String> m_IdList)
        {
            Model.LabelIntronSearchReport m_DuplicateIntronReport = null;
            if(m_IdList != null)
            {
                DateTime start = DateTime.Now;
                m_DuplicateIntronReport = GetDuplicateIntrons(m_IdList);
                DateTime stop = DateTime.Now;
                TimeSpan queryExSpan = stop - start;

                LogTime("Start Time : ", start);
                LogTime("End Time : ", stop);
                LogTimeSpan("Application Processing Time : ", queryExSpan);
            }
            return m_DuplicateIntronReport;
        }
        private Model.LabelIntronSearchReport GetDuplicateIntrons(IList<String> m_IdList)
        {
            Model.LabelIntronSearchReport report = new Model.LabelIntronSearchReport(true);
            foreach(String m_Id in m_IdList)
            {
                Hashtable paramList = new Hashtable();
                paramList.Add("ID", m_Id);
                FunctionCollection functions = DAOFactory.Function().SelectIDFunc(paramList);

                Boolean need_output = false;
                Boolean print_header = true;
                Boolean start = false;
                Int32 priority = 0;
                if(functions != null)
                {
                    for(Int32 it = 0; it < functions.Count; it++)
                    {
                        if(it < functions.Count - 1)
                        {
                            if(functions[it].IntronPriority == null || functions[it + 1].IntronPriority == null)
                                continue;

                            if(functions[it].Intron.Equals(functions[it + 1].Intron) &&
                                (functions[it].IntronPriority.Equals(functions[it + 1].IntronPriority)) &&
                                (!functions[it].Data.Equals(functions[it + 1].Data)))
                                need_output = true;
                            if(!start && functions[it].Intron.Equals(functions[it + 1].Intron))
                            {
                                start = true;
                            }
                        }
                        if(start)
                        {
                            if(need_output)
                            {
                                if(!String.IsNullOrEmpty(functions[it].IntronPriority))
                                {
                                    Model.LabelIntronSearchRecord record = new Model.LabelIntronSearchRecord();
                                    record.ID = m_Id;
                                    record.Intron = functions[it].Intron;
                                    record.Label = functions[it].Label;
                                    record.Priority = functions[it].IntronPriority;
                                    record.Synth = functions[it].Synth;
                                    record.Data = functions[it].Data;
                                    report.AddRecord(record);
                                }
                                else
                                {
                                    Model.LabelIntronSearchRecord record = new Model.LabelIntronSearchRecord();
                                    record.ID = m_Id;
                                    record.Intron = functions[it].Intron;
                                    record.Label = functions[it].Label;
                                    record.Priority = "None";
                                    record.Synth = functions[it].Synth;
                                    record.Data = functions[it].Data;
                                    report.AddRecord(record);
                                }
                            }
                        }
                        if(it < functions.Count - 1)
                        {
                            if(!functions[it].Intron.Equals(functions[it + 1].Intron) || !functions[it].IntronPriority.Equals(functions[it + 1].IntronPriority) ||
                                functions[it].Data.Equals(functions[it + 1].Data))
                            {
                                start = false;
                                need_output = false;
                            }
                        }
                    }
                }
            }
            return report;
        }        
        #endregion

        #region Synthesizer Table
        public IList<String> GetIdListforSynthesizerTableReport(Boolean m_UseDataBase)
        {
            if(m_UseDataBase)
                DAOFactory.ResetDBConnection(DBConnectionString.UEIPUBLIC);
            else
                DAOFactory.ResetDBConnection(DBConnectionString.UEITEMP);

            LogText("--Function Name:GetSynthesizerTableReport--");
            LogText("Query Execution-DBAccess.NonBlockingGetAllIDList()");

            DateTime start = DateTime.Now;
            IList<String> m_AllIDList = _DBAccess.NonBlockingGetAllIDList();

            DateTime stop = DateTime.Now;
            TimeSpan queryExSpan = stop - start;

            LogTime("Start Time : ", start);
            LogTime("End Time : ", stop);
            LogTimeSpan("Query Execution Time : ", queryExSpan);
            if(m_AllIDList != null)
            {
                return m_AllIDList;
            }
            return null;
        }
        public Model.SynthesizerTableModel GetSynthesizerTableReport(IList<String> m_IdList, Boolean m_IncludeIntron, List<String> m_SelectedModes)
        {
            Model.SynthesizerTableModel m_Synthesizers = null;
            if(m_IdList != null)
            {
                DateTime start = DateTime.Now;
                m_Synthesizers = GetSynthesizers(m_IdList, m_IncludeIntron, m_SelectedModes);
                DateTime stop = DateTime.Now;
                TimeSpan queryExSpan = stop - start;

                LogTime("Start Time : ", start);
                LogTime("End Time : ", stop);
                LogTimeSpan("Application Processing Time : ", queryExSpan);
            }
            return m_Synthesizers;
        }
        private Model.SynthesizerTableModel GetSynthesizers(IList<String> m_AllIDList, Boolean m_IncludeIntron, List<String> m_SelectedModes)
        {
            Model.SynthesizerTableModel m_Synthesizers = new Model.SynthesizerTableModel();                        
            CommonFunctions commonFunc = new CommonFunctions();            
            foreach(String m_Id in m_AllIDList)
            {
                if(m_SelectedModes.Contains(m_Id.Substring(0, 1).ToUpper()))
                {
                    Boolean isInversedData = false;
                    String syn = String.Empty;
                    String zero_str_16 = "0000000000000000";
                    Byte byte_data;
                    Int16 int16_data;
                    String data_str = String.Empty;

                    // Check whether this ID includes Inverse Data or not
                    IDHeader IDHeaderData = DAOFactory.IDHeader().Select(m_Id);
                    if(IDHeaderData.IsInversedData == "Y")
                        isInversedData = true;
                    if(m_IncludeIntron)
                    {
                        FunctionCollection functions = DAOFactory.Function().SelectSynTableFunc(m_Id);
                        if(functions != null)
                        {
                            foreach(Function functionData in functions)
                            {
                                if(functionData.Data.Length != 8 && functionData.Data.Length != 16)
                                    continue;
                                else if(functionData.Data.Length == 8)
                                {
                                    if(isInversedData)
                                        byte_data = (Byte)~(Convert.ToByte(functionData.Data, 2));
                                    else
                                        byte_data = Convert.ToByte(functionData.Data, 2);
                                    data_str = Convert.ToString(byte_data, 2);

                                    if(data_str.Length < 8)
                                        data_str = String.Concat(zero_str_16.Substring(0, 8 - data_str.Length), data_str);
                                }
                                else
                                {
                                    if(isInversedData)
                                        int16_data = (Int16)~(Convert.ToInt16(functionData.Data, 2));
                                    else
                                        int16_data = Convert.ToInt16(functionData.Data, 2);
                                    data_str = Convert.ToString(int16_data, 2);
                                    if(data_str.Length < 16)
                                        data_str = String.Concat(zero_str_16.Substring(0, 16 - data_str.Length), data_str);
                                }

                                syn = commonFunc.GetSynthesizer(data_str);
                                m_Synthesizers.AddSynthesizer(new Model.SynthesizerTable(functionData.ID, functionData.Label, syn, functionData.Intron));
                            }
                        }
                    }
                    else
                    {
                        FunctionCollection functions = DAOFactory.Function().SelectSynTableFuncNoIntron(m_Id);
                        if(functions != null)
                        {
                            foreach(Function functionData in functions)
                            {
                                if(functionData.Data.Length != 8 && functionData.Data.Length != 16)
                                    continue;

                                else if(functionData.Data.Length == 8)
                                {
                                    if(isInversedData)
                                        byte_data = (Byte)~(Convert.ToByte(functionData.Data, 2));
                                    else
                                        byte_data = Convert.ToByte(functionData.Data, 2);
                                    data_str = Convert.ToString(byte_data, 2);
                                    if(data_str.Length < 8)
                                        data_str = String.Concat(zero_str_16.Substring(0, 8 - data_str.Length), data_str);
                                }
                                else
                                {
                                    if(isInversedData)
                                        int16_data = (Int16)~(Convert.ToInt16(functionData.Data, 2));
                                    else
                                        int16_data = Convert.ToInt16(functionData.Data, 2);
                                    data_str = Convert.ToString(int16_data, 2);
                                    if(data_str.Length < 16)
                                        data_str = String.Concat(zero_str_16.Substring(0, 16 - data_str.Length), data_str);
                                }

                                syn = commonFunc.GetSynthesizer(data_str);
                                syn = commonFunc.GetSynthesizer(data_str);

                                m_Synthesizers.AddSynthesizer(new Model.SynthesizerTable(functionData.ID, functionData.Label, syn, ""));
                            }
                        }
                    }                    
                }
            }
            return m_Synthesizers;
        }
        public static DataSet GetSynthesizerTableReportDataSet(Model.SynthesizerTableModel m_Synthesizers, Boolean m_IncludeIntron, Model.IDModeRecordModel allModeNames)
        {
            DataSet dsSynthesizerTable = null;
            DataRow dr = null;
            if(m_Synthesizers != null)
            {
                dsSynthesizerTable = new DataSet();
                dsSynthesizerTable.Tables.Add("SynthesizerTable");
                if(m_IncludeIntron)
                {
                    dsSynthesizerTable.Tables["SynthesizerTable"].Columns.Add("ID");
                    dsSynthesizerTable.Tables["SynthesizerTable"].Columns.Add("Label");
                    dsSynthesizerTable.Tables["SynthesizerTable"].Columns.Add("Syn");
                    dsSynthesizerTable.Tables["SynthesizerTable"].Columns.Add("Intron");
                }
                else
                {
                    dsSynthesizerTable.Tables["SynthesizerTable"].Columns.Add("ID");
                    dsSynthesizerTable.Tables["SynthesizerTable"].Columns.Add("Label");
                    dsSynthesizerTable.Tables["SynthesizerTable"].Columns.Add("Syn");
                }
                List<String> m_ModeList = m_Synthesizers.GetDistinctModes();
                foreach(String m_Mode in m_ModeList)
                {
                    List<Model.SynthesizerTable> m_SynthesizerList = m_Synthesizers.GetSynthesizersByMode(m_Mode);
                    if(m_SynthesizerList != null)
                    {                                               
                        dr = dsSynthesizerTable.Tables["SynthesizerTable"].NewRow();
                        String modeName = allModeNames.getModeName(m_Mode);
                        //dr[0] = mode;
                        dr[0] = modeName + " (" + m_Mode + ")";
                        dsSynthesizerTable.Tables["SynthesizerTable"].Rows.Add(dr);
                        dr = null;

                        foreach(Model.SynthesizerTable m_Item in m_SynthesizerList)
                        {
                            dr = dsSynthesizerTable.Tables["SynthesizerTable"].NewRow();
                            if(m_IncludeIntron)
                            {
                                dr[0] = m_Item.ID;
                                dr[1] = m_Item.Label;
                                dr[2] = m_Item.Syn;
                                dr[3] = m_Item.Intron;
                            }
                            else
                            {
                                dr[0] = m_Item.ID;
                                dr[1] = m_Item.Label;
                                dr[2] = m_Item.Syn;
                            }
                            dsSynthesizerTable.Tables["SynthesizerTable"].Rows.Add(dr);
                        }
                        dsSynthesizerTable.Tables["SynthesizerTable"].AcceptChanges();
                    }
                }               
            }
            return dsSynthesizerTable;
        }
        public static DataSet GetSynthesizerTableReportExcelPDFDataSet(Model.SynthesizerTableModel m_Synthesizers, Boolean m_IncludeIntron, Model.IDModeRecordModel allModeNames)
        {
            DataSet dsSynthesizerTable = null;
            DataRow dr = null;
            if(m_Synthesizers != null)
            {
                dsSynthesizerTable = new DataSet("SynthesizerTable");
                
                List<String> m_ModeList = m_Synthesizers.GetDistinctModes();
                foreach(String m_Mode in m_ModeList)
                {
                    List<Model.SynthesizerTable> m_SynthesizerList = m_Synthesizers.GetSynthesizersByMode(m_Mode);
                    if(m_SynthesizerList != null)
                    {
                        dsSynthesizerTable.Tables.Add(m_Mode);
                        if(m_IncludeIntron)
                        {
                            dsSynthesizerTable.Tables[m_Mode].Columns.Add("ID");
                            dsSynthesizerTable.Tables[m_Mode].Columns.Add("Label");
                            dsSynthesizerTable.Tables[m_Mode].Columns.Add("Syn");
                            dsSynthesizerTable.Tables[m_Mode].Columns.Add("Intron");
                        }
                        else
                        {
                            dsSynthesizerTable.Tables[m_Mode].Columns.Add("ID");
                            dsSynthesizerTable.Tables[m_Mode].Columns.Add("Label");
                            dsSynthesizerTable.Tables[m_Mode].Columns.Add("Syn");
                        }

                        dr = dsSynthesizerTable.Tables[m_Mode].NewRow();
                        String modeName = allModeNames.getModeName(m_Mode);
                        //dr[0] = mode;
                        dr[0] = modeName + " (" + m_Mode + ")";
                        dsSynthesizerTable.Tables[m_Mode].Rows.Add(dr);
                        dr = null;

                        foreach(Model.SynthesizerTable m_Item in m_SynthesizerList)
                        {
                            dr = dsSynthesizerTable.Tables[m_Mode].NewRow();
                            if(m_IncludeIntron)
                            {
                                dr[0] = m_Item.ID;
                                dr[1] = m_Item.Label;
                                dr[2] = m_Item.Syn;
                                dr[3] = m_Item.Intron;
                            }
                            else
                            {
                                dr[0] = m_Item.ID;
                                dr[1] = m_Item.Label;
                                dr[2] = m_Item.Syn;
                            }
                            dsSynthesizerTable.Tables[m_Mode].Rows.Add(dr);
                        }
                        dsSynthesizerTable.AcceptChanges();
                    }
                }
            }
            return dsSynthesizerTable;
        }
        #endregion

        #region QA Status Report
        public Model.QAStatusReportModel GetQAStatusReport(Model.QAStatus m_QAStatus)
        {
            LogText("--Function Name:GetQAStatusReport--");
            LogText("Query Execution-DBAccess.NonBlockingGetAllIDList()");

            DateTime start = DateTime.Now;
            //Get All Non Blocking ID List
            DAOFactory.ResetDBConnection(DBConnectionString.UEITEMP);
            StringCollection idList = _DBAccess.NonBlockingGetAllIDList();

            DateTime stop = DateTime.Now;
            TimeSpan queryExSpan = stop - start;

            LogTime("Start Time : ", start);
            LogTime("End Time : ", stop);
            LogTimeSpan("Query execution time : ", queryExSpan);
            
            if (idList != null)
            {
                return(GetQAStatusReportModel(idList,m_QAStatus));
            }            
            return null;
        }
        public static DataSet GetQAStatusExcelReportDataSet(Model.QAStatusReportModel qaStatusReportModel)
        {
            DataSet dsQAStatus = null;
            if (qaStatusReportModel != null)
            {
                dsQAStatus = new DataSet();

                DataRow dr = null;

                List<String> modeList = qaStatusReportModel.GetDistinctModes();
                foreach (String mode in modeList)
                {
                    //String modeName = allModeNames.getModeName(mode);
                    Model.QAStatusReportModel records = qaStatusReportModel.GetQAStatusBasedonMode(mode);
                    if (records.Count > 0)
                    {
                        dsQAStatus.Tables.Add(mode);
                        dsQAStatus.Tables[mode].Columns.Add("ID");
                    }
                    foreach (Model.QAStatusRecord record in records)
                    {
                        dr = dsQAStatus.Tables[mode].NewRow();
                        dr["ID"] = record.ID;
                        dsQAStatus.Tables[mode].Rows.Add(dr);
                        dr = null;
                        if (record.TNInfo.Count > 0)
                        {
                            if (!dsQAStatus.Tables[mode].Columns.Contains("TN"))
                            {
                                dsQAStatus.Tables[mode].Columns.Add("TN");
                            }
                            if (!dsQAStatus.Tables[mode].Columns.Contains("Status"))
                            {
                                dsQAStatus.Tables[mode].Columns.Add("Status");
                            }
                            dr = dsQAStatus.Tables[mode].NewRow();
                            foreach (Model.TNInfo tnInfo in record.TNInfo)
                            {
                                dr["TN"] = tnInfo.TN;
                                dr["Status"] = tnInfo.Status;
                            }
                            dsQAStatus.Tables[mode].Rows.Add(dr);
                            dr = null;
                        }
                    }
                }
            }
            return dsQAStatus;
        }
        public static DataSet GetQAStatusTextReportDataSet(Model.QAStatusReportModel qaStatusReportModel, Model.IDModeRecordModel allModeNames)
        {
            DataSet dsQAStatus = null;
            if (qaStatusReportModel != null)
            {
                dsQAStatus = new DataSet();
                
                DataRow dr = null;

                List<String> modeList = qaStatusReportModel.GetDistinctModes();
                foreach (String mode in modeList)
                {
                    String modeName = allModeNames.getModeName(mode);
                    Model.QAStatusReportModel records = qaStatusReportModel.GetQAStatusBasedonMode(mode);
                    if (records.Count > 0)
                    {
                        dsQAStatus.Tables.Add(mode);
                        dsQAStatus.Tables[mode].Columns.Add("ID");                        
                    }
                    foreach (Model.QAStatusRecord record in records)
                    {
                        dr = dsQAStatus.Tables[mode].NewRow();
                        dr["ID"] = record.ID;
                        dsQAStatus.Tables[mode].Rows.Add(dr);
                        dr = null;
                        if (record.TNInfo.Count > 0)
                        {
                            if (!dsQAStatus.Tables[mode].Columns.Contains("TN"))
                            {
                                dsQAStatus.Tables[mode].Columns.Add("TN");
                            }
                            if (!dsQAStatus.Tables[mode].Columns.Contains("Status"))
                            {
                                dsQAStatus.Tables[mode].Columns.Add("Status");
                            }
                            dr = dsQAStatus.Tables[mode].NewRow();
                            foreach (Model.TNInfo tnInfo in record.TNInfo)
                            {
                                dr["TN"] = tnInfo.TN;
                                dr["Status"] = tnInfo.Status;
                            }
                            dsQAStatus.Tables[mode].Rows.Add(dr);
                            dr = null;
                        }
                    }
                }
            }
            return dsQAStatus;
        }
        public static DataSet GetQAStatusReportDataSet(Model.QAStatusReportModel qaStatusReportModel, Model.IDModeRecordModel allModeNames)
        {
            DataSet dsQAStatus = null;
            if (qaStatusReportModel != null)
            {
                dsQAStatus = new DataSet();
                dsQAStatus.Tables.Add("QAStatus");
                dsQAStatus.Tables["QAStatus"].Columns.Add("ID");
                DataRow dr = null;

                List<String> modeList = qaStatusReportModel.GetDistinctModes();
                foreach (String mode in modeList)
                {
                    String modeName = allModeNames.getModeName(mode);
                    Model.QAStatusReportModel records = qaStatusReportModel.GetQAStatusBasedonMode(mode);
                    if (records.Count > 0)
                    {
                        dr = dsQAStatus.Tables["QAStatus"].NewRow();
                        dr["ID"] = modeName + " (" + mode + ")";;
                        dsQAStatus.Tables["QAStatus"].Rows.Add(dr);
                        dr = null;
                    }
                    foreach (Model.QAStatusRecord record in records)
                    {
                        dr = dsQAStatus.Tables["QAStatus"].NewRow();
                        dr["ID"] = record.ID;
                        dsQAStatus.Tables["QAStatus"].Rows.Add(dr);
                        dr = null;
                        if (record.TNInfo.Count > 0)
                        {                            
                            if (!dsQAStatus.Tables["QAStatus"].Columns.Contains("TN"))
                            {
                                dsQAStatus.Tables["QAStatus"].Columns.Add("TN");
                            }
                            if (!dsQAStatus.Tables["QAStatus"].Columns.Contains("Status"))
                            {
                                dsQAStatus.Tables["QAStatus"].Columns.Add("Status");
                            }
                            dr = dsQAStatus.Tables["QAStatus"].NewRow();
                            foreach (Model.TNInfo tnInfo in record.TNInfo)
                            {
                                dr["TN"] = tnInfo.TN;
                                dr["Status"] = tnInfo.Status;
                            }
                            dsQAStatus.Tables["QAStatus"].Rows.Add(dr);
                            dr = null;
                        }
                    }
                }                
            }
            return dsQAStatus;
        }
        private Model.QAStatusReportModel GetQAStatusReportModel(StringCollection idList, Model.QAStatus m_QAStatus)
        {
            LogText("--Function Name:GetQAStatusReportModel--");
            LogText("Note:Executing procedure[UEI2_BO_Sp_GetTNDatawithStatus] and query for TNList and creating model objects");

            DateTime start = DateTime.Now;

            String idStatusWatch = String.Empty;
            idStatusWatch = (m_QAStatus.FlagQ) ? StatusFlag.PASSED_QA : "";
            idStatusWatch += (m_QAStatus.FlagM) ? StatusFlag.MODIFIED : "";
            idStatusWatch += (m_QAStatus.FlagE) ? StatusFlag.EXEC_HOLD : "";
            idStatusWatch += (m_QAStatus.FlagP) ? StatusFlag.PROJECT_HOLD : "";

            Model.QAStatusReportModel report = null;            
            foreach (String id in idList)
            {
                try
                {
                    IDHeader headerData = _DBAccess.GetHeaderData(id);
                    if (idStatusWatch.Contains(headerData.Status))
                    {
                        Model.QAStatusRecord record = new Model.QAStatusRecord();
                        if (m_QAStatus.Verbose)
                        {
                            //ID Current Status is
                            //Add ID     
                            record.Mode = id.Substring(0, 1).ToUpper();
                            record.ID = id;
                            record.Status = StatusName(headerData.Status);
                            if (report == null)
                                report = new Model.QAStatusReportModel();
                            report.Add(record);
                            record = null;
                            if (m_QAStatus.TNStatus)
                            {
                                try
                                {
                                    IntegerCollection tnList = _DBAccess.GetTNList(id);
                                    if (tnList != null)
                                        foreach (Int32 tn in tnList)
                                        {
                                            TNHeader tnHeader = _DBAccess.GetTnStatus(tn);
                                            if (tnHeader.Status == "Q")
                                            {
                                                record = report.GetQAStatusRecord(id);
                                                //TN Has Not Passed QA.
                                                //Add ID
                                                Model.TNInfo tnInfo = new Model.TNInfo();
                                                tnInfo.TN = tn.ToString();
                                                tnInfo.Status = "Not Passed QA";
                                                record.TNInfo.Add(tnInfo);
                                                tnInfo = null;
                                            }
                                        }
                                }
                                catch { }
                            }
                        }
                        else
                        {
                            //Add ID
                            record.Mode = id.Substring(0, 1).ToUpper();
                            record.ID = id;
                            record.Status = StatusName(headerData.Status);
                            if (report == null)
                                report = new Model.QAStatusReportModel();
                            report.Add(record);
                            record = null;
                        }
                    }
                }
                catch { }
            }
            DateTime stop = DateTime.Now;
            TimeSpan queryExSpan = stop - start;

            LogTime("Start Time : ", start);
            LogTime("End Time : ", stop);
            LogTimeSpan("Execution time : ", queryExSpan);

            DAOFactory.ResetDBConnection(DBConnectionString.UEIPUBLIC);
            return report;            
        }
        public string StatusName(String Flag)
        {
            switch (Flag)
            {
                case StatusFlag.SHADOW: return "Shadow ID";
                case StatusFlag.MODIFIED: return "Modified";
                case StatusFlag.PASSED_QA: return "Passed QA";
                case StatusFlag.EXEC_HOLD: return "Exec Hold";
                case StatusFlag.PROJECT_HOLD: return "Project Hold";
                default: return "Undefined Flag";
            }
        }
        #endregion                           
    
        #region Unique Function IR
        public Model.UniqueFuncIRCountModel GetUniqueFunctionsIR()
        {
            LogText("--Function Name: GetUniqueFunctionsIR--");
            
            IList<String> modeList = GetAllModes();
          
            if (modeList != null && modeList.Count > 0)
            {
                LogText("Query and Bl Execution -SelectUniqueFuncCount && SelectUniqueIRCount");
                DateTime start = DateTime.Now;

                Model.UniqueFuncIRCountModel records = new Model.UniqueFuncIRCountModel();
                foreach (String item in modeList)
                {
                    Model.UniqueFuncIRCountRecord record = new Model.UniqueFuncIRCountRecord();
                    record.Mode = item.ToUpper();
                    record.TotalFunctions = _DBAccess.SelectUniqueFuncCount(item);
                    record.TotalIRCodes = _DBAccess.SelectUniqueIRCount(item);
                    records.Add(record);
                    record = null;
                }
                DateTime stop = DateTime.Now;
                TimeSpan queryExSpan = stop - start;

                LogTime("Start Time : ", start);
                LogTime("End Time : ", stop);
                LogTimeSpan("Query and BL execution time : ", queryExSpan);
                
                return records;
            }
            return null;
        }
        public static DataSet GetUniqueFunctionsIRDataSet(Model.UniqueFuncIRCountModel uniqueFuncIRCountModel)
        {
            DataSet dsRecords = null;
            if (uniqueFuncIRCountModel != null)
            {
                Int32 funcCount = 0, ircodeCount = 0;
                dsRecords = new DataSet();
                dsRecords.Tables.Add("UniqueFuncIRCountReport");
                dsRecords.Tables["UniqueFuncIRCountReport"].Columns.Add("Mode");
                dsRecords.Tables["UniqueFuncIRCountReport"].Columns.Add("Total Functions");
                dsRecords.Tables["UniqueFuncIRCountReport"].Columns.Add("Total IR codes");
                DataRow dr = null;
                foreach (Model.UniqueFuncIRCountRecord  item in uniqueFuncIRCountModel)
                {
                    dr = dsRecords.Tables["UniqueFuncIRCountReport"].NewRow();
                    dr["Mode"] = item.Mode;
                    dr["Total Functions"] = item.TotalFunctions;
                    dr["Total IR codes"] = item.TotalIRCodes;
                    dsRecords.Tables["UniqueFuncIRCountReport"].Rows.Add(dr);
                    funcCount += item.TotalFunctions;
                    ircodeCount += item.TotalIRCodes;
                    dr = null;
                }
                dr = dsRecords.Tables["UniqueFuncIRCountReport"].NewRow();
                dr["Mode"] = "Total";
                dr["Total Functions"] = funcCount;
                dr["Total IR codes"] = ircodeCount;
                dsRecords.Tables["UniqueFuncIRCountReport"].Rows.Add(dr);                
                dr = null;
            }
            return dsRecords;
        }
        #endregion

        #region Disconnected TN  Report
        public IList<Model.TNInfo> GetDisconnectedTNReport(Int32 firstRange,Int32 lastRange)
        {
            List<Model.TNInfo> tnList = null;
            ErrorLogging.WriteToLogFile("--Function Name: GetDisconnectedTNReport --", false);
            ErrorLogging.WriteToLogFile("UEI2_BO_Sp_GetDisconnectedTNs  execution time", false);
            DateTime spStart = DateTime.Now;
            ErrorLogging.WriteToLogFile("Start Time: " + spStart.Hour.ToString() + ":" + spStart.Minute.ToString() + ":" + spStart.Second.ToString() + ":" + spStart.Millisecond.ToString(), false);
            IList list = _DBAccess.FindDisconnectedTNs();
            DateTime spEnd = DateTime.Now;
            ErrorLogging.WriteToLogFile("End Time: " + spEnd.Hour.ToString() + ":" + spEnd.Minute.ToString() + ":" + spEnd.Second.ToString() + ":" + spEnd.Millisecond.ToString(), false);
            totalExecTime = spEnd - spStart;
            ErrorLogging.WriteToLogFile("Total sp Time: " + totalExecTime.Hours.ToString() + ":" + totalExecTime.Minutes.ToString() + ":" + totalExecTime.Seconds.ToString() + ":" + totalExecTime.Milliseconds.ToString(), false);
            ErrorLogging.WriteToLogFile("Business logic execution time", false);
            DateTime blStart = DateTime.Now;
            ErrorLogging.WriteToLogFile("Start Time: " + blStart.Hour.ToString() + ":" + blStart.Minute.ToString() + ":" + blStart.Second.ToString() + ":" + blStart.Millisecond.ToString(), false);
            List<Int32> result = new List<Int32>();
            foreach (Int32 item in list)
            {
                result.Add(item);
            }
            result.Sort();
            if (result.Count > 0)
            {
                if (lastRange == -1)
                    foreach (Int32 item in result)
                    {
                        if (tnList == null)
                            tnList = new List<Model.TNInfo>();
                        Model.TNInfo tn = new Model.TNInfo();
                        tn.TN = "TN" + String.Format("{0:00000}", item);
                        tnList.Add(tn);
                    }            
                else
                    for (Int32 i = firstRange; i <= lastRange; i++)
                    {
                        if (result.Contains(i))
                        {
                            if (tnList == null)
                                tnList = new List<Model.TNInfo>();
                            Model.TNInfo tn = new Model.TNInfo();
                            tn.TN = "TN" + String.Format("{0:00000}", i);
                            tnList.Add(tn);
                        }
                    }
                if (tnList != null)
                {
                    Model.TNComparer comparer = new Model.TNComparer();
                    tnList.Sort(comparer);
                }
            }

            DateTime blEnd = DateTime.Now;
            ErrorLogging.WriteToLogFile("End Time: " + blEnd.Hour.ToString() + ":" + blEnd.Minute.ToString() + ":" + blEnd.Second.ToString() + ":" + blEnd.Millisecond.ToString(), false);
            TimeSpan totalBlTime = blEnd - blStart;
            ErrorLogging.WriteToLogFile("Total BL Time: " + totalBlTime.Hours.ToString() + ":" + totalBlTime.Minutes.ToString() + ":" + totalBlTime.Seconds.ToString() + ":" + totalBlTime.Milliseconds.ToString(), false);
            totalExecTime = totalExecTime + totalBlTime;
            ErrorLogging.WriteToLogFile("Total Time: " + totalExecTime.Hours.ToString() + ":" + totalExecTime.Minutes.ToString() + ":" + totalExecTime.Seconds.ToString() + ":" + totalExecTime.Milliseconds.ToString(), false);                        
            return tnList;
        }
        public static DataSet GetDisconnectedTNReportDataSet(IList<Model.TNInfo> iList)
        {
            DataSet dsTN = null;
            if (iList != null)
            {
                dsTN = new DataSet();
                dsTN.Tables.Add("DisconnectedTNReport");
                dsTN.Tables["DisconnectedTNReport"].Columns.Add("TN");
                DataRow dr = null;
                foreach (Model.TNInfo item in iList)
                {
                    dr = dsTN.Tables["DisconnectedTNReport"].NewRow();
                    dr["TN"] = item.TN;
                    dsTN.Tables["DisconnectedTNReport"].Rows.Add(dr);
                    dr = null;
                }
            }
            return dsTN;
        }
        #endregion        

        #region Get Remote Image Path
        public IList<String> GetRemoteImagePath(Int32 tnNumber)
        {
            LogText("--Function Name: GetRemoteImagePath--");
            LogText("Procedure Execution: [UEI2_Sp_GetTNRemoteImages]");
            DateTime start = DateTime.Now;
            IList<String> remoteImages = _DBAccess.GetRemoteImages(tnNumber);
            DateTime stop = DateTime.Now;
            TimeSpan queryExSpan = stop - start;
            LogTime("Start Time : ", start);
            LogTime("End Time : ", stop);            
            LogTimeSpan("Procedure Execution time : ", queryExSpan);

            return(remoteImages);
        }
        #endregion

        #region logging
        private static void LogText(string text)
        {
            ErrorLogging.WriteToLogFile(text, false);

        }
        private static void LogTime(string message,DateTime time)
        {
            ErrorLogging.WriteToLogFile(message + time.Hour.ToString() + ":" + time.Minute.ToString() + ":" + time.Second.ToString() + ":" + time.Millisecond.ToString(), false);
        }
        private static void LogTimeSpan(string message, TimeSpan span)
        {
            ErrorLogging.WriteToLogFile(message + span.Hours.ToString() + ":" + span.Minutes.ToString() + ":" + span.Seconds.ToString() + ":" + span.Milliseconds.ToString(), false);

        }
        #endregion

        #region Brand Based Search Report
        public static DataSet GetBrandBasedWithModelReport(Model.BrandBasedSearchModel reportModel, Model.BrandBasedSeach m_brandSearchoptions, Model.IDModeRecordModel allModeNames, String tableName)
        {
            DataSet dsbrandbasedsearch = null;
            if (reportModel != null && reportModel.Count > 0)
            {                
                DataTable dtbrandBasedSearch = new DataTable("BrandBasedSearch");
                if (m_brandSearchoptions.Mode == true)
                    dtbrandBasedSearch.Columns.Add("Mode", Type.GetType("System.String"));
                dtbrandBasedSearch.Columns.Add("Brand", Type.GetType("System.String"));                
                dtbrandBasedSearch.Columns.Add("Ids", Type.GetType("System.String"));
                dtbrandBasedSearch.Columns.Add("Models", Type.GetType("System.String"));
                if(m_brandSearchoptions.DataSource == true)
                    dtbrandBasedSearch.Columns.Add("DataSource", Type.GetType("System.String")); ;

                DataRow dr = null;                
                IList<String> distinctModes = null;
                //if mode selected
                if (m_brandSearchoptions.Mode == true)
                {
                     distinctModes = reportModel.GetDistinctModes();
                }                
                //if data source selected
                if (m_brandSearchoptions.DataSource == true)
                {
                    if (distinctModes == null)
                    {
                        IList<String> distinctBrands = reportModel.GetDistinctBrands();
                        foreach (String brand in distinctBrands)
                        {
                            IList<String> distinctDataSources = reportModel.GetDistinctDataSources(brand);
                            foreach (String dataSource in distinctDataSources)
                            {
                                dr = dtbrandBasedSearch.NewRow();
                                dr["Brand"] = brand;
                                // get id list in sorted order of model count
                                dr["Ids"] = reportModel.GetIdList(brand,dataSource);
                                //get model list sorted by there count
                                dr["Models"] = reportModel.GetModelList(brand,dataSource);
                                dr["DataSource"] = dataSource;
                                dtbrandBasedSearch.Rows.Add(dr);
                                dr = null;
                            }
                        }                        
                    }
                    else
                    {
                        foreach (String m_Modes in distinctModes)
                        {
                            IList<String> distinctBrands = reportModel.GetDistinctBrands(m_Modes);
                            foreach (String brand in distinctBrands)
                            {
                                IList<String> distinctDataSources = reportModel.GetDistinctDataSources(brand);
                                foreach (String dataSource in distinctDataSources)
                                {
                                    dr = dtbrandBasedSearch.NewRow();
                                    if (m_brandSearchoptions.Mode == true)
                                        dr["Mode"] = m_Modes;

                                    dr["Brand"] = brand;
                                    // get id list in sorted order of model count
                                    dr["Ids"] = reportModel.GetIdList(brand, dataSource);
                                    //get model list sorted by there count
                                    dr["Models"] = reportModel.GetModelList(brand, dataSource);
                                    dr["DataSource"] = dataSource;
                                    dtbrandBasedSearch.Rows.Add(dr);
                                    dr = null;
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (distinctModes == null)
                    {
                        // no data source no mode
                        // get distinct brand
                        IList<String> distinctBrands = reportModel.GetDistinctBrands();
                        foreach (String brand in distinctBrands)
                        {
                            dr = dtbrandBasedSearch.NewRow();
                            dr["Brand"] = brand;
                            // get id list in sorted order of model count
                            dr["Ids"] = reportModel.GetIdList(brand);
                            //get model list sorted by there count
                            dr["Models"] = reportModel.GetModelList(brand);
                            dtbrandBasedSearch.Rows.Add(dr);
                            dr = null;
                        }
                    }
                    else
                    {
                        foreach (String m_Modes in distinctModes)
                        {
                            IList<String> distinctBrands = reportModel.GetDistinctBrands(m_Modes);
                            foreach (String brand in distinctBrands)
                            {
                                dr = dtbrandBasedSearch.NewRow();
                                dr["Mode"] = m_Modes;
                                dr["Brand"] = brand;
                                // get id list in sorted order of model count
                                dr["Ids"] = reportModel.GetIdList(brand);
                                //get model list sorted by there count
                                dr["Models"] = reportModel.GetModelList(brand);
                                dtbrandBasedSearch.Rows.Add(dr);
                                dr = null;
                            }
                        }
                    }
                }
                if (dtbrandBasedSearch.Rows.Count > 0)
                {
                    dtbrandBasedSearch.AcceptChanges();
                    dsbrandbasedsearch = new DataSet("BrandBasedSearch");
                    dsbrandbasedsearch.Tables.Add(dtbrandBasedSearch);
                }
                return dsbrandbasedsearch;
            }
            return null;
        }
        public static DataSet GetBrandBasedWithoutModelReport(Model.BrandBasedSearchModel reportModel, Model.BrandBasedSeach m_brandSearchoptions, String tableName)
        {
            DataSet dsbrandbasedsearch = null;
            if (reportModel != null && reportModel.Count > 0)
            {                
                DataTable dtbrandBasedSearch = new DataTable("BrandBasedSearch");
                if (m_brandSearchoptions.Mode == true)
                    dtbrandBasedSearch.Columns.Add("Mode", Type.GetType("System.String"));

                dtbrandBasedSearch.Columns.Add("Brand", Type.GetType("System.String"));
                dtbrandBasedSearch.Columns.Add("ID", Type.GetType("System.String"));
                dtbrandBasedSearch.Columns.Add("DeviceType", Type.GetType("System.String"));
                
                if (m_brandSearchoptions.DataSource == true)
                    dtbrandBasedSearch.Columns.Add("DataSource", Type.GetType("System.String")); ;

                String strAvailablity = String.Empty;

                dtbrandBasedSearch.Columns.Add("UEI2", Type.GetType("System.String"));
                if (m_brandSearchoptions.Avail == 1 || m_brandSearchoptions.Avail == 3)
                    dtbrandBasedSearch.Columns.Add("POS", Type.GetType("System.String"));
                if (m_brandSearchoptions.Avail == 2 || m_brandSearchoptions.Avail == 3)
                    dtbrandBasedSearch.Columns.Add("OSS", Type.GetType("System.String"));
                DataRow dr = null;
                String strBrand = String.Empty, strDevice = String.Empty;
                foreach (Model.BrandBasedSearchRecord record in reportModel)
                {
                    strBrand = String.Empty;                    
                    strBrand = Convert.ToString(record.Brand);
                    foreach (Model.DeviceInfo  deviceInfo in record.DeviceTypes)
                    {
                        strDevice = String.Empty;
                        foreach (Model.IDInfo idInfo in deviceInfo.IdList)
                        {                            
                            if (m_brandSearchoptions.DataSource == true)
                            {
                                foreach (Model.ModelInfo modelInfo in idInfo.ModelList)
                                {                                    
                                    foreach (Model.DataSourceInfo dataSource in modelInfo.DataSources)
                                    {
                                        if (dtbrandBasedSearch.Select("Brand = '" + record.Brand + "' and ID = '" + idInfo.ID + "' and DeviceType = '" + deviceInfo.Name + "' and DataSource = '" + dataSource.Name + "' and UEI2 = '" + idInfo.UEI2Avail + "'").Length == 0)
                                        {
                                            dr = dtbrandBasedSearch.NewRow();
                                            dr["Brand"] = Convert.ToString(record.Brand);
                                            dr["ID"] = idInfo.ID;
                                            dr["DeviceType"] = deviceInfo.Name;
                                            if (m_brandSearchoptions.Mode == true)
                                                dr["Mode"] = record.Mode;
                                            dr["DataSource"] = dataSource.Name;
                                            dr["UEI2"] = idInfo.UEI2Avail;
                                            if (m_brandSearchoptions.Avail == 1 || m_brandSearchoptions.Avail == 3)
                                                dr["POS"] = idInfo.POSAvail;
                                            if (m_brandSearchoptions.Avail == 2 || m_brandSearchoptions.Avail == 3)
                                                dr["OSS"] = idInfo.OSSAvail;
                                            dtbrandBasedSearch.Rows.Add(dr);
                                            dr = null;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (dtbrandBasedSearch.Select("Brand = '" + record.Brand + "' and ID = '" + idInfo.ID + "' and DeviceType = '" + deviceInfo.Name + "' and UEI2 = '" + idInfo.UEI2Avail + "'").Length == 0)
                                {
                                    dr = dtbrandBasedSearch.NewRow();
                                    dr["Brand"] = Convert.ToString(record.Brand);
                                    dr["ID"] = idInfo.ID;
                                    dr["DeviceType"] = deviceInfo.Name;
                                    if (m_brandSearchoptions.Mode == true)
                                        dr["Mode"] = record.Mode;
                                    dr["UEI2"] = idInfo.UEI2Avail;
                                    if (m_brandSearchoptions.Avail == 1 || m_brandSearchoptions.Avail == 3)
                                        dr["POS"] = idInfo.POSAvail;
                                    if (m_brandSearchoptions.Avail == 2 || m_brandSearchoptions.Avail == 3)
                                        dr["OSS"] = idInfo.OSSAvail;
                                    dtbrandBasedSearch.Rows.Add(dr);
                                    dr = null;
                                }
                            }                            
                        }
                    }                    
                }
                if (dtbrandBasedSearch.Rows.Count > 0)
                {
                    dsbrandbasedsearch = new DataSet("BrandBasedSearch");
                    dtbrandBasedSearch.AcceptChanges();
                    dsbrandbasedsearch.Tables.Add(dtbrandBasedSearch);
                }
                return dsbrandbasedsearch;
            }
            return null;
        }
        public static DataSet GetBrandBasedSearchReport(Model.BrandBasedSearchModel brandBasedSearchModel,Model.BrandBasedSeach m_brandSearchoptions, Model.IDModeRecordModel allModeNames, String tableName)
        {
            if (m_brandSearchoptions.Model == false)
                return(GetBrandBasedWithoutModelReport(brandBasedSearchModel, m_brandSearchoptions, tableName));
            else
                return(GetBrandBasedWithModelReport(brandBasedSearchModel, m_brandSearchoptions, allModeNames, tableName));
        }
        public Model.BrandBasedSearchModel GetBrandBasedSearchResult(IList<Model.Id> m_ids, IList<Model.Region> locations, List<String> selectedModes,
        IList<Model.DeviceType> devices, IList<Model.DataSources> dataSources, Model.BrandBasedSeach m_brandSearchOptions, Model.IdSetupCodeInfo idSetupCodeInfoParam)
        {
            Model.BrandBasedSearchModel model = null;
            IDSearchParameter searchParam = null;
            try
            {
                searchParam = new IDSearchParameter();
                StringBuilder paramBuilder = new StringBuilder();
                String idStr = String.Empty;
                foreach (Model.Id id in m_ids)
                    paramBuilder.Append(id.ID + ",");
                if (paramBuilder.Length > 0)
                    paramBuilder.Remove(paramBuilder.Length - 1, 1);
                idStr = paramBuilder.ToString();

                //add locations xml
                String strLocations = String.Empty;
                if (locations != null)
                {
                    _XML = new XMLOperations("UEI2Server", XMLOperations.EncodingType.UTF16);
                    _XML.ParentNode = "Document";
                    _XML.ParentAttributeName = "Type";
                    _XML.ParentAttributeValue = "LocationSelections";
                    _XML.ParentAttributeName = "Version";
                    _XML.ParentAttributeValue = "1.0.0";
                    _XML.AppendParent();

                    foreach (Model.Region r in locations)
                    {
                        _XML.ParentNode = "Region";
                        _XML.ParentAttributeName = "Name";
                        _XML.ParentAttributeValue = r.Name;
                        _XML.ParentAttributeName = "Weight";
                        _XML.ParentAttributeValue = "0";
                        _XML.ParentAttributeName = "SystemFlags";
                        if (idSetupCodeInfoParam.IncludeNonCountryLinkedRecords == true)
                        {
                            _XML.ParentAttributeValue = "2";
                        }
                        else
                        {
                            _XML.ParentAttributeValue = "0";
                        }
                        if (r.SubRegions != null)
                        {
                            foreach (Model.Region subregion in r.SubRegions)
                            {
                                _XML.ChildNode = "SubRegion";
                                _XML.ChildAttributeName = "Name";
                                _XML.ChildAttributeValue = subregion.Name;
                                _XML.ChildAttributeName = "Weight";
                                _XML.ChildAttributeValue = "0";
                                _XML.ChildAttributeName = "SystemFlags";
                                _XML.ChildAttributeValue = "0";
                                if (r.Countries != null)
                                {
                                    foreach (Model.Country country in r.Countries)
                                    {
                                        if (subregion.Name == country.SubRegion)
                                        {
                                            _XML.ChildNode = "Country";
                                            _XML.ChildAttributeName = "Name";
                                            _XML.ChildAttributeValue = country.Name;
                                            _XML.ChildAttributeName = "Weight";
                                            _XML.ChildAttributeValue = "0";
                                            _XML.ChildAttributeName = "SystemFlags";
                                            _XML.ChildAttributeValue = "0";
                                            _XML.AppendChild();
                                        }
                                    }
                                }
                                _XML.AppendChild();
                            }
                        }
                        if (r.Countries != null)
                        {
                            foreach (Model.Country country in r.Countries)
                            {
                                if (country.SubRegion == String.Empty)
                                {
                                    _XML.ChildNode = "Country";
                                    _XML.ChildAttributeName = "Name";
                                    _XML.ChildAttributeValue = country.Name;
                                    _XML.ChildAttributeName = "Weight";
                                    _XML.ChildAttributeValue = "0";
                                    _XML.ChildAttributeName = "SystemFlags";
                                    _XML.ChildAttributeValue = "0";
                                    _XML.AppendChild();
                                }
                            }
                        }
                        _XML.AppendParent();
                    }
                }
                strLocations = _XML.XMLGenerator();
                _XML = null;

                //add device xml
                String strDevices = String.Empty;
                if (devices != null)
                {
                    _XML = new XMLOperations("UEI2Server", XMLOperations.EncodingType.UTF16);
                    _XML.ParentNode = "Document";
                    _XML.ParentAttributeName = "Type";
                    _XML.ParentAttributeValue = "DeviceSelections";
                    _XML.ParentAttributeName = "Version";
                    _XML.ParentAttributeValue = "1.0.0";
                    _XML.AppendParent();

                    foreach (String mode in selectedModes)
                    {
                        _XML.ParentNode = "DeviceMode";
                        _XML.ParentAttributeName = "Name";
                        _XML.ParentAttributeValue = mode;
                        _XML.ParentAttributeName = "SystemFlags";
                        //if (idSetupCodeInfoParam.IncludeEmptyDeviceType == true)
                        //    _XML.ParentAttributeValue = "3";
                        //else if (idSetupCodeInfoParam.IncludeEmptyDeviceType == false)
                        //    _XML.ParentAttributeValue = "1";
                        if(idSetupCodeInfoParam.IncludeNonComponentLinkedRecords == true && idSetupCodeInfoParam.IncludeEmptyDeviceType == true)
                            _XML.ParentAttributeValue = "7";
                        else if(idSetupCodeInfoParam.IncludeNonComponentLinkedRecords == true && idSetupCodeInfoParam.IncludeEmptyDeviceType == false)
                            _XML.ParentAttributeValue = "4";
                        else if(idSetupCodeInfoParam.IncludeNonComponentLinkedRecords == false && idSetupCodeInfoParam.IncludeEmptyDeviceType == true)
                            _XML.ParentAttributeValue = "3";
                        else if(idSetupCodeInfoParam.IncludeNonComponentLinkedRecords == false && idSetupCodeInfoParam.IncludeEmptyDeviceType == false)
                            _XML.ParentAttributeValue = "1";
                        foreach (Model.DeviceType device in devices)
                        {
                            if (device.Mode == mode)
                            {
                                _XML.ChildNode = "MainDevices";
                                _XML.ChildAttributeName = "Name";
                                _XML.ChildAttributeValue = device.Name;
                                _XML.ChildAttributeName = "SystemFlags";
                                _XML.ChildAttributeValue = "1";
                                if (device.SubDevices != null)
                                {
                                    foreach (Model.DeviceType subdevice in device.SubDevices)
                                    {
                                        _XML.ChildNode = "SubDevices";
                                        _XML.ChildAttributeName = "Name";
                                        _XML.ChildAttributeValue = subdevice.Name;
                                        _XML.ChildAttributeName = "SystemFlags";
                                        _XML.ChildAttributeValue = "1";
                                        if (device.Components != null)
                                        {
                                            foreach (Model.Component component in device.Components)
                                            {
                                                if (subdevice.Name == component.SubDeviceType)
                                                {
                                                    _XML.ChildNode = "Component";
                                                    _XML.ChildAttributeName = "Name";
                                                    _XML.ChildAttributeValue = component.Name;
                                                    _XML.ChildAttributeName = "SystemFlags";
                                                    _XML.ChildAttributeValue = "0";
                                                    _XML.ChildAttributeName = "SystemFlagsExt";
                                                    _XML.ChildAttributeValue = "1";
                                                    _XML.AppendChild();
                                                }
                                            }
                                        }
                                        _XML.AppendChild();
                                    }
                                }
                                if (device.Components != null)
                                {
                                    foreach (Model.Component component in device.Components)
                                    {
                                        if (component.SubDeviceType == String.Empty)
                                        {
                                            _XML.ChildNode = "Component";
                                            _XML.ChildAttributeName = "Name";
                                            _XML.ChildAttributeValue = component.Name;
                                            _XML.ChildAttributeName = "SystemFlags";
                                            _XML.ChildAttributeValue = "0";
                                            _XML.ChildAttributeName = "SystemFlagsExt";
                                            _XML.ChildAttributeValue = "1";
                                            _XML.AppendChild();
                                        }
                                    }
                                }
                                _XML.AppendChild();
                            }
                        }
                        _XML.AppendParent();
                    }
                }
                strDevices = _XML.XMLGenerator();
                _XML = null;

                paramBuilder = new StringBuilder();
                String dataSourceStr = String.Empty;
                if (dataSources != null)
                {
                    foreach (Model.DataSources l in dataSources)
                        paramBuilder.Append(l.Name + ",");
                    if (paramBuilder.Length > 0)
                        paramBuilder.Remove(paramBuilder.Length - 1, 1);
                    dataSourceStr = paramBuilder.ToString();
                }

                paramBuilder = new StringBuilder();
                String brandStr = String.Empty;
                if (m_brandSearchOptions.Brand != null)
                {
                    foreach (String item in m_brandSearchOptions.Brand)
                        paramBuilder.Append(item + ",");

                    if (paramBuilder.Length > 0)
                        paramBuilder.Remove(paramBuilder.Length - 1, 1);

                    brandStr = paramBuilder.ToString();
                }
                ErrorLogging.WriteToLogFile("--Function Name: GetBrandBasedSearchResult--", false);

                searchParam.Ids = idStr;
                searchParam.Brands = brandStr;
                searchParam.XMLLocations = strLocations;
                searchParam.XMLDevices = strDevices;
                searchParam.DataSources = dataSourceStr;                
                searchParam.RegionType = 0;
                if (m_brandSearchOptions.Model)//if model is enabled.
                {
                    searchParam.IncudeRecordWithoutModelName = (m_brandSearchOptions.IncludeBlankModelNames) ? 1 : 0;
                    searchParam.ModelType = m_brandSearchOptions.ModelType;
                }
                IDBrandResultCollection resultCollection = null;
                if (m_brandSearchOptions.Avail == 0)
                {
                    //Get for UEI2 only
                    ErrorLogging.WriteToLogFile("dbo.UEI2_Sp_GetBrandReport execution time", false);
                    DateTime spStart = DateTime.Now;
                    ErrorLogging.WriteToLogFile("Start Time: " + spStart.Hour.ToString() + ":" + spStart.Minute.ToString() + ":" + spStart.Second.ToString() + ":" + spStart.Millisecond.ToString(), false);

                    searchParam.SearchFlag = "0";
                    resultCollection = _DBAccess.GetBrandSearchList(searchParam);
                    DateTime spEnd = DateTime.Now;
                    ErrorLogging.WriteToLogFile("End Time: " + spEnd.Hour.ToString() + ":" + spEnd.Minute.ToString() + ":" + spEnd.Second.ToString() + ":" + spEnd.Millisecond.ToString(), false);

                    totalExecTime = spEnd - spStart;
                    ErrorLogging.WriteToLogFile("Total sp Time: " + totalExecTime.Hours.ToString() + ":" + totalExecTime.Minutes.ToString() + ":" + totalExecTime.Seconds.ToString() + ":" + totalExecTime.Milliseconds.ToString(), false);

                    model = GetBrandSearchRecords(resultCollection, m_brandSearchOptions);
                }
                else if (m_brandSearchOptions.Avail == 1)
                {
                    //Get UEI2 Only
                    ErrorLogging.WriteToLogFile("dbo.UEI2_Sp_GetBrandReport execution time", false);
                    DateTime spStart = DateTime.Now;
                    ErrorLogging.WriteToLogFile("Start Time: " + spStart.Hour.ToString() + ":" + spStart.Minute.ToString() + ":" + spStart.Second.ToString() + ":" + spStart.Millisecond.ToString(), false);

                    m_brandSearchOptions.Avail = 0;
                    searchParam.SearchFlag = "0";
                    resultCollection = _DBAccess.GetBrandSearchList(searchParam);

                    DateTime spEnd = DateTime.Now;
                    ErrorLogging.WriteToLogFile("End Time: " + spEnd.Hour.ToString() + ":" + spEnd.Minute.ToString() + ":" + spEnd.Second.ToString() + ":" + spEnd.Millisecond.ToString(), false);

                    totalExecTime = spEnd - spStart;
                    ErrorLogging.WriteToLogFile("Total sp Time: " + totalExecTime.Hours.ToString() + ":" + totalExecTime.Minutes.ToString() + ":" + totalExecTime.Seconds.ToString() + ":" + totalExecTime.Milliseconds.ToString(), false);

                    totalExecTime = TimeSpan.Zero;

                    Model.BrandBasedSearchModel m_UEI2Model = GetBrandSearchRecords(resultCollection, m_brandSearchOptions);

                    //Get for POS Only
                    ErrorLogging.WriteToLogFile("dbo.UEI2_Sp_GetBrandReport execution time", false);
                    spStart = DateTime.Now;
                    ErrorLogging.WriteToLogFile("Start Time: " + spStart.Hour.ToString() + ":" + spStart.Minute.ToString() + ":" + spStart.Second.ToString() + ":" + spStart.Millisecond.ToString(), false);

                    m_brandSearchOptions.Avail = 1;
                    searchParam.SearchFlag = "1";
                    searchParam.StartDate = m_brandSearchOptions.POSStartDate;
                    searchParam.EndDate = m_brandSearchOptions.POSEndDate;
                    resultCollection = _DBAccess.GetBrandSearchList(searchParam);

                    spEnd = DateTime.Now;
                    ErrorLogging.WriteToLogFile("End Time: " + spEnd.Hour.ToString() + ":" + spEnd.Minute.ToString() + ":" + spEnd.Second.ToString() + ":" + spEnd.Millisecond.ToString(), false);

                    totalExecTime = spEnd - spStart;
                    ErrorLogging.WriteToLogFile("Total sp Time: " + totalExecTime.Hours.ToString() + ":" + totalExecTime.Minutes.ToString() + ":" + totalExecTime.Seconds.ToString() + ":" + totalExecTime.Milliseconds.ToString(), false);                    

                    model = GetBrandSearchRecords(resultCollection,m_UEI2Model, m_brandSearchOptions);
                }
                else if (m_brandSearchOptions.Avail == 2)
                {
                    //Get UEI2 Only
                    ErrorLogging.WriteToLogFile("dbo.UEI2_Sp_GetBrandReport execution time", false);
                    DateTime spStart = DateTime.Now;
                    ErrorLogging.WriteToLogFile("Start Time: " + spStart.Hour.ToString() + ":" + spStart.Minute.ToString() + ":" + spStart.Second.ToString() + ":" + spStart.Millisecond.ToString(), false);

                    m_brandSearchOptions.Avail = 0;
                    searchParam.SearchFlag = "0";
                    resultCollection = _DBAccess.GetBrandSearchList(searchParam);

                    DateTime spEnd = DateTime.Now;
                    ErrorLogging.WriteToLogFile("End Time: " + spEnd.Hour.ToString() + ":" + spEnd.Minute.ToString() + ":" + spEnd.Second.ToString() + ":" + spEnd.Millisecond.ToString(), false);

                    totalExecTime = spEnd - spStart;
                    ErrorLogging.WriteToLogFile("Total sp Time: " + totalExecTime.Hours.ToString() + ":" + totalExecTime.Minutes.ToString() + ":" + totalExecTime.Seconds.ToString() + ":" + totalExecTime.Milliseconds.ToString(), false);
                    totalExecTime = TimeSpan.Zero;

                    Model.BrandBasedSearchModel m_UEI2Model = GetBrandSearchRecords(resultCollection, m_brandSearchOptions);

                    //Get for Online Search Statistics Only
                    ErrorLogging.WriteToLogFile("dbo.UEI2_Sp_GetBrandReport execution time", false);
                    spStart = DateTime.Now;
                    ErrorLogging.WriteToLogFile("Start Time: " + spStart.Hour.ToString() + ":" + spStart.Minute.ToString() + ":" + spStart.Second.ToString() + ":" + spStart.Millisecond.ToString(), false);

                    m_brandSearchOptions.Avail = 2;
                    searchParam.SearchFlag = "2";
                    searchParam.StartDate = m_brandSearchOptions.OSSStartDate;
                    searchParam.EndDate = m_brandSearchOptions.OSSEndDate;
                    resultCollection = _DBAccess.GetBrandSearchList(searchParam);

                    spEnd = DateTime.Now;
                    ErrorLogging.WriteToLogFile("End Time: " + spEnd.Hour.ToString() + ":" + spEnd.Minute.ToString() + ":" + spEnd.Second.ToString() + ":" + spEnd.Millisecond.ToString(), false);

                    totalExecTime = spEnd - spStart;
                    ErrorLogging.WriteToLogFile("Total sp Time: " + totalExecTime.Hours.ToString() + ":" + totalExecTime.Minutes.ToString() + ":" + totalExecTime.Seconds.ToString() + ":" + totalExecTime.Milliseconds.ToString(), false);
                    totalExecTime = TimeSpan.Zero;

                    model = GetBrandSearchRecords(resultCollection,m_UEI2Model, m_brandSearchOptions);
                }
                else if (m_brandSearchOptions.Avail == 3)
                {
                    //Get UEI2 only
                    ErrorLogging.WriteToLogFile("dbo.UEI2_Sp_GetBrandReport execution time", false);
                    DateTime spStart = DateTime.Now;
                    ErrorLogging.WriteToLogFile("Start Time: " + spStart.Hour.ToString() + ":" + spStart.Minute.ToString() + ":" + spStart.Second.ToString() + ":" + spStart.Millisecond.ToString(), false);
                    m_brandSearchOptions.Avail = 0;
                    searchParam.SearchFlag = "0";
                    resultCollection = _DBAccess.GetBrandSearchList(searchParam);

                    DateTime spEnd = DateTime.Now;
                    ErrorLogging.WriteToLogFile("End Time: " + spEnd.Hour.ToString() + ":" + spEnd.Minute.ToString() + ":" + spEnd.Second.ToString() + ":" + spEnd.Millisecond.ToString(), false);

                    totalExecTime = spEnd - spStart;
                    ErrorLogging.WriteToLogFile("Total sp Time: " + totalExecTime.Hours.ToString() + ":" + totalExecTime.Minutes.ToString() + ":" + totalExecTime.Seconds.ToString() + ":" + totalExecTime.Milliseconds.ToString(), false);
                    totalExecTime = TimeSpan.Zero;

                    Model.BrandBasedSearchModel m_UEI2Model = GetBrandSearchRecords(resultCollection, m_brandSearchOptions);

                    //Get for POS Only
                    ErrorLogging.WriteToLogFile("dbo.UEI2_Sp_GetBrandReport execution time", false);
                    spStart = DateTime.Now;
                    ErrorLogging.WriteToLogFile("Start Time: " + spStart.Hour.ToString() + ":" + spStart.Minute.ToString() + ":" + spStart.Second.ToString() + ":" + spStart.Millisecond.ToString(), false);
                    m_brandSearchOptions.Avail = 1;
                    searchParam.SearchFlag = "1";
                    searchParam.StartDate = m_brandSearchOptions.POSStartDate;
                    searchParam.EndDate = m_brandSearchOptions.POSEndDate;
                    resultCollection = _DBAccess.GetBrandSearchList(searchParam);

                    spEnd = DateTime.Now;
                    ErrorLogging.WriteToLogFile("End Time: " + spEnd.Hour.ToString() + ":" + spEnd.Minute.ToString() + ":" + spEnd.Second.ToString() + ":" + spEnd.Millisecond.ToString(), false);

                    totalExecTime = spEnd - spStart;
                    ErrorLogging.WriteToLogFile("Total sp Time: " + totalExecTime.Hours.ToString() + ":" + totalExecTime.Minutes.ToString() + ":" + totalExecTime.Seconds.ToString() + ":" + totalExecTime.Milliseconds.ToString(), false);
                    totalExecTime = TimeSpan.Zero;

                    Model.BrandBasedSearchModel m_POSModel = GetBrandSearchRecords(resultCollection,m_UEI2Model, m_brandSearchOptions);
                    
                    //Get for Online Search Statistics Only
                    ErrorLogging.WriteToLogFile("dbo.UEI2_Sp_GetBrandReport execution time", false);
                    spStart = DateTime.Now;
                    ErrorLogging.WriteToLogFile("Start Time: " + spStart.Hour.ToString() + ":" + spStart.Minute.ToString() + ":" + spStart.Second.ToString() + ":" + spStart.Millisecond.ToString(), false);
                    m_brandSearchOptions.Avail = 2;
                    searchParam.SearchFlag = "2";
                    searchParam.StartDate = m_brandSearchOptions.OSSStartDate;
                    searchParam.EndDate = m_brandSearchOptions.OSSEndDate;                    
                    resultCollection = _DBAccess.GetBrandSearchList(searchParam);
                    spEnd = DateTime.Now;
                    ErrorLogging.WriteToLogFile("End Time: " + spEnd.Hour.ToString() + ":" + spEnd.Minute.ToString() + ":" + spEnd.Second.ToString() + ":" + spEnd.Millisecond.ToString(), false);

                    totalExecTime = spEnd - spStart;
                    ErrorLogging.WriteToLogFile("Total sp Time: " + totalExecTime.Hours.ToString() + ":" + totalExecTime.Minutes.ToString() + ":" + totalExecTime.Seconds.ToString() + ":" + totalExecTime.Milliseconds.ToString(), false);
                    totalExecTime = TimeSpan.Zero;

                    model = GetBrandSearchRecords(resultCollection,m_POSModel, m_brandSearchOptions);
                    //Reset Brand Availability
                    m_brandSearchOptions.Avail = 3;
                }
                return (model);
            }
            catch (Exception ex)
            {
                ErrorLogging.WriteToErrorLogFile(ex, searchParam);
                return null;
            }
        }
        private Model.BrandBasedSearchModel GetBrandSearchRecords(IDBrandResultCollection searchResults,Model.BrandBasedSearchModel reportModel, Model.BrandBasedSeach m_brandSearchOption)
        {
            if (searchResults != null && searchResults.Count > 0)
            {                
                foreach (IDBrandResult item in searchResults)
                {
                    if (item.Brand != "0")
                    {
                        String m_Mode = String.Empty;
                        if (Convert.ToString(item.ID) != null)
                            m_Mode = item.ID.Substring(0, 1).ToUpper();
                        Model.BrandBasedSearchRecord record = reportModel.GetBrandSearch(item.Brand, m_Mode);
                        if (record == null)
                        {
                            record = new Model.BrandBasedSearchRecord();
                            record.Mode = m_Mode;
                            if (Convert.ToString(item.Brand) != null)
                                record.Brand = item.Brand;

                            //Get Data Source Info
                            Model.DataSourceInfo m_DataSourceInfo = null;
                            if (Convert.ToString(item.DataSource) != null)
                            {
                                m_DataSourceInfo = new Model.DataSourceInfo();
                                m_DataSourceInfo.Name = item.DataSource;
                            }
                            //Get Model Info
                            Model.ModelInfo m_ModelInfo = null;
                            if (Convert.ToString(item.Model) != null)
                            {
                                m_ModelInfo = new Model.ModelInfo();
                                m_ModelInfo.Name = item.Model;
                                m_ModelInfo.ModelCount = 1;
                                if (m_DataSourceInfo != null)
                                    m_ModelInfo.DataSources.Add(m_DataSourceInfo);
                            }
                            //Get ID Info
                            Model.IDInfo m_IDInfo = null;
                            if (Convert.ToString(item.ID) != null)
                            {
                                m_IDInfo = new Model.IDInfo();
                                m_IDInfo.ID = item.ID.ToUpper();
                                //Update for Brand Availability
                                if (m_brandSearchOption.Avail == 0)
                                {
                                    m_IDInfo.UEI2Avail = (item.Availability == 0) ? "" : "Y";
                                }
                                if (m_brandSearchOption.Avail == 1)
                                {
                                    m_IDInfo.POSAvail = (item.Availability == 0) ? "" : "Y";
                                }
                                if (m_brandSearchOption.Avail == 2)
                                {
                                    m_IDInfo.OSSAvail = (item.Availability == 0) ? "" : "Y";
                                }
                                if (m_ModelInfo != null)
                                {
                                    m_IDInfo.ModelCount = 1;
                                    m_IDInfo.ModelList.Add(m_ModelInfo);
                                }
                            }
                            //Get Device Info
                            Model.DeviceInfo m_DeviceInfo = null;
                            if (Convert.ToString(item.DeviceType) != null)
                            {
                                m_DeviceInfo = new Model.DeviceInfo();
                                m_DeviceInfo.Name = item.DeviceType;
                                if (m_IDInfo != null)
                                    m_DeviceInfo.IdList.Add(m_IDInfo);
                            }
                            if (m_DeviceInfo != null)
                                record.DeviceTypes.Add(m_DeviceInfo); ;
                            reportModel.Add(record);
                            record = null;
                        }
                        else
                        {
                            //check if device type exists for the same brand
                            //if not append new device alse update id to existing device
                            Model.DeviceInfo m_DeviceInfo = reportModel.GetBrandSearch(record.DeviceTypes, item.DeviceType);
                            if (m_DeviceInfo == null)
                            {
                                //Get Data Source Info
                                Model.DataSourceInfo m_DataSourceInfo = null;
                                if (Convert.ToString(item.DataSource) != null)
                                {
                                    m_DataSourceInfo = new Model.DataSourceInfo();
                                    m_DataSourceInfo.Name = item.DataSource;
                                }
                                //Get Model Info
                                Model.ModelInfo m_ModelInfo = null;
                                if (Convert.ToString(item.Model) != null)
                                {
                                    m_ModelInfo = new Model.ModelInfo();
                                    m_ModelInfo.Name = item.Model;
                                    m_ModelInfo.ModelCount = 1;
                                    if (m_DataSourceInfo != null)
                                        m_ModelInfo.DataSources.Add(m_DataSourceInfo);
                                }
                                //Get ID Info
                                Model.IDInfo m_IDInfo = null;
                                if (Convert.ToString(item.ID) != null)
                                {
                                    m_IDInfo = new Model.IDInfo();
                                    m_IDInfo.ID = item.ID.ToUpper();
                                    //Update for Brand Availability
                                    if (m_brandSearchOption.Avail == 0)
                                    {
                                        m_IDInfo.UEI2Avail = (item.Availability == 0) ? "" : "Y";
                                    }
                                    if (m_brandSearchOption.Avail == 1)
                                    {
                                        m_IDInfo.POSAvail = (item.Availability == 0) ? "" : "Y";
                                    }
                                    if (m_brandSearchOption.Avail == 2)
                                    {
                                        m_IDInfo.OSSAvail = (item.Availability == 0) ? "" : "Y";
                                    }
                                    if (m_ModelInfo != null)
                                        m_IDInfo.ModelList.Add(m_ModelInfo);
                                }
                                //Update Device Info                            
                                if (Convert.ToString(item.DeviceType) != null)
                                {
                                    m_DeviceInfo = new Model.DeviceInfo();
                                    m_DeviceInfo.Name = item.DeviceType;
                                    if (m_IDInfo != null)
                                        m_DeviceInfo.IdList.Add(m_IDInfo);
                                }
                                if (m_DeviceInfo != null)
                                    record.DeviceTypes.Add(m_DeviceInfo);
                            }
                            else
                            {
                                //check if id exits for the same device and brand
                                //if exists increase model count else add new id to the device
                                Model.IDInfo m_IDInfo = getIDInfo((List<Model.IDInfo>)m_DeviceInfo.IdList, item.ID);
                                if (m_IDInfo == null)
                                {
                                    //Get Data Source Info
                                    Model.DataSourceInfo m_DataSourceInfo = null;
                                    if (Convert.ToString(item.DataSource) != null)
                                    {
                                        m_DataSourceInfo = new Model.DataSourceInfo();
                                        m_DataSourceInfo.Name = item.DataSource;
                                    }
                                    //Get Model Info
                                    Model.ModelInfo m_ModelInfo = null;
                                    if (Convert.ToString(item.Model) != null)
                                    {
                                        m_ModelInfo = new Model.ModelInfo();
                                        m_ModelInfo.Name = item.Model;
                                        m_ModelInfo.ModelCount = 1;
                                        if (m_DataSourceInfo != null)
                                            m_ModelInfo.DataSources.Add(m_DataSourceInfo);
                                    }
                                    //Get ID Info                                
                                    if (Convert.ToString(item.ID) != null)
                                    {
                                        m_IDInfo = new Model.IDInfo();
                                        m_IDInfo.ID = item.ID.ToUpper();
                                        //Update for Brand Availability
                                        if (m_brandSearchOption.Avail == 0)
                                        {
                                            m_IDInfo.UEI2Avail = (item.Availability == 0) ? "" : "Y";
                                        }
                                        if (m_brandSearchOption.Avail == 1)
                                        {
                                            m_IDInfo.POSAvail = (item.Availability == 0) ? "" : "Y";
                                        }
                                        if (m_brandSearchOption.Avail == 2)
                                        {
                                            m_IDInfo.OSSAvail = (item.Availability == 0) ? "" : "Y";
                                        }
                                        if (m_ModelInfo != null)
                                            m_IDInfo.ModelList.Add(m_ModelInfo);
                                    }
                                    //Update ID Info
                                    if (m_IDInfo != null)
                                        m_DeviceInfo.IdList.Add(m_IDInfo);
                                }
                                else
                                {
                                    //Update Brand Availability
                                    if (m_brandSearchOption.Avail == 0)
                                    {
                                        m_IDInfo.UEI2Avail = (item.Availability == 0) ? "" : "Y";
                                    }
                                    if (m_brandSearchOption.Avail == 1)
                                    {
                                        m_IDInfo.POSAvail = (item.Availability == 0) ? "" : "Y";
                                    }
                                    if (m_brandSearchOption.Avail == 2)
                                    {
                                        m_IDInfo.OSSAvail = (item.Availability == 0) ? "" : "Y";
                                    }
                                    //Check if model name exits for brand, device and id                                    
                                    Model.ModelInfo m_ModelInfo = GetModelInfo((IList<Model.ModelInfo>)m_IDInfo.ModelList, item.Model);
                                    if (m_ModelInfo == null)
                                    {
                                        //Get Data Source Info
                                        Model.DataSourceInfo m_DataSourceInfo = null;
                                        if (Convert.ToString(item.DataSource) != null)
                                        {
                                            m_DataSourceInfo = new Model.DataSourceInfo();
                                            m_DataSourceInfo.Name = item.DataSource;
                                        }
                                        //Get Model Info                                    
                                        if (Convert.ToString(item.Model) != null)
                                        {
                                            m_ModelInfo = new Model.ModelInfo();
                                            m_ModelInfo.Name = item.Model;
                                            m_ModelInfo.ModelCount = 1;
                                            if (m_DataSourceInfo != null)
                                                m_ModelInfo.DataSources.Add(m_DataSourceInfo);
                                        }                                        
                                        //Update Model Count for ID
                                        if (m_ModelInfo != null)
                                        {
                                            m_IDInfo.ModelList.Add(m_ModelInfo);
                                            m_IDInfo.ModelCount = m_IDInfo.ModelCount + 1;
                                        }
                                    }
                                    else
                                    {
                                        //Check for Data Source
                                        Model.DataSourceInfo m_DataSourceInfo = GetDataSourceInfo((List<Model.DataSourceInfo>)m_ModelInfo.DataSources, item.DataSource);
                                        if (m_DataSourceInfo == null)
                                        {
                                            m_DataSourceInfo = new Model.DataSourceInfo();
                                            if (Convert.ToString(item.DataSource) != null)
                                                m_DataSourceInfo.Name = item.DataSource;
                                            //Update Model Info
                                            if (m_ModelInfo != null)
                                                m_ModelInfo.DataSources.Add(m_DataSourceInfo);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return reportModel;
        }
        private Model.BrandBasedSearchModel GetBrandSearchRecords(IDBrandResultCollection searchResults, Model.BrandBasedSeach m_brandSearchOption)
        {
            Model.BrandBasedSearchModel records = null;
            if (searchResults != null && searchResults.Count>0)
            {
                records = new Model.BrandBasedSearchModel();
                foreach (IDBrandResult item in searchResults)
                {
                    if (item.Brand != "0")
                    {
                        String m_Mode = String.Empty;
                        if (Convert.ToString(item.ID) != null)
                            m_Mode = item.ID.Substring(0, 1).ToUpper();
                        Model.BrandBasedSearchRecord record = records.GetBrandSearch(item.Brand, m_Mode);
                        if (record == null)
                        {
                            record = new Model.BrandBasedSearchRecord();
                            record.Mode = m_Mode;
                            if (Convert.ToString(item.Brand) != null)
                                record.Brand = item.Brand;

                            //Get Data Source Info
                            Model.DataSourceInfo m_DataSourceInfo = null;
                            if (Convert.ToString(item.DataSource) != null)
                            {
                                m_DataSourceInfo = new Model.DataSourceInfo();
                                m_DataSourceInfo.Name = item.DataSource;
                            }
                            //Get Model Info
                            Model.ModelInfo m_ModelInfo = null;
                            if (Convert.ToString(item.Model) != null)
                            {
                                m_ModelInfo = new Model.ModelInfo();
                                m_ModelInfo.Name = item.Model;
                                m_ModelInfo.ModelCount = 1;
                                if (m_DataSourceInfo != null)
                                    m_ModelInfo.DataSources.Add(m_DataSourceInfo);
                            }
                            //Get ID Info
                            Model.IDInfo m_IDInfo = null;
                            if (Convert.ToString(item.ID) != null)
                            {
                                m_IDInfo = new Model.IDInfo();
                                m_IDInfo.ID = item.ID.ToUpper();
                                //Update for Brand Availability
                                if (m_brandSearchOption.Avail == 0)
                                {
                                    m_IDInfo.UEI2Avail = (item.Availability == 0) ? "" : "Y";
                                }
                                if (m_brandSearchOption.Avail == 1)
                                {
                                    m_IDInfo.POSAvail = (item.Availability == 0) ? "" : "Y";
                                }
                                if (m_brandSearchOption.Avail == 2)
                                {
                                    m_IDInfo.OSSAvail = (item.Availability == 0) ? "" : "Y";
                                }
                                if (m_ModelInfo != null)
                                {
                                    m_IDInfo.ModelCount = 1;
                                    m_IDInfo.ModelList.Add(m_ModelInfo);
                                }
                            }
                            //Get Device Info
                            Model.DeviceInfo m_DeviceInfo = null;
                            if (Convert.ToString(item.DeviceType) != null)
                            {
                                m_DeviceInfo = new Model.DeviceInfo();
                                m_DeviceInfo.Name = item.DeviceType;
                                if (m_IDInfo != null)
                                    m_DeviceInfo.IdList.Add(m_IDInfo);
                            }
                            if (m_DeviceInfo != null)
                                record.DeviceTypes.Add(m_DeviceInfo); ;
                            records.Add(record);
                            record = null;
                        }
                        else
                        {
                            //check if device type exists for the same brand
                            //if not append new device alse update id to existing device
                            Model.DeviceInfo m_DeviceInfo = records.GetBrandSearch(record.DeviceTypes, item.DeviceType);
                            if (m_DeviceInfo == null)
                            {
                                //Get Data Source Info
                                Model.DataSourceInfo m_DataSourceInfo = null;
                                if (Convert.ToString(item.DataSource) != null)
                                {
                                    m_DataSourceInfo = new Model.DataSourceInfo();
                                    m_DataSourceInfo.Name = item.DataSource;
                                }
                                //Get Model Info
                                Model.ModelInfo m_ModelInfo = null;
                                if (Convert.ToString(item.Model) != null)
                                {
                                    m_ModelInfo = new Model.ModelInfo();
                                    m_ModelInfo.Name = item.Model;
                                    m_ModelInfo.ModelCount = 1;
                                    if (m_DataSourceInfo != null)
                                        m_ModelInfo.DataSources.Add(m_DataSourceInfo);
                                }
                                //Get ID Info
                                Model.IDInfo m_IDInfo = null;
                                if (Convert.ToString(item.ID) != null)
                                {
                                    m_IDInfo = new Model.IDInfo();
                                    m_IDInfo.ID = item.ID.ToUpper();
                                    //Update for Brand Availability
                                    if (m_brandSearchOption.Avail == 0)
                                    {
                                        m_IDInfo.UEI2Avail = (item.Availability == 0) ? "" : "Y";
                                    }
                                    if (m_brandSearchOption.Avail == 1)
                                    {
                                        m_IDInfo.POSAvail = (item.Availability == 0) ? "" : "Y";
                                    }
                                    if (m_brandSearchOption.Avail == 2)
                                    {
                                        m_IDInfo.OSSAvail = (item.Availability == 0) ? "" : "Y";
                                    }
                                    if (m_ModelInfo != null)
                                        m_IDInfo.ModelList.Add(m_ModelInfo);
                                }
                                //Update Device Info                            
                                if (Convert.ToString(item.DeviceType) != null)
                                {
                                    m_DeviceInfo = new Model.DeviceInfo();
                                    m_DeviceInfo.Name = item.DeviceType;
                                    if (m_IDInfo != null)
                                        m_DeviceInfo.IdList.Add(m_IDInfo);
                                }
                                if (m_DeviceInfo != null)
                                    record.DeviceTypes.Add(m_DeviceInfo);
                            }
                            else
                            {
                                //check if id exits for the same device and brand
                                //if exists increase model count else add new id to the device
                                Model.IDInfo m_IDInfo = getIDInfo((List<Model.IDInfo>)m_DeviceInfo.IdList, item.ID);
                                if (m_IDInfo == null)
                                {
                                    //Get Data Source Info
                                    Model.DataSourceInfo m_DataSourceInfo = null;
                                    if (Convert.ToString(item.DataSource) != null)
                                    {
                                        m_DataSourceInfo = new Model.DataSourceInfo();
                                        m_DataSourceInfo.Name = item.DataSource;
                                    }
                                    //Get Model Info
                                    Model.ModelInfo m_ModelInfo = null;
                                    if (Convert.ToString(item.Model) != null)
                                    {
                                        m_ModelInfo = new Model.ModelInfo();
                                        m_ModelInfo.Name = item.Model;
                                        m_ModelInfo.ModelCount = 1;
                                        if (m_DataSourceInfo != null)
                                            m_ModelInfo.DataSources.Add(m_DataSourceInfo);
                                    }
                                    //Get ID Info                                
                                    if (Convert.ToString(item.ID) != null)
                                    {
                                        m_IDInfo = new Model.IDInfo();
                                        m_IDInfo.ID = item.ID.ToUpper();
                                        //Update for Brand Availability
                                        if (m_brandSearchOption.Avail == 0)
                                        {
                                            m_IDInfo.UEI2Avail = (item.Availability == 0) ? "" : "Y";
                                        }
                                        if (m_brandSearchOption.Avail == 1)
                                        {
                                            m_IDInfo.POSAvail = (item.Availability == 0) ? "" : "Y";
                                        }
                                        if (m_brandSearchOption.Avail == 2)
                                        {
                                            m_IDInfo.OSSAvail = (item.Availability == 0) ? "" : "Y";
                                        }
                                        if (m_ModelInfo != null)
                                            m_IDInfo.ModelList.Add(m_ModelInfo);
                                    }
                                    //Update ID Info
                                    if (m_IDInfo != null)
                                        m_DeviceInfo.IdList.Add(m_IDInfo);
                                }
                                else
                                {
                                    //Check if model name exits for brand, device and id
                                    Model.ModelInfo m_ModelInfo = GetModelInfo((IList<Model.ModelInfo>)m_IDInfo.ModelList, item.Model);
                                    if (m_ModelInfo == null)
                                    {
                                        //Get Data Source Info
                                        Model.DataSourceInfo m_DataSourceInfo = null;
                                        if (Convert.ToString(item.DataSource) != null)
                                        {
                                            m_DataSourceInfo = new Model.DataSourceInfo();
                                            m_DataSourceInfo.Name = item.DataSource;
                                        }
                                        //Get Model Info                                    
                                        if (Convert.ToString(item.Model) != null)
                                        {
                                            m_ModelInfo = new Model.ModelInfo();
                                            m_ModelInfo.Name = item.Model;
                                            m_ModelInfo.ModelCount = 1;
                                            if (m_DataSourceInfo != null)
                                                m_ModelInfo.DataSources.Add(m_DataSourceInfo);
                                        }
                                        //Update Model Count for ID
                                        if (m_ModelInfo != null)
                                        {
                                            m_IDInfo.ModelList.Add(m_ModelInfo);
                                            m_IDInfo.ModelCount = m_IDInfo.ModelCount + 1;
                                        }
                                    }
                                    else
                                    {
                                        //Check for Data Source
                                        Model.DataSourceInfo m_DataSourceInfo = GetDataSourceInfo((List<Model.DataSourceInfo>)m_ModelInfo.DataSources, item.DataSource);
                                        if (m_DataSourceInfo == null)
                                        {
                                            m_DataSourceInfo = new Model.DataSourceInfo();
                                            if (Convert.ToString(item.DataSource) != null)
                                                m_DataSourceInfo.Name = item.DataSource;
                                            //Update Model Info
                                            if (m_ModelInfo != null)
                                                m_ModelInfo.DataSources.Add(m_DataSourceInfo);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return records;
        }
        private Model.DataSourceInfo GetDataSourceInfo(List<Model.DataSourceInfo> datasourceList, String dataSource)
        {
            foreach (Model.DataSourceInfo item in datasourceList)
            {
                if (item.Name == dataSource)
                    return item;
            }
            return null;
        }
        #endregion

        #region Executor Id With Same Prefix     
        public class PrefixFuctionCollection
        {
            private String m_Prefix = String.Empty;
            public String Prefixes
            {
                get { return m_Prefix; }
                set { m_Prefix = value; }
            }
            private List<IDFunction> m_FunctionList = new List<IDFunction>();
            public List<IDFunction> FunctionList
            {
                get { return m_FunctionList; }
                set { m_FunctionList = value; }
            }
        }
        private Dictionary<Int32, Dictionary<String, PrefixFuctionCollection>> ProcessExecutorWithSamePrefix(Dictionary<Int32, Dictionary<String, PrefixFuctionCollection>> resultDict)
        {
            Dictionary<Int32, Dictionary<String, PrefixFuctionCollection>> result = new Dictionary<Int32, Dictionary<String, PrefixFuctionCollection>>();
            if(resultDict != null)
            {
                foreach(Int32 m_Executor in resultDict.Keys)
                {
                    Dictionary<String, PrefixFuctionCollection> m_ExIdPrefixFunctionList = resultDict[m_Executor];
                    foreach(String Id1 in m_ExIdPrefixFunctionList.Keys)
                    {
                        foreach(String Id2 in m_ExIdPrefixFunctionList.Keys)
                        {
                            if(Id1 != Id2)
                            {
                                if(m_ExIdPrefixFunctionList[Id1].Prefixes != String.Empty && m_ExIdPrefixFunctionList[Id2].Prefixes != String.Empty)
                                {
                                    if(m_ExIdPrefixFunctionList[Id1].Prefixes == m_ExIdPrefixFunctionList[Id2].Prefixes)
                                    {
                                        if(result.ContainsKey(m_Executor))
                                        {
                                            Dictionary<String, PrefixFuctionCollection> m_ExIdList = result[m_Executor];
                                            if(!m_ExIdList.ContainsKey(Id1))
                                            {
                                                PrefixFuctionCollection m_PrefixFuctionList = new PrefixFuctionCollection();
                                                m_PrefixFuctionList.Prefixes = m_ExIdPrefixFunctionList[Id1].Prefixes;
                                                m_PrefixFuctionList.FunctionList = m_ExIdPrefixFunctionList[Id1].FunctionList;
                                                m_ExIdList.Add(Id1, m_PrefixFuctionList);
                                            }
                                            else
                                            {
                                                PrefixFuctionCollection m_PrefixFuctionList = m_ExIdList[Id1];
                                                m_PrefixFuctionList.Prefixes = m_ExIdPrefixFunctionList[Id1].Prefixes;
                                            }
                                        }
                                        else
                                        {
                                            PrefixFuctionCollection m_PrefixFuctionList = new PrefixFuctionCollection();
                                            m_PrefixFuctionList.Prefixes = m_ExIdPrefixFunctionList[Id1].Prefixes;
                                            m_PrefixFuctionList.FunctionList = m_ExIdPrefixFunctionList[Id1].FunctionList;

                                            Dictionary<String, PrefixFuctionCollection> m_ExIdList = new Dictionary<String, PrefixFuctionCollection>();
                                            m_ExIdList.Add(Id1, m_PrefixFuctionList);
                                            result.Add(m_Executor, m_ExIdList);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return result;
        }
        public DataSet GetExecutorIdWithSamePrefix(IList<String> idList)
        {
            DataSet dsResult = null;
            try
            {
                Dictionary<Int32, Dictionary<String, PrefixFuctionCollection>> resultDict = new Dictionary<Int32, Dictionary<String, PrefixFuctionCollection>>();
                HashtableCollection results = _DBAccess.GetExecIdMap(idList);
                if(results != null && results.Count > 0)
                {
                    foreach(Hashtable tab in results)
                    {
                        Int32 executor = Convert.ToInt32(tab["Executor"]);
                        String id = tab["ID"].ToString();

                        Dictionary<String, PrefixFuctionCollection> m_ExIdList = null;                        
                        //Get Prefix List
                        IList m_PrefixList = _DBAccess.SelectPrefix(id);
                        //Get Function Data List
                        IList m_IdFunctionList = _DBAccess.IDFunctionSelect(id);    

                        PrefixFuctionCollection m_PrefixFuctionList = new PrefixFuctionCollection();
                        if(m_PrefixList != null)
                        {
                            //String m_Prefix = String.Empty;
                            foreach(Prefix m_Prefix in m_PrefixList)
                            {
                                m_PrefixFuctionList.Prefixes += m_Prefix.Data;  //PrefixesList.Add(m_Prefix);
                            }

                        }
                        if(m_IdFunctionList != null)
                        {
                            foreach(IDFunction m_IdFunction in m_IdFunctionList)
                            {
                                m_PrefixFuctionList.FunctionList.Add(m_IdFunction);
                            }
                        }
                       
                        if(resultDict.ContainsKey(executor))
                        {
                            m_ExIdList = resultDict[executor];
                            if(!m_ExIdList.ContainsKey(id))
                            {
                                m_ExIdList.Add(id, m_PrefixFuctionList);
                            }                            
                        }
                        else
                        {
                            m_ExIdList = new Dictionary<String, PrefixFuctionCollection>();                                                             
                            m_ExIdList.Add(id, m_PrefixFuctionList);
                            resultDict.Add(executor, m_ExIdList);
                        }
                    }
                    resultDict = ProcessExecutorWithSamePrefix(resultDict);

                    dsResult = new DataSet("ExPrefixData");
                    dsResult.Tables.Add("ExecIdSamePrefix");
                    dsResult.Tables["ExecIdSamePrefix"].Columns.Add("Executor");
                    dsResult.Tables["ExecIdSamePrefix"].Columns.Add("ID");
                    dsResult.Tables["ExecIdSamePrefix"].Columns.Add("DataLength");
                    dsResult.Tables["ExecIdSamePrefix"].Columns.Add("Prefix");                    
                    dsResult.Tables["ExecIdSamePrefix"].AcceptChanges();
                    DataRow dr = null;
                    if(resultDict != null && resultDict.Count > 0)
                    {                        
                        foreach(int m_Executor in resultDict.Keys)
                        {
                            Dictionary<String, PrefixFuctionCollection> m_ExIdList = resultDict[m_Executor];
                            foreach(String m_Id in m_ExIdList.Keys)
                            {
                                PrefixFuctionCollection m_PrefixData = m_ExIdList[m_Id];

                                DataRow[] dataRowSameExPrefix = dsResult.Tables["ExecIdSamePrefix"].Select("Executor ='" + m_Executor + "' and Prefix = '" + m_PrefixData.Prefixes + "' and DataLength = '" + m_PrefixData.FunctionList[0].Data.Length + "'");
                                if(dataRowSameExPrefix.Length > 0)
                                {
                                    foreach(DataRow dataRow in dataRowSameExPrefix)
                                    {
                                        if(!dataRow[1].ToString().Contains(m_Id))
                                        {
                                            dataRow[1] = dataRow[1] + ", " + m_Id;
                                        }
                                    }
                                }
                                else
                                {
                                    dr = dsResult.Tables["ExecIdSamePrefix"].NewRow();
                                    dr[0] = m_Executor;
                                    dr[1] = m_Id;
                                    dr[3] = m_PrefixData.Prefixes;
                                    dr[2] = m_PrefixData.FunctionList[0].Data.Length;
                                    dsResult.Tables["ExecIdSamePrefix"].Rows.Add(dr);
                                    dr = null;
                                }
                            }
                        }
                    }
                    dsResult.AcceptChanges();
                }     
            }
            catch
            {                
                throw;
            }
            return dsResult;
        }
        #endregion

        #region ExecIdMap
        public Dictionary<Int32, Model.ExIdMap> GetExecIdMap(IList<String> idList)
        {
            try
            {
                Dictionary<Int32, Model.ExIdMap> resultDict = new Dictionary<Int32, Model.ExIdMap>();
                
                HashtableCollection results = _DBAccess.GetExecIdMap(idList);
                if (results != null && results.Count > 0)
                {
                    foreach (Hashtable tab in results)
                    {
                        Int32 executor = Convert.ToInt32(tab["Executor"]);
                        String id = tab["ID"].ToString();
                        String brand = String.Empty;
                        if(tab["Brand"] != null)
                            brand = tab["Brand"].ToString();
                        if (resultDict.ContainsKey(executor))
                        {
                            Model.ExIdMap m_IdBrand = resultDict[executor];
                            //ids.Add(id);
                            m_IdBrand.Add(id, brand);
                        }
                        else
                        {
                            Model.ExIdMap m_IdBrand = new Model.ExIdMap();
                            m_IdBrand.Add(id, brand);                            
                            resultDict.Add(executor, m_IdBrand);
                        }
                    }
                }

                return resultDict;
            }
            catch (Exception)
            {

                throw;
            }
        }

        #endregion

        #region ConsolidateReports
        //Not used currently.
        //public DataTable GenerateConsolidatedReportColWise(bool totalExecutors, bool totalUsedExecutors, bool mainDevices, bool subDevices, bool deviceTypes,  bool standardBrand,bool aliasBrand,bool brandVariations, bool deviceModels, bool remoteModels, bool uniqueKeyFunctions, bool keyCount)
        //{
        //    try
        //    {
        //        DataTable dtResults = CreateResultTable(totalExecutors, totalUsedExecutors, mainDevices, subDevices, deviceTypes, standardBrand,aliasBrand,brandVariations, deviceModels, remoteModels, uniqueKeyFunctions, keyCount);

        //        ErrorLogging.WriteToLogFile("--Function Name: GenerateConsolidatedReport--", false);
        //        ErrorLogging.WriteToLogFile("Query execution time", false);
        //        DateTime spStart = DateTime.Now;
        //        ErrorLogging.WriteToLogFile("Start Time: " + spStart.Hour.ToString() + ":" + spStart.Minute.ToString() + ":" + spStart.Second.ToString() + ":" + spStart.Millisecond.ToString(), false);

        //        Hashtable resultCollection = _DBAccess.GetTotalCounts(totalExecutors, totalUsedExecutors, mainDevices, subDevices, deviceTypes, standardBrand, aliasBrand, brandVariations, deviceModels, remoteModels, uniqueKeyFunctions, keyCount);

        //        if (resultCollection != null && resultCollection.Count > 0)
        //        {
        //            DataRow newRow = dtResults.NewRow();

        //            if (totalExecutors)
        //                newRow["ExecutorCount"] = resultCollection["ExecutorCount"];
                        

        //            if (totalUsedExecutors)
        //                newRow["UsedExecutorCount"] = resultCollection["UsedExecutorCount"];


        //            if (mainDevices)
        //                newRow["MainDeviceCount"] = resultCollection["MainDeviceCount"];
                    


        //            if (subDevices)
        //                newRow["SubDeviceCount"] = resultCollection["SubDeviceCount"];

        //            if (deviceTypes)
        //                newRow["DeviceTypeCount"] = resultCollection["DeviceTypeCount"];
                        
                    
        //            if (standardBrand)
        //            {
        //                HashtableCollection brandCounts = (HashtableCollection) resultCollection["BrandCount"];

        //                foreach (Hashtable item in brandCounts)
        //                {
        //                    if (item["BrandType"].ToString() == "Standard Brand")
        //                    {
        //                        newRow["StandardBrandCount"] = item["TotalCount"];
        //                        break;
        //                    }
        //                }
                        
                       
        //            }

        //            if (aliasBrand)
        //            {
        //                HashtableCollection brandCounts = (HashtableCollection)resultCollection["BrandCount"];

        //                foreach (Hashtable item in brandCounts)
        //                {
        //                    if (item["BrandType"].ToString() == "Alias Brand")
        //                    {
        //                        newRow["AliasBrandCount"] = item["TotalCount"];
        //                        break;
        //                    }
        //                }
        //            }

        //            if (brandVariations)
        //            {
        //                HashtableCollection brandCounts = (HashtableCollection)resultCollection["BrandCount"];

        //                foreach (Hashtable item in brandCounts)
        //                {
        //                    if (item["BrandType"].ToString() == "Brand Variation")
        //                    {
        //                        newRow["BrandVariationCount"] = item["TotalCount"];
        //                        break;
        //                    }
        //                }
        //            }


        //            if (deviceModels)
        //                newRow["DeviceModelCount"] = resultCollection["DeviceModelCount"];
                        
        //            if (remoteModels)
        //                newRow["RemoteModelCount"] = resultCollection["RemoteModelCount"];
                       
        //            if (uniqueKeyFunctions)
        //                newRow["KeyFunctionCount"] = resultCollection["KeyFunctionCount"];
                    
        //            if (keyCount)
        //                newRow["KeyCount"] = resultCollection["KeyCount"];
                       
                    

        //            dtResults.Rows.Add(newRow);
        //        }

        //        DateTime spEnd = DateTime.Now;
        //        ErrorLogging.WriteToLogFile("End Time: " + spEnd.Hour.ToString() + ":" + spEnd.Minute.ToString() + ":" + spEnd.Second.ToString() + ":" + spEnd.Millisecond.ToString(), false);

        //        totalExecTime = spEnd - spStart;
        //        ErrorLogging.WriteToLogFile("Total sp Time: " + totalExecTime.Hours.ToString() + ":" + totalExecTime.Minutes.ToString() + ":" + totalExecTime.Seconds.ToString() + ":" + totalExecTime.Milliseconds.ToString(), false);

        //        return dtResults;
        //    }
        //    catch(Exception ex)
        //    {
        //        ErrorLogging.WriteToErrorLogFile(ex);

        //        throw;
        //    }
        //}

        private DataTable CreateResultTable(bool totalExecutors, bool totalUsedExecutors, bool mainDevices, bool subDevices, bool deviceTypes, bool standardBrand, bool aliasBrand, bool brandVariations, bool deviceModels, bool remoteModels, bool uniqueKeyFunctions, bool keyCount)
        {
            DataTable dtResults = new DataTable();
            //create columns
            if (totalExecutors)
                dtResults.Columns.Add("ExecutorCount");

            if (totalUsedExecutors)
                dtResults.Columns.Add("UsedExecutorCount");

            if (mainDevices)
                dtResults.Columns.Add("MainDeviceCount");

            if (subDevices)
                dtResults.Columns.Add("SubDeviceCount");

            if (deviceTypes)
                dtResults.Columns.Add("DeviceTypeCount");

            if (standardBrand)
                dtResults.Columns.Add("StandardBrandCount");

            if (aliasBrand)
                dtResults.Columns.Add("AliasBrandCount");

            if (brandVariations)
                dtResults.Columns.Add("BrandVariationCount");

            if (deviceModels)
                dtResults.Columns.Add("DeviceModelCount");

            if (remoteModels)
                dtResults.Columns.Add("RemoteModelCount");

            if (uniqueKeyFunctions)
                dtResults.Columns.Add("KeyFunctionCount");

            if (keyCount)
                dtResults.Columns.Add("KeyCount");

            return dtResults;
        }
        public DataTable GenerateConsolidatedReport(bool totalExecutors, bool totalUsedExecutors, bool mainDevices, bool subDevices, bool deviceTypes, bool standardBrand, bool aliasBrand, bool brandVariations, bool deviceModels, bool remoteModels, bool uniqueKeyFunctions, bool keyCount)
        {
            try
            {
                DataTable dtResults = new DataTable("Consolidated Report");
                dtResults.Columns.Add("Description");
                dtResults.Columns.Add("Count");

                ErrorLogging.WriteToLogFile("--Function Name: GenerateConsolidatedReport--", false);
                ErrorLogging.WriteToLogFile("Query execution time", false);
                DateTime spStart = DateTime.Now;
                ErrorLogging.WriteToLogFile("Start Time: " + spStart.Hour.ToString() + ":" + spStart.Minute.ToString() + ":" + spStart.Second.ToString() + ":" + spStart.Millisecond.ToString(), false);

                Hashtable resultCollection = _DBAccess.GetTotalCounts(totalExecutors, totalUsedExecutors, mainDevices, subDevices, deviceTypes, standardBrand, aliasBrand, brandVariations, deviceModels, remoteModels, uniqueKeyFunctions, keyCount);

                if (resultCollection != null && resultCollection.Count > 0)
                {
                    

                    if (totalExecutors)
                    {
                        DataRow newRow = dtResults.NewRow();
                        newRow["Description"] = "Total Executor Count";
                        newRow["Count"] = resultCollection["ExecutorCount"];
                        dtResults.Rows.Add(newRow);

                    }

                    if (totalUsedExecutors)
                    {
                        DataRow newRow = dtResults.NewRow();
                        newRow["Description"] = "Used Executor Count";
                        newRow["Count"] = resultCollection["UsedExecutorCount"];
                        dtResults.Rows.Add(newRow);

                        //newRow["UsedExecutorCount"] = resultCollection["UsedExecutorCount"];
                    }

                    if (mainDevices)
                    {
                        DataRow newRow = dtResults.NewRow();
                        newRow["Description"] = "Main Device Count";
                        newRow["Count"] = resultCollection["MainDeviceCount"];
                        dtResults.Rows.Add(newRow);
                     //   newRow["MainDeviceCount"] = resultCollection["MainDeviceCount"];
                    }



                    if (subDevices)
                    {
                        DataRow newRow = dtResults.NewRow();
                        newRow["Description"] = "SubDevice Count";
                        newRow["Count"] = resultCollection["SubDeviceCount"];
                        dtResults.Rows.Add(newRow);
                     //   newRow["SubDeviceCount"] = resultCollection["SubDeviceCount"];
                    }

                    if (deviceTypes)
                    {
                        DataRow newRow = dtResults.NewRow();
                        newRow["Description"] = "DeviceType Count";
                        newRow["Count"] = resultCollection["DeviceTypeCount"];
                        dtResults.Rows.Add(newRow);
                        //   newRow["DeviceTypeCount"] = resultCollection["DeviceTypeCount"];
                    }


                    if (standardBrand)
                    {
                        HashtableCollection brandCounts = (HashtableCollection)resultCollection["BrandCount"];

                        foreach (Hashtable item in brandCounts)
                        {
                            if (item["BrandType"].ToString() == "Standard Brand")
                            {
                                DataRow newRow = dtResults.NewRow();
                                newRow["Description"] = "Standard Brand Count";
                                newRow["Count"] = item["TotalCount"];
                                dtResults.Rows.Add(newRow);

                                //newRow["StandardBrandCount"] = item["TotalCount"];
                                break;
                            }
                        }


                    }

                    if (aliasBrand)
                    {
                        HashtableCollection brandCounts = (HashtableCollection)resultCollection["BrandCount"];

                        foreach (Hashtable item in brandCounts)
                        {
                            if (item["BrandType"].ToString() == "Alias Brand")
                            {
                                DataRow newRow = dtResults.NewRow();
                                newRow["Description"] = "Alias Brand Count";
                                newRow["Count"] = item["TotalCount"];
                                dtResults.Rows.Add(newRow);
                                //newRow["AliasBrandCount"] = item["TotalCount"];
                                break;
                            }
                        }
                    }

                    if (brandVariations)
                    {
                        HashtableCollection brandCounts = (HashtableCollection)resultCollection["BrandCount"];

                        foreach (Hashtable item in brandCounts)
                        {
                            if (item["BrandType"].ToString() == "Brand Variation")
                            {
                                DataRow newRow = dtResults.NewRow();
                                newRow["Description"] = "Brand Variation Count";
                                newRow["Count"] = item["TotalCount"];
                                dtResults.Rows.Add(newRow);
                                //newRow["BrandVariationCount"] = item["TotalCount"];
                                break;
                            }
                        }
                    }


                    if (deviceModels)
                    {
                        DataRow newRow = dtResults.NewRow();
                        newRow["Description"] = "Device Model Count";
                        newRow["Count"] = resultCollection["DeviceModelCount"];
                        dtResults.Rows.Add(newRow);

                       // newRow["DeviceModelCount"] = resultCollection["DeviceModelCount"];
                    }

                    if (remoteModels)
                    {
                        DataRow newRow = dtResults.NewRow();
                        newRow["Description"] = "Remote Model Count";
                        newRow["Count"] = resultCollection["RemoteModelCount"];
                        dtResults.Rows.Add(newRow);

                        //   newRow["RemoteModelCount"] = resultCollection["RemoteModelCount"];
                    }

                    if (uniqueKeyFunctions)
                    {
                        DataRow newRow = dtResults.NewRow();
                        newRow["Description"] = "Key Function Count";
                        newRow["Count"] = resultCollection["KeyFunctionCount"];
                        dtResults.Rows.Add(newRow);
                        //newRow["KeyFunctionCount"] = resultCollection["KeyFunctionCount"];
                    }

                    if (keyCount)
                    {
                        DataRow newRow = dtResults.NewRow();
                        newRow["Description"] = "Key Count";
                        newRow["Count"] = resultCollection["KeyCount"];
                        dtResults.Rows.Add(newRow);
                        //   newRow["KeyCount"] = resultCollection["KeyCount"];
                    }



                }

                DateTime spEnd = DateTime.Now;
                ErrorLogging.WriteToLogFile("End Time: " + spEnd.Hour.ToString() + ":" + spEnd.Minute.ToString() + ":" + spEnd.Second.ToString() + ":" + spEnd.Millisecond.ToString(), false);

                totalExecTime = spEnd - spStart;
                ErrorLogging.WriteToLogFile("Total sp Time: " + totalExecTime.Hours.ToString() + ":" + totalExecTime.Minutes.ToString() + ":" + totalExecTime.Seconds.ToString() + ":" + totalExecTime.Milliseconds.ToString(), false);

                return dtResults;
            }
            catch (Exception ex)
            {
                ErrorLogging.WriteToErrorLogFile(ex);

                throw;
            }
        }                
        #endregion

        #region Supported Ids by Platform
        public IList<Hashtable> GetAllSupportedPlatforms()
        {
            ErrorLogging.WriteToLogFile("--Function Name: GetAllSupportedPlatforms--", false);
            ErrorLogging.WriteToLogFile("Query execution time", false);
            DateTime spStart = DateTime.Now;
            ErrorLogging.WriteToLogFile("Start Time: " + spStart.Hour.ToString() + ":" + spStart.Minute.ToString() + ":" + spStart.Second.ToString() + ":" + spStart.Millisecond.ToString(), false);

            IList<Hashtable> resultCollection = _DBAccess.GetAllSupportedPlatforms();
            DateTime spEnd = DateTime.Now;
            ErrorLogging.WriteToLogFile("End Time: " + spEnd.Hour.ToString() + ":" + spEnd.Minute.ToString() + ":" + spEnd.Second.ToString() + ":" + spEnd.Millisecond.ToString(), false);

            totalExecTime = spEnd - spStart;
            ErrorLogging.WriteToLogFile("Total sp Time: " + totalExecTime.Hours.ToString() + ":" + totalExecTime.Minutes.ToString() + ":" + totalExecTime.Seconds.ToString() + ":" + totalExecTime.Milliseconds.ToString(), false);
            return resultCollection;
        }
        public IList<String> GetAllSupportedIdsForPlatforms(IList<String> platforms)
        {
            ErrorLogging.WriteToLogFile("--Function Name: GetAllSupportedPlatforms--", false);
            ErrorLogging.WriteToLogFile("Query execution time", false);
            DateTime spStart = DateTime.Now;
            ErrorLogging.WriteToLogFile("Start Time: " + spStart.Hour.ToString() + ":" + spStart.Minute.ToString() + ":" + spStart.Second.ToString() + ":" + spStart.Millisecond.ToString(), false);

            IList<Hashtable> resultCollection = _DBAccess.GetAllSupportedIdsForPlatforms(platforms);
            DateTime spEnd = DateTime.Now;
            ErrorLogging.WriteToLogFile("End Time: " + spEnd.Hour.ToString() + ":" + spEnd.Minute.ToString() + ":" + spEnd.Second.ToString() + ":" + spEnd.Millisecond.ToString(), false);

            totalExecTime = spEnd - spStart;
            ErrorLogging.WriteToLogFile("Total sp Time: " + totalExecTime.Hours.ToString() + ":" + totalExecTime.Minutes.ToString() + ":" + totalExecTime.Seconds.ToString() + ":" + totalExecTime.Milliseconds.ToString(), false);
            IList<String> idList = null;
            if (resultCollection != null)
            {
                foreach (Hashtable item in resultCollection)
                {
                    String id = item["ID"].ToString().ToUpper();
                    if (idList == null)
                        idList = new List<String>();
                    int IDNumber;
                    if ((id.Length == 5) && (int.TryParse(id.Substring(1), out IDNumber)))
                    {
                        idList.Add(id);
                    }                    
                }
            }
            return idList;
        }
        #endregion

        #region Locations By Load
        public DataSet GetLocationsByLoad(IList<String> idList)
        {
            DataSet dsResult = null;
            try
            {                
                if (idList != null)
                {
                    String strId = String.Empty;
                    foreach (String item in idList)
                    {
                        strId += item + ",";
                    }
                    if (strId.Length > 0)
                        strId = strId.Remove(strId.Length - 1, 1);

                    ADONetDBOperations objDB = new ADONetDBOperations(DBConnectionString.UEIPUBLIC);
                    String strQuery = String.Format(@"IF EXISTS(SELECT NAME FROM TEMPDB..SYSOBJECTS
                                                            WHERE (NAME LIKE N'#LoadList') 
                                                                AND (TYPE = 'U'))
                                                        DROP TABLE [#LoadList];
                                                        CREATE TABLE [#LoadList](
                                                        ID	VARCHAR(5))
                                                        IF EXISTS(SELECT NAME FROM TEMPDB..SYSOBJECTS
                                                            WHERE (NAME LIKE N'#Locations') 
                                                                AND (TYPE = 'U'))
                                                        DROP TABLE [#Locations];
                                                        CREATE TABLE [#Locations] (
                                                        [Location_RID]      int NOT NULL,
                                                        [Region_RID]        int,
                                                        [StandardName_RID]  int,
                                                        [Region]            varchar(128) NOT NULL,
                                                        [SubRegion]         varchar(128)  NULL,
                                                        [Name]              varchar(128)  NULL)
                                                        --,PRIMARY KEY CLUSTERED ([Location_RID]))
                                                        IF EXISTS(SELECT NAME FROM TEMPDB..SYSOBJECTS
                                                            WHERE (NAME LIKE N'#SubLocations') 
                                                                AND (TYPE = 'U'))
                                                        DROP TABLE [#SubLocations];
                                                        CREATE TABLE [#SubLocations] (
                                                        [Location_RID]      int NOT NULL,
                                                        [Region_RID] int NOT NULL,
                                                        [Sub_Region_RID]     varchar(128) NOT NULL,
                                                        [Country_RID] int NOT NULL,
                                                        [Region]            varchar(128) NOT NULL,
                                                        [SubRegion]         varchar(128)  NULL,    
                                                        [Country]     varchar(128) NOT NULL,
                                                        PRIMARY KEY CLUSTERED ([Sub_Region_RID],[Country_RID])
                                                        );         
                                                        IF EXISTS(SELECT NAME FROM TEMPDB..SYSOBJECTS
                                                            WHERE (NAME LIKE N'#SubRegions') 
                                                                AND (TYPE = 'U'))
                                                        DROP TABLE [#SubRegions];
                                                        CREATE TABLE [#SubRegions] (
                                                        [Location_RID] int NOT NULL,
                                                        [Region_RID] int NOT NULL,
                                                        [StandardName_RID]  int,
                                                        [Region]     varchar(128) NOT NULL,
                                                        [SubRegion]  varchar(128) NOT NULL,
                                                        PRIMARY KEY CLUSTERED ([Location_RID],[Region_RID])
                                                        ); 
                                                        --Extract IDs
                                                        INSERT INTO #LoadList  
                                                        SELECT DISTINCT [str] 
                                                        FROM iter_charlist_to_table({0}, ',');
                                                        -- Extract Regions
                                                        INSERT [#Locations] 
                                                                ([Location_RID],[Region_RID],[StandardName_RID],[Region])
                                                        SELECT [Location_RID], [Location_RID],[LK_StandardName_RID], [Name]
                                                        FROM  [Locations] AS [T2] 
                                                        WHERE [T2].FK_LocationType_RID = dbo.ufn_GetLocationTypeID('Region');        
                                                        -- Add Countries related to Regions
                                                        INSERT [#Locations] 
                                                                ([Location_RID],[Region_RID],[StandardName_RID],[Region],[Name])
                                                        SELECT [T2].Location_RID ,[T1].Location_RID , [T2].LK_StandardName_RID, 
                                                                    [T1].Name,[T2].Name
                                                        FROM Locations  AS [T1] 
                                                        INNER JOIN RelatedLocations AS [T3] 
                                                        ON [T1].Location_RID = [T3].FK_Location_RID 
                                                        LEFT JOIN Locations AS [T2] 
                                                        ON [T3].FK_SubLocation_RID = [T2].Location_RID
                                                        INNER JOIN [#Locations] AS [T4]
                                                        ON [T4].Region=[T1].Name
                                                        WHERE [T2].FK_LocationType_RID = dbo.ufn_GetLocationTypeID('Country')
                                                        AND   [T3].SystemFlags !=1              
                                                        ORDER BY [T1].Location_RID ,[T2].Name;
                                                        -- Extract Sub-Regions
                                                        INSERT  [#SubRegions]
                                                                    ([Location_RID],[Region_RID],[StandardName_RID],[Region],[SubRegion])       
                                                        SELECT DISTINCT [T2].Location_RID,[T3].FK_Location_RID,[T2].LK_StandardName_RID,
                                                                                    [R].Region,[T2].Name
                                                        FROM Locations AS [T2] 
                                                        INNER JOIN RelatedLocations [T3]
                                                        ON [T2].Location_RID  = [T3].FK_SubLocation_RID
                                                        INNER JOIN [#Locations] [R]
                                                        ON [T3].FK_Location_RID = [R].Region_RID           
                                                        WHERE [T2].FK_LocationType_RID = dbo.ufn_GetLocationTypeID('Sub Region'); 
                                                        -- Insert Region-Subregion
                                                        INSERT [#Locations] 
                                                                ([Location_RID],[Region_RID],[StandardName_RID],[Region],[SubRegion])
                                                        SELECT [Location_RID],[Region_RID],[StandardName_RID],[Region],[SubRegion]
                                                        FROM [#SubRegions]
                                                        -- Extract Countries of Sub-Regions             
                                                        INSERT [#SubLocations]
                                                                ([Location_RID],[Region_RID],[Sub_Region_RID],[Country_RID],[Region],[SubRegion],[Country])              
                                                        SELECT DISTINCT [T2].ContryRID,[T1].Region_RID,[T2].SubRID,[T2].ContryRID,[T2].Region,[T2].SubRegion,[T2].Country
                                                        FROM [#Locations] [T1]
                                                        INNER JOIN 
                                                        (
                                                                SELECT DISTINCT [T1].Location_RID SubRID, [T1].Name SubRegion,
                                                                [T2].Location_RID ContryRID, [T2].Name Country,[T4].Region_RID,[T4].Region
                                                                FROM [#SubRegions] AS [T4]
                                                                INNER JOIN Locations  AS [T1] 
                                                                ON [T4].SubRegion=[T1].Name
                                                                INNER JOIN RelatedLocations AS [T3] 
                                                                ON [T1].Location_RID = [T3].FK_Location_RID 
                                                                LEFT JOIN Locations AS [T2] 
                                                                ON [T3].FK_SubLocation_RID = [T2].Location_RID
                                                                WHERE [T2].FK_LocationType_RID = dbo.ufn_GetLocationTypeID('Country')
                                                                AND    [T3].SystemFlags !=1
                                                        ) [T2]
                                                        ON T1.Region_RID = [T2].Region_RID
                                                        AND T1.Name = [T2].Country
                                                        ORDER BY SubRegion  
                                                        --Countries with Subregion
                                                        DELETE FROM [#Locations] 
                                                        WHERE Location_RID IN (SELECT DISTINCT Location_RID FROM [#SubLocations]) 
                                                        -- Insert Sub-Region Countries
                                                        INSERT [#Locations] 
                                                                ([Location_RID],[Region_RID],[StandardName_RID],[Region],[SubRegion],[Name])
                                                        SELECT [T1].Country_RID,[T1].Region_RID,[T2].LK_StandardName_RID,[T1].Region,
                                                                    [T1].SubRegion,[T1].Country
                                                        FROM [#SubLocations] [T1]
                                                        INNER JOIN Locations [T2]
                                                        ON [T1].Country_RID = [T2].Location_RID  
                                                        -- Update Custom Countries
                                                        INSERT [#Locations] 
                                                                ([Location_RID], [StandardName_RID],[Region_RID],[Region],[SubRegion], [Name])
                                                        SELECT DISTINCT [T1].[Location_RID], [T1].[LK_StandardName_RID],[T2].Region_RID,[T2].Region,
                                                                                [T2].SubRegion,[T1].[Name]
                                                        FROM [Locations] [T1] 
                                                        JOIN [#Locations] [T2]
                                                        ON [T1].[LK_StandardName_RID] = [T2].[Location_RID]
                                                        WHERE [T1].FK_LocationType_RID = dbo.ufn_GetLocationTypeID('Alternate Names')
                                                        AND [T1].[Location_RID] NOT IN (SELECT [Location_RID] FROM [#Locations])
                                                        ORDER BY [T1].[Name];
                                                        -- Add Standard Names for Custom Regions          
                                                        INSERT [#Locations] 
                                                                ([Location_RID], [StandardName_RID],[Region_RID],[Region])           
                                                        SELECT DISTINCT [T1].[Location_RID],[LK_StandardName_RID],
                                                                    [T1].[Location_RID],[T1].[Name]
                                                        FROM [Locations] [T1] 
                                                        JOIN [#Locations] [T2]
                                                        ON [T1].[Location_RID] = [T2].[StandardName_RID]
                                                        WHERE [T1].[Location_RID] NOT IN (SELECT [Location_RID] FROM [#Locations]);                   
                                                        -- Update Custom Regions
                                                        INSERT [#Locations] 
                                                                ([Location_RID], [StandardName_RID],[Region_RID], [Region],[Name])
                                                        SELECT DISTINCT [T1].[Location_RID], [T1].[LK_StandardName_RID], 
                                                                    [T2].[Region_RID],[T2].[Region],[T1].[Name]
                                                        FROM [Locations] [T1] 
                                                        INNER JOIN [#Locations] [T2]
                                                        ON [T1].[LK_StandardName_RID] = [T2].[Region_RID]  
                                                        WHERE [T1].[Location_RID] NOT IN (SELECT [Location_RID] FROM [#Locations]);                           
                                                        SELECT DISTINCT [ID]		= [T1].ID,								
				                                                        [Region]	= [L2].Region,
				                                                        [SubRegion] = ISNULL([L2].SubRegion,''),
				                                                        [Country]	= ISNULL([L2].Name,'')
                                                        FROM [#Locations] [L2]
                                                        INNER JOIN  LocalDevices [LD] 
                                                        ON [L2].Location_RID  = [LD].FK_Location_RID 
                                                        INNER JOIN Devices [D]
                                                        ON [LD].FK_Device_RID = [D].Device_RID 
                                                        INNER JOIN DeviceFunctions [DF] 
                                                        ON [D].Device_RID = [DF].FK_Device_RID
                                                        INNER JOIN Functions [F]
                                                        ON [DF].FK_Function_RID = [F].Function_RID 
                                                        INNER JOIN #LoadList [T1]
                                                        ON [F].FK_ID = [T1].ID
                                                        ORDER BY 	[ID],
			                                                        [Region],
			                                                        [SubRegion],
			                                                        [Country]     									         
                                                        DROP TABLE [#Locations];
                                                        DROP TABLE [#SubLocations]; 
                                                        DROP TABLE [#SubRegions];
                                                        DROP TABLE [#LoadList];", "'"+strId.ToString()+"'");
                    IDbCommand objCmd = objDB.CreateCommand();
                    objCmd.CommandType = CommandType.Text;
                    objCmd.CommandText = strQuery;
                    dsResult = objDB.ExecuteDataSet(objCmd);
                    if (dsResult != null && dsResult.Tables[0].Rows.Count > 0)
                    {
                        dsResult.Tables[0].TableName = "LocationSearch";
                    }
                }               
            }
            catch (Exception ex)
            {
                ErrorLogging.WriteToErrorLogFile(ex);
            }
            return dsResult;
        }
        #endregion
    }        
}