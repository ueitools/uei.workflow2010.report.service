﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.IO;
namespace UEI.Workflow2010.Report.Service
{
    public class ErrorLogging
    {
        private static String logFile = String.Empty;
        private static String errorlogFile = String.Empty;
        public static String reportLogFile = String.Empty;
        private const string _UEITOOLSROOT = "HKEY_CURRENT_USER\\Software\\UEITools\\";

        private const string _WORKINGDIR = "WorkingDirectory";
        private const string _DEFAULT_WORKINGDIR = "C:\\Working";
        
        static ErrorLogging()
        {
            logFile = GetSettings("Log");
            errorlogFile = GetSettings("ErrorLog");
            reportLogFile = GetSettings("ReportLog");
            //Check if the Log file Directory is exist
            //if not create
            IsDirectoryPresent(StripDirectoryName(logFile), true);

            //Check if the Error Log file Directory is exist
            //if not create
            IsDirectoryPresent(StripDirectoryName(errorlogFile), true);
        }
        private static String GetfilefromPath(String path)
        {          
            try
            {
                String formattedDate = String.Empty;
                String file = String.Empty;
                Int32 indexOfPeriod;
                formattedDate = "(" + DateTime.Now.ToString("dd - MM - yyyy") + ")";
                indexOfPeriod = path.LastIndexOf(".");
                file = path.Insert(indexOfPeriod, formattedDate);
                return file;
            }
            catch (Exception ex)
            {
                //Write To Error Log
                WriteToErrorLogFile(ex);
                return "";
            }
            finally
            {
            }
        }

        private static String GetSettings(String val)
        {
            try
            {
                String path = String.Empty;
                switch (val)
                {
                    case "Log":
                        path = GetfilefromPath((string)Microsoft.Win32.Registry.GetValue(
                               _UEITOOLSROOT, _WORKINGDIR, _DEFAULT_WORKINGDIR) + "\\Report\\Log.txt");
                        break;
                    case "ErrorLog":
                        path = GetfilefromPath( (string)Microsoft.Win32.Registry.GetValue(
                               _UEITOOLSROOT, _WORKINGDIR, _DEFAULT_WORKINGDIR) + "\\Report\\ErrorLog.txt");
                        break;
                    case "ReportLog":
                        path = GetfilefromPath((string)Microsoft.Win32.Registry.GetValue(
                               _UEITOOLSROOT, _WORKINGDIR, _DEFAULT_WORKINGDIR) + "\\Report\\ReportLog.txt");
                        break;
                }
                return path;
            }
            catch(Exception ex)
            {
                //Write To Error Log
                WriteToErrorLogFile(ex);
                return "";
            }
            finally
            {
            }            
        }

        #region Write To File
        public static void WriteToReportLogFile(String ReportType, IList<Model.Id> ids,IList<Model.DataSources> dataSources, List<String> selectedModes, IList<Model.Region> locations, IList<Model.DeviceType> deviceTypes, Model.ModelInfoSpecific miscFilters, Model.IdSetupCodeInfo idSetupCodeInfoParam,Model.QuickSetInfo quicksetInfo, Boolean isAppend)
        {
            if (IsDirectoryPresent(StripDirectoryName(reportLogFile), true))
            {
                FileStream fs = null;
                StreamWriter sw = null;
                try
                {
                    if (isAppend)                    
                        fs = new FileStream(reportLogFile, FileMode.Append, FileAccess.Write);                    
                    else
                        fs = new FileStream(reportLogFile, FileMode.Create, FileAccess.Write);                    
                    sw = new StreamWriter(fs);
                    sw.WriteLine("-- ----------------------------------------------------------- --");
                    sw.WriteLine("--                       User Details                            ");
                    sw.WriteLine("-- ----------------------------------------------------------- --");
                    sw.WriteLine("User          : " + Environment.UserDomainName + "\\" + Environment.UserName);
                    sw.WriteLine("Machine Name  : " + Environment.MachineName);
                    sw.WriteLine("Report        : " + ReportType);
                    sw.WriteLine("Date          : " + DateTime.Now.ToString("d"));
                    sw.WriteLine("Time          : " + DateTime.Now.ToString("T"));
                    sw.WriteLine("-- ----------------------------------------------------------- --");
                    sw.WriteLine("--                      Load List                              --");
                    sw.WriteLine("-- ----------------------------------------------------------- --");
                    if (ids != null)
                    {
                        foreach (Model.Id item in ids)
                        {
                            sw.Write(item.ID + ",");
                        }
                        sw.WriteLine("");
                    }                    
                    sw.WriteLine("-- ----------------------------------------------------------- --");
                    sw.WriteLine("-- Priority Id List                                            --");
                    sw.WriteLine("-- ----------------------------------------------------------- --");
                    if (idSetupCodeInfoParam.PriorityIDList != null)
                    {                        
                        foreach (Model.Id id in idSetupCodeInfoParam.PriorityIDList)
                        {
                            sw.Write(id.ID + ",");
                        }
                        sw.WriteLine("");
                    }                    
                    sw.WriteLine("-- Alias Id List --------------------------------------------- --");
                    if (idSetupCodeInfoParam.IdenticalIdList != null)
                    {
                        foreach (Model.AliasId identicalId in idSetupCodeInfoParam.IdenticalIdList)
                        {
                            if (identicalId.AliasID.Length > 0)
                            {
                                sw.Write(identicalId.AliasID + ",");
                            }
                            sw.WriteLine("");
                        }
                    }
                    sw.WriteLine("-- ----------------------------------------------------------- --");
                    sw.WriteLine("--                 Market Tab Selection                        --");
                    sw.WriteLine("-- ----------------------------------------------------------- --");
                    sw.WriteLine("-- Regions                                                     --");
                    sw.WriteLine("-- ----------------------------------------------------------- --");
                    if (locations != null)
                    {
                        foreach (Model.Region region in locations)
                        {
                            sw.WriteLine(region.Name);
                        }
                    }                    
                    sw.WriteLine("-- ----------------------------------------------------------- --");
                    sw.WriteLine("-- Sub Regions                                                 --");
                    if (locations != null)
                    {
                        foreach (Model.Region region in locations)
                        {
                            if (region.SubRegions != null)
                            {
                                foreach (Model.Region subRegion in region.SubRegions)
                                {
                                    sw.WriteLine(region.Name + " : " + subRegion.Name);
                                }
                            }                            
                        }
                    }                    
                    sw.WriteLine("-- ----------------------------------------------------------- --");
                    sw.WriteLine("-- Countries                                                   --");
                    sw.WriteLine("-- ----------------------------------------------------------- --");
                    if (locations != null)
                    {
                        foreach (Model.Region region in locations)
                        {
                            if (region.SubRegions != null)
                            {
                                foreach (Model.Region subRegion in region.SubRegions)
                                {
                                    if (region.Countries != null)
                                    {
                                        foreach (Model.Country country in region.Countries)
                                        {
                                            if (subRegion.Name == country.SubRegion)
                                            {
                                                sw.WriteLine(region.Name + " : " + subRegion.Name + " : " + country.Name);
                                            }
                                        }
                                    }                                    
                                }
                            }
                            if (region.Countries != null)
                            {
                                foreach (Model.Country country in region.Countries)
                                {
                                    if (country.SubRegion == String.Empty)
                                    {
                                        sw.WriteLine(region.Name + " : " + "" + " : " + country.Name);
                                    }
                                }
                            }
                        }
                    }                    
                    sw.WriteLine("-- ----------------------------------------------------------- --");
                    sw.WriteLine("-- Include Non Country Linked Records                          --");
                    sw.WriteLine(idSetupCodeInfoParam.IncludeNonCountryLinkedRecords.ToString());                    
                    sw.WriteLine("-- Include Unknown Location                                    --");
                    sw.WriteLine(idSetupCodeInfoParam.IncludeUnknownLocation.ToString());
                    sw.WriteLine("-- ----------------------------------------------------------- --");
                    sw.WriteLine("--           Data Source Location Tab Selection                --");
                    sw.WriteLine("-- ----------------------------------------------------------- --");
                    if (dataSources != null)
                    {
                        foreach (Model.DataSources item in dataSources)
                        {
                            sw.Write(item.Name.Trim() + ",");
                        }
                        sw.WriteLine("");
                    }                    
                    sw.WriteLine("-- ----------------------------------------------------------- --");
                    sw.WriteLine("--               Device Tab Selection                          --");
                    sw.WriteLine("-- ----------------------------------------------------------- --");
                    sw.WriteLine("-- Main Devices                                                --");
                    sw.WriteLine("-- ----------------------------------------------------------- --");
                    foreach (String mode in selectedModes)
                    {
                        foreach (Model.DeviceType device in deviceTypes)
                        {
                            if (device.Mode == mode)
                            {
                                sw.WriteLine(mode + " : " + device.Name);
                            }
                        }
                    }                    
                    sw.WriteLine("-- ----------------------------------------------------------- --");
                    sw.WriteLine("-- Sub Devices                                                 --");
                    sw.WriteLine("-- ----------------------------------------------------------- --");
                    foreach (String mode in selectedModes)
                    {
                        foreach (Model.DeviceType device in deviceTypes)
                        {
                            if (device.Mode == mode)
                            {
                                if (device.SubDevices != null)
                                {
                                    foreach (Model.DeviceType subdevice in device.SubDevices)
                                    {
                                        sw.WriteLine(mode + " : " + device.Name + " : " + subdevice.Name);
                                    }
                                }                                
                            }
                        }
                    }                    
                    sw.WriteLine("-- ----------------------------------------------------------- --");
                    sw.WriteLine("-- Components                                                  --");
                    sw.WriteLine("-- ----------------------------------------------------------- --");
                    foreach (String mode in selectedModes)
                    {
                        foreach (Model.DeviceType device in deviceTypes)
                        {
                            if (device.Mode == mode)
                            {
                                if (device.SubDevices != null)
                                {
                                    foreach (Model.DeviceType subdevice in device.SubDevices)
                                    {
                                        if (device.Components != null)
                                        {
                                            foreach (Model.Component component in device.Components)
                                            {
                                                if (subdevice.Name == component.SubDeviceType)
                                                {
                                                    sw.WriteLine(mode + " : " + device.Name + " : " + subdevice.Name + " : " + component.Name);
                                                }
                                            }
                                        }                                        
                                    }
                                }
                                if (device.Components != null)
                                {
                                    foreach (Model.Component component in device.Components)
                                    {
                                        if (component.SubDeviceType == String.Empty)
                                        {
                                            sw.WriteLine(mode + " : " + device.Name + " : " + "" + " : " + component.Name);
                                        }
                                    }
                                }
                            }
                        }
                    }                    
                    sw.WriteLine("-- ----------------------------------------------------------- --");
                    sw.WriteLine("-- Include Empty Device Types                                  --");
                    sw.WriteLine(idSetupCodeInfoParam.IncludeEmptyDeviceType.ToString());
                    sw.WriteLine("-- Include Non Component Linked Records                        --");
                    sw.WriteLine(idSetupCodeInfoParam.IncludeNonComponentLinkedRecords.ToString());
                    sw.WriteLine("-- ----------------------------------------------------------- --");
                    switch (ReportType)
                    {
                        case "IDList":                            
                            sw.WriteLine("-- Id & Setup Code Info Tab----------------------------------- --");
                            sw.WriteLine("-- ----------------------------------------------------------- --");
                            sw.WriteLine("-- Include Restricted Ids                                      --");
                            sw.WriteLine((idSetupCodeInfoParam.IncludeRestrictedIds == "1") ? "true" : "false");
                            sw.WriteLine("-- Include Empty Models                                        --");
                            sw.WriteLine(idSetupCodeInfoParam.IncludeBlankModelNames.ToString());
                            sw.WriteLine("-- ----------------------------------------------------------- --");
                            break;
                        case "BSC":
                        case "BSCPIC":
                            sw.WriteLine("-- Id & Setup Code Info Tab----------------------------------- --");
                            sw.WriteLine("-- ----------------------------------------------------------- --");
                            sw.WriteLine("-- Include Restricted Ids                                      --");
                            sw.WriteLine((idSetupCodeInfoParam.IncludeRestrictedIds == "1") ? "true" : "false");
                            sw.WriteLine("-- Include Empty Models                                        --");
                            sw.WriteLine(idSetupCodeInfoParam.IncludeBlankModelNames.ToString());
                            sw.WriteLine("-- Include Alias Brands                                        --");
                            sw.WriteLine(idSetupCodeInfoParam.InclideAliasBrand.ToString());
                            sw.WriteLine("-- ----------------------------------------------------------- --");
                            break;                                                    
                        case "ModelInformation":
                        case "MODELINFOPIC":
                            sw.WriteLine("-- Model Info Specific Tab ----------------------------------- --");
                            sw.WriteLine("-- ----------------------------------------------------------- --");
                            sw.WriteLine("-- Include Restricted Ids                                      --");
                            sw.WriteLine((idSetupCodeInfoParam.IncludeRestrictedIds == "1") ? "true" : "false");
                            sw.WriteLine("-- Include TN Links                                            --");
                            sw.WriteLine(miscFilters.SelectTNLink.ToString());
                            sw.WriteLine("-- ----------------------------------------------------------- --");
                            break;                                                   
                        case "QuickSet":
                            sw.WriteLine("-- QuickSet Info Tab ----------------------------------------- --");
                            sw.WriteLine("-- ----------------------------------------------------------- --");
                            sw.WriteLine("-- Mode Setup Device List                                      --");
                            if (quicksetInfo != null)
                            {
                                foreach (int deviceCode in quicksetInfo.ModeSetup.Keys)
                                {
                                    Model.QuickSetModeSetup modeSetup = quicksetInfo.ModeSetup[deviceCode];
                                    if (quicksetInfo.ModeSetup[deviceCode].SubDevices.Count > 0)
                                    {
                                        String strSubDevice = String.Empty;
                                        foreach (String subdevice in quicksetInfo.ModeSetup[deviceCode].SubDevices)
                                        {
                                            strSubDevice += "'" + subdevice + "',";
                                        }
                                        if (!String.IsNullOrEmpty(strSubDevice))
                                            sw.WriteLine(modeSetup.DeviceName + " : " + modeSetup.ModeList + " : " + strSubDevice);
                                        else
                                            sw.WriteLine(modeSetup.DeviceName + " : " + modeSetup.ModeList + " : " + strSubDevice);
                                    }
                                    else
                                    {
                                        sw.WriteLine(modeSetup.DeviceName + " : " + modeSetup.ModeList + " : " + "");
                                    }
                                }
                                
                            }
                            sw.WriteLine("-- ----------------------------------------------------------- --");
                            break;
                    }                    
                }
                finally
                {
                    if (sw != null)
                    {
                        sw.Close();
                    }

                    if (fs != null)
                    {
                        fs.Close();
                    }
                }
            }
        }
        public static void WriteToLogFile(string message, bool tm)
        {
            String _logFileName = String.Empty;
            if (IsDirectoryPresent(StripDirectoryName(logFile), true))
            {
                FileStream fs = null;
                StreamWriter sw = null;
                try
                {
                    fs = new FileStream(logFile, FileMode.Append, FileAccess.Write);
                    sw = new StreamWriter(fs);
                    if (tm == true)
                    {
                        //sw.WriteLine("===================================================");
                        sw.WriteLine(message + "\t" + DateTime.Now.ToString());
                        //sw.WriteLine("===================================================");
                    }
                    else
                    {
                       // sw.WriteLine("===================================================");
                        sw.WriteLine(message);
                        //sw.WriteLine("===================================================");
                    }
                    //sw.WriteLine("");
                }
                catch(Exception ex)
                {
                    WriteToErrorLogFile(ex);
                }
                finally
                {
                    if (sw != null)
                    {
                        sw.Close();
                    }
                    if (fs != null)
                    {
                        fs.Close();
                    }                    
                }
            }
        }

        public static void WriteToErrorLogFile(Exception sourceException)
        {
            if (IsDirectoryPresent(StripDirectoryName(errorlogFile), true))
            {
                FileStream fs = null;
                StreamWriter sw = null;
                try
                {
                    fs = new FileStream(errorlogFile, FileMode.Append, FileAccess.Write);
                    sw = new StreamWriter(fs);
                    sw.WriteLine("==================================================================");
                    sw.WriteLine("ERROR OCCOURED IN:" + sourceException.Source);
                    sw.WriteLine("ERROR DESCRPTION:" + sourceException.Message);
                    sw.WriteLine("ERROR DESCRPTION:" + sourceException.StackTrace);
                    sw.WriteLine("ERROR DATE TIME:" + System.DateTime.Now.ToString());
                    sw.WriteLine("==================================================================");
                    sw.WriteLine("");
                }
                finally
                {
                    if (sw != null)
                    {
                        sw.Close();
                    }

                    if (fs != null)
                    {
                        fs.Close();
                    }
                }
            }
        }
        public static void WriteToErrorLogFile(Exception sourceException,BusinessObject.IDSearchParameter param)
        {
            if (IsDirectoryPresent(StripDirectoryName(errorlogFile), true))
            {
                FileStream fs = null;
                StreamWriter sw = null;
                try
                {
                    fs = new FileStream(errorlogFile, FileMode.Append, FileAccess.Write);
                    sw = new StreamWriter(fs);
                    sw.WriteLine("==================================================================");
                    sw.WriteLine("PARAMETERS:-");
                    sw.WriteLine("ID :" + param.Ids);
                    sw.WriteLine("REGION :" + param.Regions);
                    sw.WriteLine("SUB REGION :" + param.SubRegions);
                    sw.WriteLine("COUNTRY :" + param.Countries);
                    sw.WriteLine("LOCATION XML:" + param.XMLLocations);
                    sw.WriteLine("DATASOURCE :" + param.DataSources);
                    sw.WriteLine("DEVICETYPES :" + param.DeviceTypes);
                    sw.WriteLine("SUBDEVICETYPES :" + param.SubDeviceTypes);
                    sw.WriteLine("COMPONENTS :" + param.DeviceComponents);
                    sw.WriteLine("DEVICE XML :" + param.XMLDevices);
                    sw.WriteLine("ERROR OCCOURED IN:" + sourceException.Source);
                    sw.WriteLine("ERROR DESCRPTION:" + sourceException.Message);
                    sw.WriteLine("ERROR DESCRPTION:" + sourceException.StackTrace);
                    sw.WriteLine("ERROR DATE TIME:" + System.DateTime.Now.ToString());
                    sw.WriteLine("==================================================================");
                    sw.WriteLine("");
                }
                finally
                {
                    if (sw != null)
                    {
                        sw.Close();
                    }

                    if (fs != null)
                    {
                        fs.Close();
                    }
                }
            }
        }
        public static bool IsDirectoryPresent(string directory, bool create)
        {
            try
            {
                if (!Directory.Exists(directory))
                {
                    if (create == true)
                    {
                        Directory.CreateDirectory(directory);
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return true;
                }
            }
            catch(Exception ex)
            {    
                //Write to Error Log
                WriteToErrorLogFile(ex);
                return false;
            }
            finally
            {
            }
        }
        public static string StripDirectoryName(string path)
        {
            string direcoryPath = @"";
            int indexOfLastSlash = 0;

            try
            {
                indexOfLastSlash = path.LastIndexOf(@"\");
                direcoryPath = path.Substring(0, indexOfLastSlash);
                return direcoryPath;
            }
            catch(Exception ex)
            {      
                //Write to Error Log file
                WriteToErrorLogFile(ex);
                return "";
            }
            finally
            {
            }
        }
        #endregion
    }
}
